﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;


public class Conversation : MonoBehaviour
{
    public GUIText talkTextGUI;

    public Font normalFont;
    public Font specialFont;

    public string[] talkLines;
    public string[] delimiters = new string[2];
    public float textScrollSpeed;

    private bool talking;
    private bool textIsScrolling;

    private Player playerScript;
    private int currentLine;

    void OnTriggerEnter(Collider col)
    {
        //if (col.tag == "Player")
        //{
        //    Debug.Log(playerScript);
        //    talking = true;
        //    currentLine = 0;
        //    //talkTextGUI.text = talkLines[currentLine]; //STATIC
        //    StartCoroutine(StartScrolling());
        //    //playerScript.enabled = false;
        //    //col.animation.CrossFade("idle");
        //}
    }

    void Start()
    {
        playerScript = GameObject.Find("Player").GetComponent<Player>();
        delimiters[0] = "::";
        //delimiters[1] = "";
    }
﻿
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            playerScript.status = Character.Status.locked;

            if (textIsScrolling)
            {
                //talkTextGUI.text = talkLines[currentLine];
                //textIsScrolling = false;
            }
            else
            {
                if (currentLine < talkLines.Length - 1)
                {
                    StartCoroutine(StartScrolling());
                    currentLine++;
                }
                else
                {
                    currentLine = 0;
                    talkTextGUI.text = "";
                    talking = false;
                    playerScript.status = Character.Status.none;
                }
            }
        }
    }

    string RemoveSpecialCharacters(string s)
    {
        s = s.Replace("♪", "");
        return s;
    }
﻿
    IEnumerator StartScrolling()
    {
        //Text is being printed
        textIsScrolling = true;

        //Store the currently printed line's number
        int startLine = currentLine;

        string displayText = "";

        //For each line of text
        if (textIsScrolling && currentLine == startLine)
        {
            #region Start
            //if (talkLines[currentLine][i].ToString() != "♪")
            //{

            //    displayText += talkLines[currentLine][i];
            //    talkTextGUI.text = displayText;
            //    yield return new WaitForSeconds(1 / textScrollSpeed);
            //}
            //else
            #endregion

            var line = talkLines[currentLine].ToString();

            string[] words = line.Split((string[])null, System.StringSplitOptions.None);

            ///For each word in this one line
            for (int currentWord = 0; currentWord < words.Length; currentWord++)
            {
                print(words[currentWord]);

                if (words[currentWord] == ("[Pause]"))
                {
                    yield return new WaitForSeconds(1);
                }
                else
                {
                    for (int currentLetter = 0; currentLetter < words[currentWord].Length; currentLetter++)
                    {
                        displayText += words[currentWord][currentLetter];
                        talkTextGUI.text = displayText;
                        yield return new WaitForSeconds(1 / textScrollSpeed);
                    }
                    displayText += " ";
                }
            }
        }

        textIsScrolling = false;
    }
}