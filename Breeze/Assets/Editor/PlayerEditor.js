// Custom Editor using SerializedProperties.
// Automatic handling of multi-object editing, undo, and prefab overrides.

#pragma strict
@CustomEditor(Player)
@CanEditMultipleObjects

class PlayerEditor extends Editor 
{

    //Health
    var health : SerializedProperty;
    var maxHealth : SerializedProperty;
    
    //Stamina
    var stamina : SerializedProperty;
    var maxStamina : SerializedProperty;

    //Stats
    var strength : SerializedProperty;
    var defense : SerializedProperty;
    var walkSpeed : SerializedProperty;
    var runSpeed : SerializedProperty;
    var jump : SerializedProperty;

    //Factoring Variables
    var expFactor : SerializedProperty;
    var statsFactor : SerializedProperty;
    var healthFactor : SerializedProperty;

    //Base Stat Rates
    var baseAtk : SerializedProperty;
    var baseDef : SerializedProperty;
    var baseSta : SerializedProperty;
    var baseHp : SerializedProperty;

    //Character Type
    var charType : SerializedProperty;
    var cIndex : int;
    var cOptions : String[];
    
    //Status
    var status : SerializedProperty;
    var iIndex : int;
    var iOptions : String[];

    //Character state
    var state : SerializedProperty;
    var stIndex : int;
    var stOptions : String[];

    //Character status
    var charStatus : SerializedProperty;
    var charStIndex : int;
    var charStOptions : String[];
    
    
    //Booleans for showing and Hiding
    var showStats : boolean = true;
    var showBaseStats : boolean = true;
    var showHP : boolean = true;
    var showExp : boolean = true;
    var showStamina : boolean = true;
    var showAnim : boolean = true;

    var  walkAnim : SerializedProperty;
    var  runAnim : SerializedProperty;
    var  idleAnim : SerializedProperty;
    var  fallAnim : SerializedProperty;
    var  glideAnim : SerializedProperty;
    var  jumpAnim : SerializedProperty;
    var deathAnim : SerializedProperty;

    var attack1Anim : SerializedProperty;

    var  damaged1Anim : SerializedProperty;
    var  damaged2Anim : SerializedProperty;
    var  damaged3Anim : SerializedProperty;

    function OnEnable () 
    {
        // Setup the SerializedProperties

        health = serializedObject.FindProperty ("health");
        maxHealth = serializedObject.FindProperty ("maxHealth");

        stamina = serializedObject.FindProperty ("stamina");
        maxStamina = serializedObject.FindProperty ("maxStamina");

        strength = serializedObject.FindProperty ("strength");
        defense = serializedObject.FindProperty ("defense");
        walkSpeed = serializedObject.FindProperty ("walkSpeed");
        runSpeed = serializedObject.FindProperty ("runSpeed");
        jump = serializedObject.FindProperty ("jumpHeight");

        charType = serializedObject.FindProperty ("charType");
        status = serializedObject.FindProperty ("infliction");
        state = serializedObject.FindProperty ("characterState");
        charStatus = serializedObject.FindProperty ("status");
    }

    function OnInspectorGUI() {
        
        var character = target as Character;
        
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update ();
         
        showStats = EditorGUILayout.Foldout(showStats, "Character Stats");
        if (showStats)
            ShowCharacterStats();

        showHP = EditorGUILayout.Foldout(showHP, "Character Health");
        if (showHP)
        {ShowHealthBar();}


        showStamina = EditorGUILayout.Foldout(showStamina, "Character Stamina");
        if (showStamina)
        {ShowStaminaBar();

        ShowStates();


        if (GUI.changed)
            EditorUtility.SetDirty (character);
            
            // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
            serializedObject.ApplyModifiedProperties ();

        }
    }

    // Custom GUILayout progress bar.
    function ProgressBar (value : float, label : String) 
        {
            // Get a rect for the progress bar using the same margins as a textfield:
            var rect : Rect = GUILayoutUtility.GetRect (18, 18, "TextField");
            EditorGUI.ProgressBar (rect, value, label);
            EditorGUILayout.Space ();
        }

        function ShowCharacterStats()
        {
            GUILayout.Label ("Character Settings", EditorStyles.boldLabel);
            strength.intValue = EditorGUILayout.IntField("Strength:", strength.intValue);
            defense.intValue = EditorGUILayout.IntField("Defense:", defense.intValue);
            walkSpeed.floatValue = EditorGUILayout.FloatField("Walk Speed:", walkSpeed.floatValue);
            runSpeed.floatValue = EditorGUILayout.FloatField("Run Speed:", runSpeed.floatValue);
            jump.intValue = EditorGUILayout.IntField("Jump Height:", jump.intValue);


           // jump.intValue = EditorGUILayout.IntField("Jump:", jump.intValue);
        }

        function ShowHealthBar()
        {
            GUILayout.Label ("Health Settings", EditorStyles.boldLabel);

            // Show the custom GUI controls
            health.intValue = EditorGUILayout.IntField("Health:", health.intValue);
            maxHealth.intValue = EditorGUILayout.IntField("Max Health:", maxHealth.intValue);

            //if (!health.hasMultipleDifferentValues)
            var hp : float = health.intValue;
            var maxhp : float = maxHealth.intValue;
        
            ProgressBar (hp/maxhp, "Health: " + hp + "/" + maxhp);
        }
    
        function ShowStaminaBar()
        {
            GUILayout.Label ("Stamina Settings", EditorStyles.boldLabel);
        
            // Show the custom GUI controls
            stamina.intValue = EditorGUILayout.IntField("Stamina:", stamina.intValue);
            maxStamina.intValue = EditorGUILayout.IntField("Max Stamina:", maxStamina.intValue);

            //if (!health.hasMultipleDifferentValues)
            var sp : float = stamina.intValue;
            var maxsp : float = maxStamina.intValue;
        
            ProgressBar (sp/maxsp, "Stamina");
        }


        function ShowStates()
        {
            cIndex = charType.enumValueIndex;
            GUILayout.Label ("Character Type", EditorStyles.boldLabel);
            cOptions = charType.enumNames;
            cIndex = EditorGUILayout.Popup(cIndex, cOptions);
            charType.enumValueIndex = cIndex;

            iIndex = status.enumValueIndex;
            GUILayout.Label ("Status Infliction", EditorStyles.boldLabel);
            iOptions = status.enumNames;
            iIndex = EditorGUILayout.Popup(iIndex, iOptions);
            status.enumValueIndex = iIndex;

            stIndex = state.enumValueIndex;
            GUILayout.Label ("Character State", EditorStyles.boldLabel);
            stOptions = state.enumNames;
            stIndex = EditorGUILayout.Popup(stIndex, stOptions);
            state.enumValueIndex = stIndex;

            charStIndex = charStatus.enumValueIndex;
            GUILayout.Label ("Character Status", EditorStyles.boldLabel);
            charStOptions = charStatus.enumNames;
            charStIndex = EditorGUILayout.Popup(charStIndex, charStOptions);
            charStatus.enumValueIndex = charStIndex;

            //DrawDefaultInspector ();
        }


    }