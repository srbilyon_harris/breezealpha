// Custom Editor using SerializedProperties.
// Automatic handling of multi-object editing, undo, and prefab overrides.

#pragma strict
@CustomEditor(Enemy)
@CanEditMultipleObjects

class EnemyEditor extends Editor 
{
    var health : SerializedProperty;
    var maxHealth : SerializedProperty;
    
    var stamina : SerializedProperty;
    var maxStamina : SerializedProperty;
    
    var magic : SerializedProperty;
    var maxMagic : SerializedProperty;

    var charType : SerializedProperty;
    var cIndex : int;
    var cOptions : String[];
    
    var battleState : SerializedProperty;
    var bsIndex : int;
    var bsOptions : String[];

    var status : SerializedProperty;
    var iIndex : int;
    var iOptions : String[];
    
    var behavior : SerializedProperty;
    var bIndex : int;
    var bOptions : String[];
    
    var showHP : boolean = true;
    var showMP : boolean = true;
    var showStamina : boolean = true;
    var showAnim : boolean = true;

    var  walkAnim : SerializedProperty;
    var  runAnim : SerializedProperty;
    var  idleAnim : SerializedProperty;
    var  fallAnim : SerializedProperty;
    var  glideAnim : SerializedProperty;
    var  jumpAnim : SerializedProperty;
    var deathAnim : SerializedProperty;

    var attack1Anim : SerializedProperty;

    var  damaged1Anim : SerializedProperty;
    var  damaged2Anim : SerializedProperty;
    var  damaged3Anim : SerializedProperty;


    function OnEnable () 
    {
        // Setup the SerializedProperties
        health = serializedObject.FindProperty ("health");
        maxHealth = serializedObject.FindProperty ("maxHealth");

        stamina = serializedObject.FindProperty ("stamina");
        maxStamina = serializedObject.FindProperty ("maxStamina");

        magic = serializedObject.FindProperty ("magic");
        maxMagic = serializedObject.FindProperty ("maxMagic");

        charType = serializedObject.FindProperty ("charType");
        battleState = serializedObject.FindProperty ("battleState");
        status = serializedObject.FindProperty ("status");
        behavior = serializedObject.FindProperty ("behavior");

        idleAnim = serializedObject.FindProperty ("idleAnim");
        walkAnim = serializedObject.FindProperty ("walkAnim");
        runAnim = serializedObject.FindProperty ("runAnim");
        idleAnim = serializedObject.FindProperty ("idleAnim");
        fallAnim = serializedObject.FindProperty ("fallAnim");
        glideAnim = serializedObject.FindProperty ("glideAnim");
        jumpAnim = serializedObject.FindProperty ("jumpAnim");
        deathAnim = serializedObject.FindProperty ("deathAnim");

        attack1Anim = serializedObject.FindProperty ("attack1Anim");

        damaged1Anim = serializedObject.FindProperty ("damaged1Anim");
        damaged2Anim = serializedObject.FindProperty ("damaged2Anim");
        damaged3Anim = serializedObject.FindProperty ("damaged3Anim");

      
    }

    function OnInspectorGUI() {
        
        var character = target as Character;
        
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update ();
        
        showHP = EditorGUILayout.Foldout(showHP, "Character Health");
        if (showHP)
        {ShowHealthBar();}

        showMP = EditorGUILayout.Foldout(showMP, "Character Magic");
        if (showMP)
        {ShowMagicBar();}

        showStamina = EditorGUILayout.Foldout(showStamina, "Character Stamina");
        if (showStamina)
        {ShowStaminaBar();}

        showAnim = EditorGUILayout.Foldout(showAnim, "Character Animations");
        //if (showAnim)
        //{ShowAnimations();}

        ShowStates();

         
        if (GUI.changed)
            EditorUtility.SetDirty (character);
            
        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties ();

    }

    // Custom GUILayout progress bar.
    function ProgressBar (value : float, label : String) 
        {
            // Get a rect for the progress bar using the same margins as a textfield:
            var rect : Rect = GUILayoutUtility.GetRect (18, 18, "TextField");
            EditorGUI.ProgressBar (rect, value, label);
            EditorGUILayout.Space ();
        }

        function ShowHealthBar()
        {
            GUILayout.Label ("Health Settings", EditorStyles.boldLabel);

            // Show the custom GUI controls
            health.intValue = EditorGUILayout.IntField("Health:", health.intValue);
            maxHealth.intValue = EditorGUILayout.IntField("Max Health:", maxHealth.intValue);

            //if (!health.hasMultipleDifferentValues)
            var hp : float = health.intValue;
            var maxhp : float = maxHealth.intValue;
        
            ProgressBar (hp/maxhp, "Health: " + hp + "/" + maxhp);
        }
    
        function ShowStaminaBar()
        {
            GUILayout.Label ("Stamina Settings", EditorStyles.boldLabel);
        
            // Show the custom GUI controls
            stamina.intValue = EditorGUILayout.IntField("Stamina:", stamina.intValue);
            maxStamina.intValue = EditorGUILayout.IntField("Max Stamina:", maxStamina.intValue);

            //if (!health.hasMultipleDifferentValues)
            var sp : float = stamina.intValue;
            var maxsp : float = maxStamina.intValue;
        
            ProgressBar (sp/maxsp, "Pace");
        }

        function ShowMagicBar()
        {
            GUILayout.Label ("Magic Settings", EditorStyles.boldLabel);
            // Show the custom GUI controls
            //magic.intValue = EditorGUILayout.IntField("Magic:", magic.intValue);
            //maxMagic.intValue = EditorGUILayout.IntField("Max Magic:", maxMagic.intValue);

            //if (!health.hasMultipleDifferentValues)
            //var mp : float = magic.intValue;
            //var maxmp : float = maxMagic.intValue;
        
            //ProgressBar (mp/maxmp, "Magic: " + mp + "/" + maxmp);
        }

        function ShowStates()
        {
            cIndex = charType.enumValueIndex;
            GUILayout.Label ("Character Type", EditorStyles.boldLabel);
            cOptions = charType.enumNames;
            cIndex = EditorGUILayout.Popup(cIndex, cOptions);
            charType.enumValueIndex = cIndex;

            //bIndex = behavior.enumValueIndex;
            //GUILayout.Label ("Steering Behavior", EditorStyles.boldLabel);
            //bOptions = behavior.enumNames;
            //bIndex = EditorGUILayout.Popup(bIndex, bOptions);
            //behavior.enumValueIndex = bIndex;

            iIndex = status.enumValueIndex;
            GUILayout.Label ("Status Infliction", EditorStyles.boldLabel);
            iOptions = status.enumNames;
            iIndex = EditorGUILayout.Popup(iIndex, iOptions);
            status.enumValueIndex = iIndex;

            //DrawDefaultInspector ();
        }

        //function ShowAnimations()
        //{            
        //    idleAnim.stringValue = EditorGUILayout.TextField("Idle Animation:" , idleAnim.stringValue);
        //    walkAnim.stringValue = EditorGUILayout.TextField("Walk Animation:" , walkAnim.stringValue);
        //    runAnim.stringValue = EditorGUILayout.TextField("Run Animation:" , runAnim.stringValue);
        //    fallAnim.stringValue = EditorGUILayout.TextField("Falling Animation:" , fallAnim.stringValue);
        //    glideAnim.stringValue = EditorGUILayout.TextField("Glide Animation:" , glideAnim.stringValue);
        //    jumpAnim.stringValue = EditorGUILayout.TextField("Jump Animation:" , jumpAnim.stringValue);
        //    deathAnim.stringValue = EditorGUILayout.TextField("Death Animation:" , deathAnim.stringValue);

        //    attack1Anim.stringValue = EditorGUILayout.TextField("Attack 1 Animation:" , attack1Anim.stringValue);

        //    damaged1Anim.stringValue = EditorGUILayout.TextField("Hit 1 Animation:" , damaged1Anim.stringValue);
        //    damaged2Anim.stringValue = EditorGUILayout.TextField("Hit 2 Animation:" , damaged2Anim.stringValue);
        //    damaged3Anim.stringValue = EditorGUILayout.TextField("Hit 3 Animation:" , damaged3Anim.stringValue);

        //}
    }