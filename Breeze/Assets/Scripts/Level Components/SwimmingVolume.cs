/*
 * Swimming Trigger
 * Attach this script to enemies
 * Make sure this instance is a trigger
 * 
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Attach this script to enemies
/// Make sure this instance is a trigger
/// </summary>
public class SwimmingVolume : GenericTrigger
{
    public override void Update()
    {
        if (IsPlayerInVolume())
            RunVolume();
        else
            if (!IsPlayerInVolume() && ObjectsInTrigger.Contains(player))
                PlayerLeftTrigger();

        CheckTrigger();
    }

    public void RunVolume()
    {
        player.GetComponent<Character>().status = Character.Status.swimming;
    }

    public void PlayerLeftTrigger()
    {
        player.GetComponent<Character>().status = Character.Status.none;
        player.GetComponent<Character>().characterState = Character.CharacterState.none;

    }

    /// <summary>
    /// Checks to see if the object that fell in is a player or an enemy
    /// </summary>
    /// <param name="other"></param>
    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        //other.GetComponent<Character>().characterState = Character.CharacterState.swimming;
    //        other.GetComponent<Character>().status = Character.Status.swimming;
    //        //LOAD PARTICLE
    //        //var splash = (GameObject)Instantiate(Resources.Load("Splash"));
    //        //splash.transform.position = new Vector3(other.transform.position.x,other.transform.position.y + 2, other.transform.position.z);
    //    }
    //    else
    //        if (other.gameObject.tag == "Enemy")
    //        {
    //            //LOAD PARTICLE
    //            //var splash = (GameObject)Instantiate(Resources.Load("Splash"));
    //            //splash.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 2, other.transform.position.z);
    //        }

    //}
    /// <summary>
    /// Checks to see if the object that jumped out is a player or an enemy
    /// </summary>
    /// <param name="other"></param>
    //void OnTriggerExit(Collider other)
    //{

    //    if (other.gameObject.tag == "Player")
    //    {
    //        var splash = (GameObject)Instantiate(Resources.Load("Splash"));
    //        splash.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 2, other.transform.position.z);
    //    }
    //    else
    //        if (other.gameObject.tag == "Enemy")
    //        {
    //            var splash = (GameObject)Instantiate(Resources.Load("Splash"));
    //            splash.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 2, other.transform.position.z);
    //        }
    //}


}
