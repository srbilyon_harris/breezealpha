using UnityEngine;
using System.Collections;

/// <summary>
/// This is the base class for objects that need to receive damage from attacks
/// </summary>
public class DamageReciever : MonoBehaviour
{
    #region Members
    /// <summary>
    /// Health of the object
    /// </summary>
    public float health = 1;
    /// <summary>
    /// Minimum amount of damage required to hurt the object
    /// </summary>
    public float minDamageToHurt = 0;
    
    public bool dead = false;
    /// <summary>
    /// The HitBox Component
    /// </summary>
    public Collider hitbox;
    #endregion

    #region Methods
    public virtual void Awake()
    {
        // Find hitbox object
        hitbox = FindHitbox(transform);
        if (hitbox == null)
        {
            hitbox = CheckChildrenForHitbox(transform);
        }
        else
        {
            hitbox.isTrigger = true;
        }
    }

    /// <summary>
    ///  Apply damage to the object
    /// </summary>
    /// <param name="dmg"></param>
    /// <param name="attackerPos"></param>
    public virtual void ApplyDamage(float dmg, Vector3 attackerPos)
    {
        if (dmg > minDamageToHurt)
        {	// Check if incoming damage is greater than minDamageToHurt
            health -= dmg;
        }
        CheckHealthLeft();
    }

    /// <summary>
    /// Check to see if the health is above 0 or not
    /// </summary>
    public void CheckHealthLeft()
    {
        if (health <= 0)
        {
            Dead();
        }
    }

    /// <summary>
    /// When the object's health drops below 0
    /// </summary>
    public virtual void Dead()
    {
        dead = true;
        collider.enabled = false;
        gameObject.renderer.enabled = false;
    }

    /// <summary>
    /// Check Children's Children for hitbox
    /// </summary>
    /// <param name="trans"></param>
    /// <returns></returns>
    public Collider CheckChildrenForHitbox(Transform trans)
    {
        Collider hb = null;
        bool found = false;

        for (int i = 0; i < trans.childCount; i++)
        {	
            // Loop through Children's Children
            hb = FindHitbox(trans.GetChild(i));
            if (hb != null)
            {
                found = true;
                return hb;
            }
        }

        if (!found)
        {
            for (int j = 0; j < trans.childCount; j++)
            { 
                // Loop through Children's Children's Children
                hb = CheckChildrenForHitbox(trans.GetChild(j));
            }
        }

        return hb;
    }

    /// <summary>
    /// Check Child for hitbox
    /// </summary>
    /// <param name="trans"></param>
    /// <returns></returns>
    public Collider FindHitbox(Transform trans)
    {
        if (trans.FindChild("HitBox") != null)
        {
            return trans.FindChild("HitBox").collider;
        }

        return null;
    }
    #endregion
}
