﻿using UnityEngine;
using System.Collections;

public class MoneyPickup : MonoBehaviour
{
    public string playerName;
    public Player player;
    public AudioClip audioClip;
    public int moneyDrop = 1;

    public enum Direction { Up, Down };
    public Direction direction = Direction.Up;
    public enum BubbleBehavior { Standard, Raising, Moving }
    public BubbleBehavior bubbleBehavior = BubbleBehavior.Standard;

    Vector3 moveDirection;


    void Awake()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
       // audioClip = (AudioClip)Resources.Load("Sounds/Breeze/BreezeCollectRaindrop01");
        audio.clip = audioClip;
        moveDirection.y = 0.03f;
    }

    void Update()
    {
       switch (bubbleBehavior)
        {
           case BubbleBehavior.Standard:
                
                    // Save the y position prior to start floating (maybe in the Start function):
                    if (direction == Direction.Up)
                    {
                        moveDirection.y = 0;
                        StartCoroutine("FloatingUp");
                    }
                    else if (direction == Direction.Down)
                    {
                        moveDirection.y = 0;
                        StartCoroutine("FloatingDown");
                    }

                    break;

           case BubbleBehavior.Raising:

                    moveDirection.y = 2f * Time.smoothDeltaTime;
                    break;       
        }

       // Put the floating movement in the Update function:
       transform.position = new Vector3(transform.position.x, moveDirection.y + transform.position.y, transform.position.z);

   }

    IEnumerator FloatingUp()
    {
        moveDirection.y += 0.08f * Time.smoothDeltaTime;
        yield return new WaitForSeconds(3);
        direction = Direction.Down;
    }

    IEnumerator FloatingDown()
    {
        moveDirection.y -= 0.08f * Time.smoothDeltaTime;
        yield return new WaitForSeconds(3);
        direction = Direction.Up;
    }

    IEnumerator OnTriggerEnter(Collider hit)
    {
        if (hit.name == playerName)
        {
            //playerStats = other.GetComponent<Stats>();
            player.money += moneyDrop   ;
                    
            //audio.Play();
            gameObject.GetComponent<Renderer>().renderer.enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;

           yield return new WaitForSeconds(audioClip.length);
           Destroy(gameObject);
        }
    }

    void LateUpdate()
    {
        if (Vector3.Distance(this.transform.position, player.transform.position) < 2)
            transform.Translate((player.transform.position - transform.position) * Time.deltaTime * 8, Space.World);
    }

}
