using UnityEngine;
using System.Collections;

public class Triggerable : MonoBehaviour {

	public virtual void Activate () {
		
	}
	
	public virtual void Deactivate () {
		
	}
}
