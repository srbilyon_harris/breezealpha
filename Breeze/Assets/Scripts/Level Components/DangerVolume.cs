using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DangerVolume : GenericTrigger
{
    public int damage = 5;
    public float lift = 0;
    public float push = 0;

    public enum VolumeType { Lava, Poison, Spikes, Instakill }
    public VolumeType volumeType = VolumeType.Spikes;

    // Use this for initialization
    void Awake()
    {
        base.Awake();
    }

    // Update is called once per frame
    public override void Update() 
    {
        base.Update();
        if (ObjectsInTrigger.Contains(player)) RunVolume();
    }

    public void RunVolume()
    {
        switch (volumeType)
        {
            case VolumeType.Spikes:
                if (player.GetComponent<Character>().status != Character.Status.flinching)
                {
                    //player.GetComponent<Character>().ApplyDamage(damage, transform.forward, push, lift, 2);
                }
                break;

            case VolumeType.Instakill:
                player.GetComponent<Character>().health = 0;
                break;

            case VolumeType.Lava:
                break;

            case VolumeType.Poison:
                //player.infliction = Character.Infliction.poisoned;
                //player.photonView.RPC("SetPoisoned", other.gameObject.GetComponent<PhotonView>().owner, null);
                //player.photonView.RPC("RemoveInfliction", other.gameObject.GetComponent<PhotonView>().owner, 5
                break;
        }
    }
}
