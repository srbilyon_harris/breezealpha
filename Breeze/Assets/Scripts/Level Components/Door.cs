using UnityEngine;
using System.Collections;

public class Door : Togglable {
	
	Animation anim;	// Animation component
	bool opened = false;	// If the door has been opened
	
	public bool startOpen = false;	// If the door should start open
	public string openAnimName;	// Open animation name
	public string closeAnimName;	// Close animation name

	// Use this for initialization
	public virtual void Start () {
		anim = GetComponent<Animation>();	// Get the Animation component
		if(startOpen){	// If it starts open, set the open animation to play then skip to the end
			opened = true;
			anim.Play(openAnimName);
			anim[openAnimName].normalizedTime = 1;
		}
	}
	
	// Inherited Toggle
	public override void Toggle (bool toggle) {
		base.Toggle(toggle);
		if(opened){	// If the door is open when Toggle is called, get the normalizedTime of the open anim if it's in progress then stop it
			float aTime = 0;
			if(anim[openAnimName].normalizedTime < 1 && anim[openAnimName].normalizedTime > 0){
				aTime = 1 - anim[openAnimName].normalizedTime;
				anim.Stop(openAnimName);
			}
			
			// Set opened to false, the play close anim at the normalizedTime
			opened = false;
			anim[closeAnimName].normalizedTime = aTime;
			anim.Play(closeAnimName);
		}
		else{	// If the door is closed when Toggle is called, get the normalizedTime of the close anim if it's in progress then stop it
			float aTime = 0;
			if(anim[closeAnimName].normalizedTime < 1 && anim[closeAnimName].normalizedTime > 0){
				aTime = 1 - anim[closeAnimName].normalizedTime;
				anim.Stop(closeAnimName);
			}
			
			// Set opened to true, the play open anim at the normalizedTime
			opened = true;
			anim[openAnimName].normalizedTime = aTime;
			anim.Play(openAnimName);
		}
	}
}
