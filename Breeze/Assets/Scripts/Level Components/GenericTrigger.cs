using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenericTrigger : MonoBehaviour {

    public Collider volume;
    Collider hit;
    public List<GameObject> ObjectsInTrigger;

    public GameObject player;

    // Use this for initialization
    public virtual void Awake()
    {
        player = GameObject.Find("Player");
        volume = gameObject.collider;

    }
	
	// Update is called once per frame
	public virtual void Update () 
    {
        CheckTrigger();
	}

    public void CheckTrigger()
    {
        if (IsPlayerInVolume() && !ObjectsInTrigger.Contains(player))
        {
            ObjectsInTrigger.Add(player);
        }
        else if (!IsPlayerInVolume())
            ObjectsInTrigger.Remove(player);
    }

    public bool IsPlayerInVolume()
    {
        if (volume.bounds.Intersects(player.collider.bounds))
        {
            return true;
        }
        else
            return false;
    }


}
