/*
 * Death Box
 * Kills of players or enemies that fall in it
 * 
 */
using UnityEngine;
using System.Collections;

public class DeathBox : MonoBehaviour 
{
    /// <summary>
    /// Get the player and store it here
    /// </summary>
	GameObject player;
	
    /// <summary>
    /// The starting location of the map
    /// </summary>
	public Transform startLocation;

    /// <summary>
    /// Location where the player needs to be placed after falling.
    /// </summary>
	public Transform spawnLocation;

    /// <summary>
    /// The name of the player instance
    /// </summary>
    public string playerName;
		
    /// <summary>
    /// Make the spawning location (if the character dies at start) the respawn location
    /// Also, assign the player name the player instance's name
    /// </summary>
	public void Start()
	{	
		if (GameObject.Find(playerName))
            player = GameObject.Find(playerName);
			                                    
		spawnLocation = startLocation;
	}
	
    /// <summary>
    /// Make the recent checkpoint the new spawning location
    /// </summary>
    /// <param name="num"></param>
	public void CurrentCheckPoint(Transform num )
	{
		spawnLocation = num;
	}
	
	/// <summary>
    /// Stores whatever walks into one of these fields into "hit"
	/// </summary>
	/// <param name="hit"></param>
	void OnTriggerEnter(Collider hit)
	{
       //If 'hit' is a player object
	   if (hit.transform.tag == "Player") 
	   { 
			Debug.Log(playerName + " Detected");
			hit.transform.position = spawnLocation.position;	
		}
       //If 'hit' is an enemy object
       if (hit.transform.tag == "Enemy")
       {
           //if(hit.GetComponent<Character>().activated)
           var me = hit.GetComponent<Character>();
           me.Die();
           //If you want the death to not happen instantly
           //hit.GetComponent<Character>().StartCoroutine("Die");
       }
	}

}
