/*
 * DestructablePlatform
 * 
 * Create a DestructablePlatform
 * This platform will break when a character lands on it
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Create a Destructable Platform
/// This platform will break when a character lands on it
/// </summary>
public class DestructablePlatform : MonoBehaviour
{
    /// <summary>
    /// The amount of time it takes for this platform to break
    /// once a character lands on it
    /// </summary>
    public float delay;

    /// <summary>
    /// The character will be parented to this object
    /// This object MUST have a scale of 1,1,1, otherwise, your character
    /// could end up being scaled!
    /// </summary>
    public Transform parent, capturedObj;

    private bool broken = false;
    public Collider col;
    /// <summary>
    /// Set the parent object as whatever the root object in the 
    /// hiearchy is 
    /// </summary>
    void Start()
    {
        parent = transform.root;
    }

    /// <summary>
    /// Check to see if a player had entered the triggered
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        capturedObj = other.gameObject.transform.root;
        StartCoroutine("Break");
    }

    /// <summary>
    /// Break the object
    /// </summary>
    /// <returns></returns>
    IEnumerator Break()
    {
        yield return new WaitForSeconds(delay);
        broken = true;
        
        col.enabled = false;
        parent.gameObject.renderer.enabled = false;
    }

}
