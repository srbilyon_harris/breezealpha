using UnityEngine;
using System.Collections;

/// <summary>
///  Base class for Togglable Classes
/// </summary>
public class Togglable : Triggerable {
	
	public bool toggled;
	
	public override void Activate () {
		Toggle(true);
	}
	
	public override void Deactivate () {
		Toggle(false);
	}
	
	public virtual void Toggle (bool toggle) {
		toggled = toggle;
	}
}
