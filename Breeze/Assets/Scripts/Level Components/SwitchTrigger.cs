using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

/// <summary>
/// The SwitchTrigger.cs includes the functionality for making a switch object, 
/// as well as having including a feature for having an on/off indicator.
/// 
/// The switch has to have an object to activate that inherits from the togglable class
/// </summary>
public class SwitchTrigger : GenericTrigger {

    /// <summary>
    /// This is the object that will be toggled. Must inherit from the togglable class
    /// </summary>
    public Togglable objectToActivate;

    /// <summary>
    /// ON and OFF indicators
    /// </summary>
    public GameObject offIndicator, onIndicator;

    public bool objectActivated = false;
	
	void Update () 
    {
        if (IsPlayerInVolume())
        {
            if (objectActivated && (InputHandler.b1 || InputHandler.jb1))
            {
                objectActivated = false;
            }
            else
                if (!objectActivated && (InputHandler.b1 || InputHandler.jb1))
                {
                    objectActivated = true;
                }
        }

        if (objectActivated)
        {
            offIndicator.SetActive(false);
            onIndicator.SetActive(true);
        }
        else
        {
            offIndicator.SetActive(true);
            onIndicator.SetActive(false);
        }

        objectToActivate.toggled = objectActivated;
	}
}
