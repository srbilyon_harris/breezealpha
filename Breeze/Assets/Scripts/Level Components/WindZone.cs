using UnityEngine;
using System.Collections;

public class WindZone : GenericTrigger {

    public Vector3 direction;
    public enum TriggerType { Push, Lift }
    public TriggerType triggerType = TriggerType.Push;


    public override void Update()
    {
        if (IsPlayerInVolume())
            RunVolume();

        CheckTrigger();
    }

    public void RunVolume()
    {
        if (triggerType == TriggerType.Push)
        {
            var character = player.gameObject.GetComponent<Character>();
            character.Rush(direction, 3);
        }
        else
        {
            var character = player.gameObject.GetComponent<Character>();
            character.Upward(direction.y);
        }
    }
    #region Old
    //void OnTriggerStay(Collider hit)
    //{
    //    if (triggerType == TriggerType.Push)
    //    {
    //        if (hit.gameObject.tag == "Player")
    //        {
    //            Debug.Log("Hello");
    //            var character = hit.gameObject.GetComponent<Character>();
    //            character.Rush(direction, 3);
    //        }
    //    }
    //    else
    //    {
    //        if (hit.gameObject.tag == "Player")
    //        {
    //            Debug.Log("Bye!");
    //            var character = hit.gameObject.GetComponent<Character>();
    //            character.Upward(direction.y);
    //        }
    //    }
    //}
    #endregion
}
