/*
 * Moving Platform
 * Creates a moving platform that moves between [x#] of points
 * 
 */

using UnityEngine;
using System.Collections;

/// <summary>
/// This component is for making a platform that will move along several points
/// </summary>
public class MovingPlatform : Togglable
{
    public enum PlatformType { Normal, SwitchActivated }
    public PlatformType platformType = PlatformType.Normal;

    public Transform[] points;
    public Transform platform;
    
    /// <summary>
    /// A trigger that will parent all those who enter it.
    /// </summary>
    public GameObject container;
    public float speed = 2;
    public float waitTime = 2;
    private int amountOfPoints;
    public int currentPoint = 0;
    public bool activated;

    void Start()
    {
        amountOfPoints = points.Length;

        switch (platformType)
        {
            case PlatformType.Normal:
                toggled = true; break;
            case PlatformType.SwitchActivated:
                toggled = false; break;
        }       
    }

    void Update()
    {
        if (toggled)
        {
            var platformDirection = Vector3.MoveTowards(platform.transform.position, points[currentPoint].position, speed * Time.deltaTime);
            platform.transform.position = platformDirection;

            if (platform.transform.position == points[currentPoint].position)
                StartCoroutine("SwitchDirection");
        }
    }
    
    /// <summary>
    /// Switch the direction this platform is going to the next waypoint
    /// </summary>
    /// <returns></returns>
    IEnumerator SwitchDirection()
    {
        yield return new WaitForSeconds(waitTime);
        StopCoroutine("SwitchDirection");
        currentPoint++;

        if (currentPoint == amountOfPoints)
            currentPoint = 0;
    }
}
