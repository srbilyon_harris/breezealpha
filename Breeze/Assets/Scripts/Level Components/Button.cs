using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
    Animation anim;	// Animation component
    public int minimumToPress = 1;	// Minimum number of buttonPushers to activate
    int buttonPushers = 0;	// Number of current buttonPushers

    public bool pressed;	// If the button has been pressed
    public bool canUse = true;	// If the button can be used

    public string[] tags;	// Tags of objects allowed to push the button

    public string upAnimName;	// Name of up animation
    public string downAnimName;	// Name of down animation

    public GameObject[] target;	// Target GameObject[s] with Togglable component

    public int maxUses = -1;	// Maximum number of uses, -1 for infinite

    public float delayBeforeReset = 0; // Time before switch should be allowed to be pressed again
    public float delayTimer;	// Timer for reset

    public bool autoReleaseOnly = false;	// If the button only releases automatically
    public float autoReleaseTime = 0; // Time before switch automatically releases
    public float autoTimer;	// Timer for autoRelease

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animation>();	// Get the Animation component
    }

    // Update is called once per frame
    void Update()
    {
        if (autoReleaseTime > 0)
        {
            if (autoTimer > 0)
            {
                autoTimer -= Time.deltaTime;
            }
            else
            {
                autoTimer = 0;
                if (pressed)
                {
                    ButtonRelease();
                }
            }
        }

        if (delayBeforeReset > 0)
        {
            if (delayTimer > 0)
            {
                delayTimer -= Time.deltaTime;
            }
            else
            {
                delayTimer = 0;
                ToggleUsability(true);
            }
        }

        if (buttonPushers >= minimumToPress)
        {	
            // If the buttonPushers reaches the minimumToPress, ButtonPress
            if (!pressed)
            {
                if (canUse)
                {
                    ButtonPress();
                }
            }
        }
        else
        {									
            // If buttonPushers is less than minimumToPress and button was pressed, ButtonRelease
            if (!autoReleaseOnly)
            {
                if (pressed)
                {
                    ButtonRelease();
                }
            }
        }
    }

    // When a button pusher enters the trigger area
    void OnTriggerEnter(Collider other)
    {
        if (CheckTag(other.tag))
        {	// If the tag matches an allowed tag, button gets pressed
            StepOnButton();
        }
    }

    // When a button pusher leaves the trigger area
    void OnTriggerExit(Collider other)
    {
        if (CheckTag(other.tag))
        {	// If the tag matches an allowed tag, button gets released
            StepOffButton();
        }
    }

    // Player steps on Button
    void StepOnButton()
    {
        buttonPushers++;	// Increment buttonPushers count
    }

    // Player steps off Button
    void StepOffButton()
    {
        buttonPushers--;	// Deincrement the buttonPushers
    }

    // Button press
    void ButtonPress()
    {
        if (maxUses != 0)
        {	// If there are uses available
            if (autoReleaseTime > 0)
            {
                autoTimer = autoReleaseTime;
            }

            pressed = true;
            anim.Play(downAnimName);
            for (int i = 0; i < target.Length; i++)
            {	// Send Toggle messages to targets
                target[i].GetComponent<Triggerable>().SendMessage("Activate");
            }

            if (maxUses > 0)
            {	// Subtract a use
                maxUses--;
            }
        }
    }

    // Button release
    void ButtonRelease()
    {
        pressed = false;
        anim.Play(upAnimName);

        for (int i = 0; i < target.Length; i++)
        {	// Send Toggle messages to targets
            target[i].GetComponent<Triggerable>().SendMessage("Deactivate");
        }

        if (delayBeforeReset > 0)
        {
            delayTimer = delayBeforeReset;
            ToggleUsability(false);
        }
    }

    // Checks the tag of a potential buttonPusher
    bool CheckTag(string tagToCheck)
    {
        bool check = false;
        for (int i = 0; i < tags.Length; i++)
        {	// Loop through allowed tags and compare
            if (tagToCheck == tags[i])
            {
                check = true;
                break;
            }
        }
        return check;
    }

    void ToggleUsability(bool toggle)
    {
        canUse = toggle;
    }
}
