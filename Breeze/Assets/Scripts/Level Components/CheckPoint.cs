﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour
{

    GameObject player;
    //GaleCheckPointManager dT;

    public Transform point;
    public Transform tempPoint;
    public string playerName;

    public void Start()
    {
        if (GameObject.Find(playerName))
            player = GameObject.Find(playerName);
    }

    //Stores whatever walks into one of these fields into "hit"
    void OnTriggerEnter(Collider hit)
    {
        if (hit.transform.tag == playerName)
        {
            Debug.Log(playerName + " Detected");
        }
    }

}
