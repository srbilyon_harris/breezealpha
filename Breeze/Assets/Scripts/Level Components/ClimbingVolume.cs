using UnityEngine;
using System.Collections;

public class ClimbingVolume : GenericTrigger {

    public bool playerInTrigger = false;
	
	// Update is called once per frame
    public override void Update() 
    {
        if (IsPlayerInVolume())
        {
            if (!playerInTrigger && (InputHandler.b1 || InputHandler.jb1) )
            {
                playerInTrigger = true;
                
                //Cancel out any movement when entering this trigger
                player.GetComponent<Character>().direction.x = 0;
                player.GetComponent<Character>().direction.y = 0;

                player.GetComponent<Character>().status = Character.Status.climbing;
                player.GetComponent<Character>().characterState = Character.CharacterState.climbing;
            }
            else
                if (playerInTrigger && (InputHandler.b1 || InputHandler.jb1))
            {
                playerInTrigger = false;

                player.GetComponent<Character>().status = Character.Status.none;
                player.GetComponent<Character>().characterState = Character.CharacterState.falling;

            }
        }
        else if  (!IsPlayerInVolume() && playerInTrigger)
        {
            playerInTrigger = false;

            player.GetComponent<Character>().status = Character.Status.none;
            player.GetComponent<Character>().characterState = Character.CharacterState.falling;
        }




	}
}
