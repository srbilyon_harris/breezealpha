/*
 * Parenting Trigger
 * Attach this to the trigger object of a platform
 * 
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Attach this to the trigger object of a platform
/// </summary>
public class ParentingTrigger : GenericTrigger 
{
    public Transform parentObject;
    public bool wall = false;

    public override void Update()
    {
        if (IsPlayerInVolume())
            RunVolume();
        else
            if (!IsPlayerInVolume() && ObjectsInTrigger.Contains(player)) 
            PlayerLeftTrigger();

        CheckTrigger();
    }

    public void RunVolume()
    {
        var capturedObj = player.gameObject.transform;
        capturedObj.parent = parentObject;
    }

    public void PlayerLeftTrigger()
    {
        player.gameObject.transform.parent = null;
        player.GetComponent<Character>().status = Character.Status.none;
        //ObjectsInTrigger.Remove(player);

    }

    #region Old
    /// <summary>
	/// Check and see if the object is a player
	/// </summary>
	/// <param name="other"></param>
    //void OnTriggerEnter (Collider other) 
    //{
    //        if (other.gameObject.tag == "Player")
    //        {
    //            //other.GetComponent<Character>().status = Character.Status.locked;
    //            var capturedObj = other.gameObject.transform;
    //            capturedObj.parent = parentObject;
    //        }
    //    //else
    //    //{
    //    //    var capturedObj = other.gameObject.transform.root;
    //    //    capturedObj.parent = parentObject;
    //    //}
    //}

    ///// <summary>
    ///// UnParent the object once it exits
    ///// </summary>
    ///// <param name="other"></param>
    //void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.transform.parent == parentObject)
    //    {
    //        other.gameObject.transform.parent = null;
    //        other.GetComponent<Character>().status = Character.Status.none;
    //    }
    //}
    #endregion
}
