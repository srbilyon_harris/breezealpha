using UnityEngine;
using System.Collections;

/// <summary>
/// A trigger that can be used to move from one room to another
/// </summary>
public class RoomTrigger : GenericTrigger
{
    public string levelToLoad;
    public Main main;
    public int goToLocationNumber;
    public float fakeTimer = 0;
    public bool canChangeRooms = false;

    void Start()
    {
        main = GameObject.Find("GameManager").GetComponent<Main>();
    }

    void Update()
    {
        //Check to see if the player can move to the next room
        if (IsPlayerInVolume() && !canChangeRooms)
        {
            StartCoroutine("ChangeRooms");
        }

        fakeTimer += 1;

        if (canChangeRooms)
        {
            if (fakeTimer > 120)
            {
                Application.LoadLevel(levelToLoad);
            }
        }
    }

    void ChangeRooms()
    {
        //Store the location in the room you want to be transported to.
        main.locationNumber = goToLocationNumber;

        //Store the name of the level
        //gameManager.dhInstance.currentLevel = levelToLoad;
        //main.saver.Save("Assets/Save/Save.sav");
        Camera.mainCamera.GetComponent<IrisController>().irisState = IrisController.IrisState.zoomOut;

        ///TODO: Bad Practice
        GameObject.Find("Player").GetComponent<Character>().status = Character.Status.staggered;
        GameObject.Find("Player").GetComponent<HeadLookController>().theTarget = null;
        GameObject.Find("Player").GetComponent<HeadLookController>().canTrack = false;

        //Temporarily pause everything that is using time
        Time.timeScale = 0;

        fakeTimer = 0;
        canChangeRooms = true;
    }

    IEnumerator LoadTheLevel()
    {
        AsyncOperation async = Application.LoadLevelAsync(levelToLoad);
        yield return async;
        Debug.Log("Loading complete");
    }
}