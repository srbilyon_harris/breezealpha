﻿/*
 * Health Pickup
 * Allows for picking up health objects
 * 
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Allows for picking up health objects
/// </summary>
public class HealthPickup : MonoBehaviour
{
    /// <summary>
    /// The sound that will play when this object is picked up
    /// </summary>
    public AudioClip audioClip;
    public int healthGain = 1;

    /// <summary>
    /// This is a state that tells if the object is floating up or down
    /// </summary>
    public enum Direction { Up, Down };
    public Direction direction = Direction.Up;

    /// <summary>
    /// This is a state of the object that dictates it's idle behavior
    /// </summary>
    public enum ObjectBehavior { Standard, Raising, Moving }
    public ObjectBehavior objectBehavior = ObjectBehavior.Standard;

    /// <summary>
    /// The move direction of this item
    /// </summary>
    Vector3 moveDirection;

    /// <summary>
    /// Assigns the audio clip to a default one
    /// </summary>
    void Start()
    {
        audioClip = (AudioClip)Resources.Load("Sounds/Breeze/BreezeCollectRaindrop01");
        audio.clip = audioClip;
        moveDirection.y = 0.03f;
    }
    /// <summary>
    /// The switch case for making the object move based on it's specifed behavior
    /// </summary>
    void Update()
    {
        switch (objectBehavior)
        {
            case ObjectBehavior.Standard:
                
                    // Save the y position prior to start floating (maybe in the Start function):
                    if (direction == Direction.Up)
                    {
                        moveDirection.y = 0;
                        StartCoroutine("FloatingUp");
                    }
                    else if (direction == Direction.Down)
                    {
                        moveDirection.y = 0;
                        StartCoroutine("FloatingDown");
                    }

                    break;

            case ObjectBehavior.Raising:

                    moveDirection.y = 2f * Time.smoothDeltaTime;
                    break;       
        }

       // Put the floating movement in the Update function:
       transform.position = new Vector3(transform.position.x, moveDirection.y + transform.position.y, transform.position.z);

   }

    #region Floating
    /// <summary>
    /// Make the object float up
    /// </summary>
    /// <returns></returns>
    IEnumerator FloatingUp()
    {
        moveDirection.y += 0.08f * Time.smoothDeltaTime;
        yield return new WaitForSeconds(3);
        direction = Direction.Down;
    }

    /// <summary>
    /// Make the object float down
    /// </summary>
    /// <returns></returns>
    IEnumerator FloatingDown()
    {
        moveDirection.y -= 0.08f * Time.smoothDeltaTime;
        yield return new WaitForSeconds(3);
        direction = Direction.Up;
    }
    #endregion

    /// <summary>
    /// If a player runs into this instance, collect and destroy this
    /// </summary>
    /// <param name="hit"></param>
    /// <returns></returns>
    IEnumerator OnTriggerEnter(Collider hit)
    {
        foreach (GameObject me in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (me == this)
            {
                var player = me.GetComponent<Player>();

                //playerStats = other.GetComponent<Stats>();
                player.health += healthGain;

                audio.Play();
                gameObject.GetComponent<Renderer>().renderer.enabled = false;
                gameObject.GetComponent<Collider>().enabled = false;

                yield return new WaitForSeconds(audioClip.length);
                Destroy(gameObject);
            }
        }
    }
}
