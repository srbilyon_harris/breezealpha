using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

/// <summary>
/// This is a basic prompt trigger that will show a notification
/// on the screen. This is useful for giving the player an indiction for if
/// an object is usable, etc.
/// </summary>
public class PromptTrigger : GenericTrigger 
{
    public GameObject trigger;
    public GameObject notification;
    public string notificationSound;

    public bool triggered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (transform.root.GetComponent<DamageReciever>())
        {
            if (transform.root.GetComponent<DamageReciever>().dead)
                transform.root.gameObject.SetActive(false);
        }

        if (IsPlayerInVolume())
        {
            ShowNotification(true);
            
            if (player.GetComponent<Character>().characterState == Character.CharacterState.grounded && player.GetComponent<Character>().targetSpeed == 0)
            player.GetComponent<HeadLookController>().theTarget = this.transform;

            if (!triggered)
            {
                triggered = true;
                PlaySound(notificationSound);
            }
        }
        else
        {
            if (player.GetComponent<HeadLookController>().theTarget == this.transform)
                player.GetComponent<HeadLookController>().theTarget = null;

            ShowNotification(false);
            triggered = false;
        }
	}

    void ShowNotification(bool enable)
    {
        if (notification != null)
        notification.SetActive(enable);
    }

    /// <summary>
    /// Plays a sound.
    /// </summary>
    /// <param name="sound"></param>
    void PlaySound(string sound)
    {
        if (audio != null)
        {
            audio.clip = (AudioClip)Resources.Load("Sounds/" + sound);

            if (!audio.isPlaying)
                audio.Play();
        }
    }
}
