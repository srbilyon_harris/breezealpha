/*
 * Breakable.cs
 * Create a breakable object
 * 
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Create a breakable object
/// </summary>
public class Breakable : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The Health of the object
    /// </summary>
    public float health;

    /// <summary>
    /// The amount of time it takes for this platform to break
    /// </summary>
    public float delay = 0;

    /// <summary>
    /// You can assign object(s) to be spawned once this object is destroyed
    /// </summary>
    public GameObject[] itemsToDrop;
    public GameObject[] pieces;

    public Vector3 force;
    /// <summary>
    /// This is the box that the player will need to hit in order for it to be
    /// damaged. Make sure that the hit box is big enough
    /// </summary>
	public Collider hitbox;
    
    /// <summary>
    /// The state of the object
    /// </summary>
    private bool broken = false;
    #endregion

    /// <summary>
    /// Apply damage to this object
    /// </summary>
    /// <param name="damage"></param>
	public void ApplyDamage(int damage)
	{
		health -= damage;
	}
	
    /// <summary>
    /// Check the health of the object, and prepare to break it 
    /// once health <= 0
    /// </summary>
	void Update()
	{
	if (health <= 0)
        {
            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                delay = 0;
                if (!broken)
                {
                    Break();
                }
            }
        }	
	}

    /// <summary>
    /// Grab all of the pieces in the broken object, and deactivate them
    /// </summary>
    void Start()
    {
        int i = 0;
        for (i = 0; i < pieces.Length; i++)
        {
            pieces[i].SetActive(false);
        }
    }


    /// <summary>
    /// Drop items when this object is destroyed
    /// </summary>
    void DropItems()
    {
        int i = 0;
        for (i = 0; i < itemsToDrop.Length; i++)
        {
            GameObject item = Instantiate(itemsToDrop[i], transform.position, transform.rotation) as GameObject;
            item.name = itemsToDrop[i].name;
        }
    }
	
	#region Destruction
    /// <summary>
    /// Break this instance
    /// </summary>
    void Break()
    {
        broken = true;
        collider.enabled = false;
        gameObject.renderer.enabled = false;
        //SwitchPieces();
        //GetForce();
        //ApplyForce(force);

        if (itemsToDrop != null)
        {
            DropItems();
        }
    }

    /// <summary>
    /// Switch the object with another object
    /// </summary>
    void SwitchPieces()
    {
        gameObject.SetActive(false);
        int i = 0;
		
        for (i = 0; i < pieces.Length; i++)
        {
            pieces[i].SetActive(false);
        }
    }

    /// <summary>
    /// If the broken object has pieces, shoot them everywhere
    /// </summary>
    /// <param name="force"></param>
    void ApplyForce(Vector3 force)
    {
        int i = 0;
        for (i = 0; i < pieces.Length; i++)
        {
            pieces[i].rigidbody.AddForce(force);
        }
    }
	#endregion
}