using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour 
{

    public Transform pointA;
    //public Transform pointB;

    // Use this for initialization
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            //other.GetComponent<Character>().status = Character.Status.swimming;
            if (other.GetComponent<Character>().characterState == Character.CharacterState.none)
                return;

            Input.ResetInputAxes();

            other.transform.position = pointA.position;
            other.transform.rotation = pointA.rotation;
            //Debug.Log("Player FOUND!");
            //var splash = (GameObject)Instantiate(Resources.Load("Splash"));
            //splash.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 2, other.transform.position.z);
            //StartCoroutine("Ripples", other.transform.position);
        }
    }

    void Unlock()
    {

    }

    // Use this for initialization
    void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            Input.ResetInputAxes();

            //   other.GetComponent<Character>().status = Character.Status.swimming;
            // StopCoroutine("Ripples");
            //other.GetComponent<Character>().characterState = Character.CharacterState.stationary;

            //var splash = (GameObject)Instantiate(Resources.Load("Splash"));
            //splash.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 2, other.transform.position.z);
        }
    }
  
}
