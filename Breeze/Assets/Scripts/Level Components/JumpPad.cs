using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

/// <summary>
/// Attach this to a trigger if you want the player to be able to jump on it
/// </summary>
public class JumpPad : GenericTrigger
{
    #region Members
    public float jumpHeight = 10.0f;
    public string soundDirectory;
    public bool dropItem;
    #endregion

    #region Methods
    public override void Awake()
    {
        player = GameObject.Find("Player");
        base.Awake();
    }

    public override void Update()
    {
        base.Update();
        if (ObjectsInTrigger.Contains(player)) RunVolume();
    }

    public void RunVolume()
    {
        player.GetComponent<Character>().Upward(jumpHeight);
        player.GetComponent<Character>().characterState = Character.CharacterState.jumping;

        //Play Sound
        PlaySound(soundDirectory);
    }

    /// <summary>
    /// Plays a sound
    /// </summary>
    /// <param name="sound"></param>
    void PlaySound(string sound)
    {
        if (audio != null)
        {
            audio.clip = (AudioClip)Resources.Load("Sounds/" + sound);
            
            if (!audio.isPlaying)
            audio.Play();
        }
    }
    #endregion
}
