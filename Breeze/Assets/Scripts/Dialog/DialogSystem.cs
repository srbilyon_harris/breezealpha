using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Linq;

public struct Speech
{
    public string target;
    public string text;
}

public class DialogSystem : MonoBehaviour {

    XmlNodeList speechNodes;
    XmlDocument doc = new XmlDocument();

	// Use this for initialization
	void Start () 
    {
        try
        {
            doc = new XmlDocument();
            doc.Load("test.xml");
            LoadConvo("Opening");
        }
        catch
        {
            XmlElement root = doc.CreateElement("Dialog");

            XmlElement convo = doc.CreateElement("Convo");
            convo.SetAttribute("Name", "Opening");

            XmlElement speechBox = doc.CreateElement("Speechbox");

            speechBox.InnerText = "howdy! This is test one! wut <PlayAnimation>flip bird</PlayAnimation> no";
            
            convo.AppendChild(speechBox);
            root.AppendChild(convo);
            doc.AppendChild(root);
            doc.Save("text.xml");
        }
        
	
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyUp(KeyCode.Space))
        {            
            
        }
	
	}


    /// <summary>
    /// <Dialog>
    ///     <Convo>
    ///         <Speechbox speaker="person1" >blah blah blah</Speech>
    ///         <Speechbox speaker="person2" >wut <playanimation anim="flip bird"><addSpeechBubble text ="wut"> no</Speech>
    ///     </Convo>
    /// </Dialog>
    /// </summary>
    public void LoadConvo(string convoName)
    {
        XmlNodeList nodes = doc.SelectNodes("/Dialog/Convo");
        foreach (XmlNode node in nodes)
        {
            if (convoName == node.Attributes["Name"].Value)
            {
                speechNodes = node.ChildNodes;
                return;
            }            
        }
        Debug.LogError("Convo" + convoName + " not found!");
    }

    public Speech StartConvo()
    {
        Speech speech;
        speech.target = speechNodes[0].Attributes["Name"].Value;
        speech.text = speechNodes[0].InnerText;
        return speech;
    }



    string ParseXMLFromString(string text)
    {
        List<string> sections = text.Split('<').OfType<string>().ToList();
        bool skip = false;

        foreach (string section in sections)
        {
            if (section.Contains("PlayAnimation"))
            {
                
            }
        }

        return "";
    }
}
