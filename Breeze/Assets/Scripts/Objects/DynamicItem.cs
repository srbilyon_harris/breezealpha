using UnityEngine;
using System.Collections;

/// <summary>
/// Interactive objects will derive from this class.
/// This may become useful for editor scripts to locat items,
/// or we may want to log a number of items, etc.
/// Who knows? Could be cool :>
/// </summary>
public class DynamicItem : MonoBehaviour 
{

}
