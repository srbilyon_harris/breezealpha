using UnityEngine;
using System.Collections;

/// <summary>
/// This is a Dynamic Item (DI for short) that Breeze can fuck with.
/// </summary>
public class DIGrabable : DynamicItem
{

    #region Variables

    private Transform m_GOMainObject;
    private Transform m_GOOldParent;
    bool held = false;

    #endregion

    #region Methods

    /// <summary>
    /// Assigns references to gameobjects so that "getcomponent" methods are not continuously used
    /// </summary>
    void Start()
    {
        this.m_GOMainObject = this.transform.parent.transform;
        this.m_GOOldParent = this.transform.parent.transform.parent;
    }

    /// <summary>
    /// On trigger enter, will send the game object to the pickup so that it can be grabbed
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if(other.tag.ToLower() == "player")
        {
            other.GetComponentInChildren<PickUpItem>().InheritItem(this.m_GOMainObject);
            print("within range " + other.gameObject.name);
        }

    }

    /// <summary>
    /// On trigger exit, remove link to this object
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerExit(Collider other)
    {
        if (other.tag.ToLower() == "player")
        {
            PickUpItem pui = other.GetComponentInChildren<PickUpItem>();
            if (!pui.ItemHeld && pui.ItemInRange)
            {
                pui.DropItem();
            }
            print("out of range ");
        }

    }

    #endregion

}
