using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float life = 3;
    public int damage = 4;

    void Update()
    {
        StartCoroutine("DestroyCall");
        transform.Translate(transform.forward * (Time.deltaTime));
    }

    //IEnumerator CheckForTarget(Character character)
    //{
    //    yield return new WaitForSeconds(0.8f);
    //    character.gameObject.GetComponent<Character>().ApplyDamage(5, transform.forward, 5, 2, 0);

    //}

    IEnumerator DestroyCall()
    {
        yield return new WaitForSeconds(life);
        Destroy(this.gameObject);
        StopCoroutine("DestroyCall");
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Enemy")
        {
            //hit.gameObject.GetComponent<Character>().ApplyDamage(5, -transform.forward, 8, 2, 0);

            GameObject newParticle = (GameObject)Instantiate(Resources.Load("Particles/PunchRed"));
            newParticle.transform.position = transform.position;

            GameObject newParticle2 = (GameObject)Instantiate(Resources.Load("Particles/FireHit"));
            newParticle2.transform.position = transform.position;

            ///TODO: Clean this up with a reference
            if (!hit.gameObject.GetComponent<Character>().guarding)
                hit.gameObject.GetComponent<Character>().sfxManager.PlaySound(hit.gameObject.GetComponent<Character>().sfxManager.hit2);

            Debug.Log("Magic Landed");
            Destroy(this.gameObject);
        }
    }

}
