using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour
{
    public Transform target, player;
    float distance = 10.0f;

    float xSpeed = 1;
    float ySpeed = 1;

    float yMinLimit = -20;
    float yMaxLimit = 80;

    private float x = 0.0f;
    private float y = 0.0f;


    float smoothTime = 0.3f;

    private float xSmooth = 0.0f;
    private float ySmooth = 0.0f;
    private float xVelocity = 0.0f;
    private float yVelocity = 0.0f;

    private Vector3 posSmooth = Vector3.zero;
    private Vector3 posVelocity = Vector3.zero;

    public float zOffset = 3;


    //@script AddComponentMenu("Camera-Control/Mouse Orbit smoothed")

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        // Make the rigid body not change rotation
        if (rigidbody)
            rigidbody.freezeRotation = true;
    }

    // The distance in the x-z plane to the target
    // the height we want the camera to be above the target
    public float height = 5.0f;
    // How much we 
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;

    public float offset;
    void LateUpdate()
    {
        if (target)
        {

            Vector3 gap = target.position - player.position;

            // Calculate the current rotation angles
            var wantedRotationAngle = player.eulerAngles.y;
            var wantedHeight = player.position.y + 2;

            var currentRotationAngle = transform.eulerAngles.y;
            var currentHeight = transform.position.y;

            // Damp the rotation around the y-axis
            currentRotationAngle += Input.GetAxis("Horizontal") * rotationDamping * Time.deltaTime;

            // Damp the height
            currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

            // Convert the angle into a rotation
            var currentRotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, currentRotationAngle, 0), rotationDamping);

            transform.position = player.position;
            transform.position -= (currentRotation * Vector3.forward * distance);

            // Set the height of the camera
            transform.position = new Vector3(transform.position.x, player.position.y + height, transform.position.z);

            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Linecast(player.position, transform.position, out hitInfo, 1))
            {
                float tempDistance = Vector3.Distance(player.position, hitInfo.point);
                //print("Hit!");
                Vector3 position = currentRotation * new Vector3(0.0f, height / 2, -tempDistance - offset) + player.position;
                transform.position = Vector3.Lerp(transform.position, position, 0.5f);
            }
            if (Vector3.Distance(target.position, player.position) > 5 && Vector3.Distance(target.position, player.position) < 30)
            {
                transform.LookAt(target);
            }
            else if (Vector3.Distance(target.position, player.position) >= 30)
            {
                Camera.mainCamera.GetComponent<CameraHandler>().cameraMode = CameraMode.ThirdPerson;
            }
        }
    }

    void CircleStrife()
    {
        xSmooth = Mathf.SmoothDamp(xSmooth, x, ref xVelocity, smoothTime);
        ySmooth = Mathf.SmoothDamp(ySmooth, y, ref yVelocity, smoothTime);

        ClampAngle(ySmooth, yMinLimit, yMaxLimit);

        posSmooth = (target.position + player.position) / 2; // no follow smoothing

        var playerLook = new Vector3(target.position.x, player.position.y, target.position.z);


        player.LookAt(playerLook);
        transform.LookAt(player.position);

        // Calculate the current rotation angles
        var wantedRotationAngle = player.eulerAngles.y;
        var wantedHeight = player.position.y + 2;

        var currentRotationAngle = transform.eulerAngles.y;
        var currentHeight = transform.position.y;

        // Damp the rotation around the y-axis
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
        //currentRotationAngle += Input.GetAxis("Horizontal") * rotationDamping * Time.deltaTime;

        //currentRotationAngle += Mathf.LerpAngle(currentRotationAngle, Input.GetAxis("Horizontal"), rotationDamping * Time.deltaTime);
        // Damp the height
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        // Convert the angle into a rotation
        var currentRotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, currentRotationAngle, 0), rotationDamping);

        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target
        transform.position = player.position;


        transform.position -= (currentRotation * Vector3.forward * distance);


        // Set the height of the camera
        transform.position = new Vector3(transform.position.x, height, transform.position.z);


        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Linecast(player.position, transform.position, out hitInfo, 1))
        {
            float tempDistance = Vector3.Distance(player.position, hitInfo.point);
            //print("Hit!");
            Vector3 position = currentRotation * new Vector3(0.0f, height / 2, -tempDistance - offset) + player.position;
            transform.position = Vector3.Lerp(transform.position, position, 0.5f);
        }

        // Always look at the target
        transform.LookAt(player);

    }

    static void ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        //return Mathf.Clamp (angle, min, max);
    }
}
