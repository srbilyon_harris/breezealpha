using UnityEngine;
using System.Collections;

public class WaypointSettings : MonoBehaviour
{
    public float lerpSpeed = 0.02f;
    public float zoomSpeed = 0.01f;
    public float newDistance = 10f;
    private Renderer rend;

    public void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
    }

}
