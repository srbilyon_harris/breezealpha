using UnityEngine;
using System.Collections;

#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value `false'

public class ThirdPersonCamera : MonoBehaviour
{
    #region Floats
    public float distance = 12.0f;                    // Default Distance 
    public float minDistance = 0.6f;                  // Minimum zoom Distance 
    float maxDistance = 20;                           // Maximum zoom Distance 

    float xSpeed = 50;                                // Orbit speed (Left/Right) 
    float ySpeed = 50.0f;                             // Orbit speed (Up/Down)
    public float rotationDampening = 3.0f;            // Auto Rotation speed (higher = faster) 
    public float targetHeight = 1.7f;                 // Vertical offset adjustment 


    public float offsetFromWall = 0.1f;               // Bring camera away from any colliding objects 
    float xJoySpeed = 200.0f;                         // Orbit speed (Left/Right) 
    float yJoySpeed = 200.0f;                         // Orbit speed (Up/Down)  
    float yMinLimit = -10;                            // Looking up limit 
    float yMaxLimit = 80;                             // Looking down limit 
    public float zoomRate = 40;                       // Zoom Speed 
    float zoomDampening = 5.0f;                       // Auto Zoom speed (Higher = faster) 
    public float xDeg = 0.0f;
    public float yDeg = 0.0f;

    private float currentDistance;
    private float desiredDistance;
    private float correctedDistance;
    #endregion

    public GameObject cameraTarget;

    #region Bool
    public bool activated;
    bool cameraBump = true;
    bool lockToRearOfTarget = false;          // Lock camera to rear of target 
    public bool allowMouseInputX = true;      // Allow player to control camera angle on the X axis (Left/Right) 
    public bool allowMouseInputY = true;      // Allow player to control camera angle on the Y axis (Up/Down) 
    bool allowGamepadInputX = true;           // Allow player to control camera angle on the X axis (Left/Right) 
    bool allowGamepadInputY = true;           // Allow player to control camera angle on the Y axis (Up/Down) 
    public bool rotateBehind = false;
    #endregion

    public Vector3 vTargetOffset;
    Quaternion rotation;
    Vector3 position;

    void Start()
    {
        Camera.mainCamera.GetComponent<ColorCorrectionCurves>().enabled = false;
        Vector3 angles = transform.eulerAngles;
        xDeg = angles.x;

        currentDistance = distance;
        desiredDistance = distance;
        correctedDistance = distance;

        // Make the rigid body not change rotation 
        if (rigidbody)
            rigidbody.freezeRotation = true;

        if (lockToRearOfTarget)
            rotateBehind = true;

        SnapBehindTarget();
    }

    void LateUpdate()
    {
        //Snap the camera to face the forward direction
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetMouseButton(2)  || Input.GetKeyDown(KeyCode.JoystickButton9))
            SnapBehindTarget();

        if (Input.GetAxis("Horizontal") != 0)
        { RotateBehindTarget(); }


        if (!cameraTarget)
            return;

        if (!activated)
            return;

        #region Mouse and Joystick Controls

        //If either mouse buttons are down, let the mouse govern camera position 
        if (GUIUtility.hotControl == 0)
        {
            if (!Input.GetMouseButton(0) || !Input.GetMouseButton(1))
            {
                //Check to see if mouse input is allowed on the axis 
                if (allowMouseInputX)
                {
                    xDeg += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
                }
                // else
                //    RotateBehindTarget();
                if (allowMouseInputY)
                {
                    yDeg -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
                }
                //Interrupt rotating behind if mouse wants to control rotation 
                if (!lockToRearOfTarget)
                    rotateBehind = false;
            }
            #region GamePad
            //if (Input.GetAxis("Horizontal2") != 0 || Input.GetAxis("Vertical2") != 0)
            //{
                //Check to see if mouse input is allowed on the axis 
                if (allowGamepadInputX)
                {
                    xDeg += Input.GetAxis("Horizontal2") * xJoySpeed/100;
                }
                //else
                //    RotateBehindTarget();

                if (allowGamepadInputY)
                {
                    yDeg -= Input.GetAxis("Vertical2") * yJoySpeed/100;
                }
            //}
            #endregion

            //if (Input.GetAxis("Horizontal") != 0)
            //{
            //    RotateBehindTarget();
            //}
        }
        #endregion

        ClampAngle(yDeg, yMinLimit, yMaxLimit);

        // Set camera rotation 
        rotation = Quaternion.Euler(yDeg, xDeg, 0);
        position = Vector3.Slerp(position, cameraTarget.transform.position - (rotation * Vector3.forward * desiredDistance + vTargetOffset), 0.5f);

        //Camera Bumping on surfaces
        if (cameraBump)
            CameraBumping();

        //Finally Set rotation and position of camera 
        transform.rotation = rotation;
        transform.position = position;

    }

    void CameraBumping()
    {
        desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomRate * Mathf.Abs(desiredDistance);
        desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
        correctedDistance = desiredDistance;

        // Calculate desired camera position 
        vTargetOffset = new Vector3(0, -targetHeight, 0);

        // Check for collision using the true target's desired registration point as set by user using height 
        RaycastHit collisionHit;
        Vector3 trueTargetPosition = new Vector3(cameraTarget.transform.position.x, cameraTarget.transform.position.y + targetHeight, cameraTarget.transform.position.z);

        // If there was a collision, correct the camera position and calculate the corrected distance 
        int NPCLayer = 0;
        // This is used to illustrate which Layer we want to use.

        int NPCMask = 1 << NPCLayer;
        // This is used to generate the Mask derived from NPC Layer.

        bool isCorrected = false;
        if (Physics.Linecast(trueTargetPosition, position, out collisionHit, NPCMask))
        {
            // Calculate the distance from the original estimated position to the collision location, 
            // subtracting out a safety "offset" distance from the object we hit.  The offset will help 
            // keep the camera from being right on top of the surface we hit, which usually shows up as 
            // the surface geometry getting partially clipped by the camera's front clipping plane. 
            if (collisionHit.collider.gameObject.layer != 8)
            {
                correctedDistance = Vector3.Distance(trueTargetPosition, collisionHit.point) - offsetFromWall;
                isCorrected = true;
            }
        }

        // For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance 
        currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * zoomDampening) : correctedDistance;

        // Keep within limits 
        currentDistance = Mathf.Clamp(currentDistance, minDistance, maxDistance);

        // Recalculate position based on the new currentDistance 
        position = cameraTarget.transform.position - (rotation * Vector3.forward * currentDistance + vTargetOffset);
    }

    public void RotateBehindTarget()
    {
        float targetRotationAngle = cameraTarget.transform.eulerAngles.y;
        float currentRotationAngle = transform.eulerAngles.y;
        xDeg = Mathf.LerpAngle(currentRotationAngle, targetRotationAngle, rotationDampening * Time.deltaTime);

        // Stop rotating behind if not completed 
        if (targetRotationAngle == currentRotationAngle)
        {
            if (!lockToRearOfTarget)
                rotateBehind = false;
        }
        else
            rotateBehind = true;
    }

    public void SnapBehindTarget()
    {
        if (cameraTarget == null)
            return;

        float targetRotationAngle = cameraTarget.transform.eulerAngles.y;
        float currentRotationAngle = transform.eulerAngles.y;
        xDeg = Mathf.LerpAngle(currentRotationAngle, targetRotationAngle, 4);

        // Stop rotating behind if not completed 
        if (targetRotationAngle == currentRotationAngle)
        {
            if (!lockToRearOfTarget)
                rotateBehind = false;
        }
        else
            rotateBehind = true;
    }


    public void ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;

        if (yDeg > yMaxLimit)
            yDeg = yMaxLimit;
        if (yDeg < yMinLimit)
            yDeg = yMinLimit;


        //return Mathf.Clamp(angle, min, max);
    }

}