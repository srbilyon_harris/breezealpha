using UnityEngine;
using System.Collections;

public enum CameraMode { ThirdPerson, Targeting, Waypoint }
public class CameraHandler : MonoBehaviour 
{
    public CameraMode cameraMode = CameraMode.ThirdPerson;
    public ThirdPersonCamera thirdPerson;
    public Orbit targeting;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (Input.GetKeyDown(KeyCode.Joystick1Button5) || InputHandler.b3 || InputHandler.jb3)
        {
            switch (cameraMode)
            {
                case CameraMode.Targeting: 
                    cameraMode = CameraMode.ThirdPerson;
                    break;
                case CameraMode.ThirdPerson:
                    {
                        if (targeting.target != null)
                        cameraMode = CameraMode.Targeting;
                    }
                    break;
            }
            //cameraMode = CameraMode.Targeting;
        }

        CameraUpdate(); 
	}

    void CameraUpdate()
    {
        switch (cameraMode)
        {
            case CameraMode.Targeting:
                thirdPerson.enabled = false;
                targeting.enabled = true;
                break;
            case CameraMode.ThirdPerson: 
                thirdPerson.enabled = true;
                targeting.enabled = false;
                break;
        }
    }
}
