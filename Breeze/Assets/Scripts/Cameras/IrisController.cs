using UnityEngine;
using System.Collections;

public class IrisController : MonoBehaviour {

    public GameObject iris;
    public float irisMinimum = 0.01f, irisMaximum = 300f;
    public float speed;

    public float zoomPercentage = 0;

    public enum IrisState { none, zoomIn, zoomOut }
    public IrisState irisState = IrisState.none;

    float newX, newY;

	// Use this for initialization
	void OnLevelWasLoaded (int level) 
    {
         //irisState = IrisState.zoomIn;
	}
    void Awake()
    {
        zoomPercentage = irisMinimum;
    }

    void Start()
    {
        irisState = IrisState.zoomIn;
    }
	
	// Update is called once per frame
	void Update () 
    {

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            irisState = IrisState.zoomOut;
        }
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            irisState = IrisState.zoomIn;
        }

        if (irisState == IrisState.zoomOut)
        {
            FadeOut();
        }
        if (irisState == IrisState.zoomIn)
        {
            FadeIn();
        }
        //zoomPercentage = Mathf.Sin(zoomPercentage);
        //iris.transform.localScale = new Vector3(zoomPercentage, zoomPercentage, 1);
        iris.transform.localScale = new Vector3(zoomPercentage, zoomPercentage, 1);

        zoomPercentage = Mathf.Clamp(zoomPercentage, irisMinimum, irisMaximum);
       

	}

    void FadeOut()
    {
         zoomPercentage -= speed;
    }

    void FadeIn()
    {
        
        zoomPercentage += speed ;
    }
}
