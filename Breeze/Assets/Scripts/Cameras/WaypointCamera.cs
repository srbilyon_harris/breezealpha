/* Waypoint Camera
 * Created by SrBilyon Harris - 2012
 * Contributions by Daniel Kinjerski
 * 
 * This script controls the multiple systems that are in the camera system
 */
using UnityEngine;
using System.Collections;

public class WaypointCamera : MonoBehaviour
{
    public string playerName;
    /// <summary>
    /// Used for versus mode
    /// </summary>
    /// 
    #region Floats
    public float distanceFromTarget;
    public float newDistance;
    public float zoomSpeed = 0.01f;
    public float lerpSpeed = 0.02f;
    public float height = 5.0f;
    public float distance = 0.0f;
    public float speed = 1;
    public float offset;

    float currentDistance;
    float largestDistance;
    #endregion

    #region GameObjects
    public GameObject target;
    GameObject closestWaypoint;
    GameObject playerTarget;
    GameObject[] waypoints;

    public Camera theCamera;
    #endregion

    #region Bool
    public bool versusMode = false;
    public bool activated;
    #endregion

    #region Vectors/Quaternion
    Quaternion rotation;

    public Vector3 vDisplacement;
    Vector3 avgDistance;
    #endregion

    // Use this for initialization
    void Start()
    {
        //Make sure the rooms starts off at the right time scale
        //Time.timeScale = 1;

        playerTarget = GameObject.Find(playerName);
        //target = GameObject.Find("Breeze");
    }

    void Update()
    {
        Zoom();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!target)
            return;

        if (!activated)
            return;

        FindClosestWaypoint();
        target = closestWaypoint;

        transform.position = target.transform.position;

        #region Finally Set rotation and position of camera
        if (target.transform.localRotation.eulerAngles.x > 180)
        {
            float tempX = target.transform.localRotation.eulerAngles.x;
            tempX -= 360;
        }
        else if (target.transform.localRotation.eulerAngles.y > 180)
        {
            float tempY = target.transform.localRotation.eulerAngles.y;
            tempY -= 360;
        }
        else if (target.transform.localRotation.eulerAngles.z > 180)
        {
            float tempZ = target.transform.localRotation.eulerAngles.z;
            tempZ -= 360;
        }
        #endregion

        zoomSpeed = target.GetComponent<WaypointSettings>().zoomSpeed;
        lerpSpeed = target.GetComponent<WaypointSettings>().lerpSpeed;
        newDistance = target.GetComponent<WaypointSettings>().newDistance;

       

        transform.position = playerTarget.transform.position - (transform.localRotation * Vector3.forward * distanceFromTarget + vDisplacement);

        Quaternion flatVersion;
        flatVersion = Quaternion.Euler(target.transform.rotation.eulerAngles.x, target.transform.rotation.eulerAngles.y, 0f);
        transform.rotation = Quaternion.Slerp(transform.rotation, flatVersion, Time.smoothDeltaTime * .5f);
        distanceFromTarget = Mathf.Lerp(distanceFromTarget, newDistance, zoomSpeed);
    }
    /// <summary>
    /// Finds the closest waypoint.
    /// </summary>
    /// <returns>
    /// The closest waypoint.
    /// </returns>
    public GameObject FindClosestWaypoint()
    {
        // Find all game objects with tag "Waypoint"
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");

        var distance = Mathf.Infinity;
        //var position = transform.position; 

        // Iterate through them and find the closest one
        foreach (GameObject go in waypoints)
        {
            var diff = (go.transform.position - playerTarget.transform.position);
            var curDistance = diff.sqrMagnitude;

            if (curDistance < distance)
            {
                closestWaypoint = go;
                distance = curDistance;
            }
        }
        return closestWaypoint;
    }

    void Zoom()
    {
        if (Input.GetKey(KeyCode.Minus))
        {
            distanceFromTarget -= .1f;
        }
        if (Input.GetKey(KeyCode.Equals))
        {
            distanceFromTarget += .1f;
        }


        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            distanceFromTarget -= .1f;
        }
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            distanceFromTarget += .1f;
        }
    }


}
