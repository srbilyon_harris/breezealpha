using UnityEngine;
using System.Collections;

public class Culling : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
    float[] distances = new float[32];
	    // Set up layer 10 to cull at 15 meters distance.
	    /*Layer #*/ distances[13] = /*Distance*/ 200;
        /*Layer #*/ distances[8] = /*Distance*/ 200;
        /*Layer #*/ distances[0] = /*Distance*/ 200;
        /*Layer #*/ distances[1] = /*Distance*/ 200;
        /*Layer #*/ distances[9] = /*Distance*/ 200;
        /*Layer #*/ distances[10] = /*Distance*/ 200;

	    camera.layerCullDistances = distances;
	}

} 	
