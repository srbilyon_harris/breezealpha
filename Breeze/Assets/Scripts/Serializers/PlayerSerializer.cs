/*
 * Player Serializer
 * This is a serializer which will serialize player data
 * This is merely a standpoint that holds basic data for the player
 * 
 */
using UnityEngine;
using System.Collections;

using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Runtime.Serialization;
using System.Reflection;

/// <summary>
/// This will create a SavaData instance for player 1
/// </summary>
[Serializable()]
public class PlayerSerializer
{
    public SaveData RetrievePlayer1Info(SaveData data)
    {
        Player pRef = GameObject.Find(data.playerName).GetComponent<Player>();
        Debug.Log(pRef.name);

        #region Current Stats after Adjustments
        data.p1d = new MainPlayer();
        data.p1d.health = pRef.health; 
        data.p1d.maxHealth = pRef.maxHealth;

        data.p1d.stamina = pRef.stamina;
        data.p1d.maxStamina = pRef.maxStamina;

        data.p1d.strength = pRef.strength;
        data.p1d.defense = pRef.defense;
        //data.p1d.agility = pRef.agility;

        #endregion
        return data;
    }
}

[Serializable()]
public class MainPlayer : ISerializable
{
    #region Values that will need to be saved
    public int playerID;

    public int health, maxHealth, stamina, maxStamina, strength, defense;
    public int agility, levitate, jump;

    public int level, currentExp, levelUpExp;

    /// <summary>
    /// The character's stats with only leveling adjustments
    /// </summary>
    public int flatExp, flatAtk, flatDef, flatSta, flatHp;

    //Influences the growth of stats
    public int baseExp, baseAtk, baseDef, baseSta, baseHp;
   
    /// <summary>
    /// Adjustments made after equipping items
    /// </summary>
    public int adjMaxHealth, adjMaxStamina, adjStr, adjDef, adjAgi, adjJump;
    //public int money;

    //public string playerName;	//player name?
    //public string area;
    #endregion

    public MainPlayer() { }

    public MainPlayer(SerializationInfo info, StreamingContext ctxt)
    {
        #region Current Stats after adjustments
        health = (int)info.GetValue("health", typeof(int));
        maxHealth = (int)info.GetValue("maxHealth", typeof(int));
        stamina = (int)info.GetValue("stamina", typeof(int));
        maxStamina = (int)info.GetValue("maxStamina", typeof(int));

        level = (int)info.GetValue("level", typeof(int));
        currentExp = (int)info.GetValue("currentExp", typeof(int));
        levelUpExp = (int)info.GetValue("levelUpExp", typeof(int));

        strength = (int)info.GetValue("strength", typeof(int));
        defense = (int)info.GetValue("defense", typeof(int));
        agility = (int)info.GetValue("agility", typeof(int));
        levitate = (int)info.GetValue("levitate", typeof(int));
        jump = (int)info.GetValue("jump", typeof(int));

        adjAgi = (int)info.GetValue("adjAgi", typeof(int));
        adjDef = (int)info.GetValue("adjDef", typeof(int));
        adjJump = (int)info.GetValue("adjJump", typeof(int));
        adjMaxHealth = (int)info.GetValue("adjMaxHealth", typeof(int));
        adjMaxStamina = (int)info.GetValue("adjMaxStamina", typeof(int));
        adjStr = (int)info.GetValue("adjStr", typeof(int));


        flatExp = (int)info.GetValue("flatExp", typeof(int));
        flatAtk = (int)info.GetValue("flatAtk", typeof(int));
        flatDef = (int)info.GetValue("flatDef", typeof(int));
        flatSta = (int)info.GetValue("flatSta", typeof(int));
        flatHp = (int)info.GetValue("flatHp", typeof(int));
        #endregion

    }
    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        #region Current Stats after Adjustments
        info.AddValue("health", this.health);
        info.AddValue("maxHealth", this.maxHealth);
        info.AddValue("stamina", this.stamina);
        info.AddValue("maxStamina", this.maxStamina);

        info.AddValue("level", this.level);
        info.AddValue("currentExp", this.currentExp);
        info.AddValue("levelUpExp", this.levelUpExp);

        info.AddValue("strength", this.strength);
        info.AddValue("defense", this.defense);
        info.AddValue("agility", this.agility);
        info.AddValue("levitate", this.levitate);
        info.AddValue("jump", this.jump);

        info.AddValue("adjAgi", this.adjAgi);
        info.AddValue("adjDef", this.adjDef);
        info.AddValue("adjJump", this.adjJump);
        info.AddValue("adjMaxHealth", this.adjMaxHealth);
        info.AddValue("adjMaxStamina", this.adjMaxStamina);
        info.AddValue("adjStr", this.adjStr);

        info.AddValue("flatExp", this.flatExp);
        info.AddValue("flatAtk", this.flatAtk);
        info.AddValue("flatDef", this.flatDef);
        info.AddValue("flatSta", this.flatSta);
        info.AddValue("flatHp", this.flatHp);

        #endregion
    }

    public void LoadMainPlayer(SaveData data)
    {
        Player pd = GameObject.Find(data.playerName).GetComponent<Player>();
        pd.health = data.p1d.health;
        pd.maxHealth = data.p1d.maxHealth;
        pd.stamina = data.p1d.stamina;
        pd.maxStamina = data.p1d.maxStamina;

        pd.strength = data.p1d.strength;
        pd.defense = data.p1d.defense;
        //pd.agility = data.p1d.agility;   
    }
}

