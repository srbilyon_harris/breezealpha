using UnityEngine;
using System.Collections;

/// <summary>
/// A Debugging script for this player instance
/// </summary>
public class PlayerDebug : MonoBehaviour
{
    public Player target;
    public GameObject cameraTarget;

    public bool ShowGUI = false;
    string camMode = "",  playerHealth = "",  playerStamina = "",  playerExperience = "", playerLevel = "", playerMoney = "";

    bool showPlayerDebug = true, showControlsDebug = false, activateCutscene = false, lockMouse = true;


    void Start()
    {
        foreach (GameObject me in GameObject.FindGameObjectsWithTag("Player"))
        {
            //if (me.GetComponent<PhotonView>().isMine)
                target = me.GetComponent<Player>();
        }

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
            showPlayerDebug = !showPlayerDebug;

        if (Input.GetKeyDown(KeyCode.Alpha1))
            showControlsDebug = !showControlsDebug;

        if (Input.GetKeyDown(KeyCode.LeftControl) )
        {
            lockMouse = !lockMouse;
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            //lockMouse = true;
        }
    }

    void OnGUI()
    {
        if (ShowGUI)
        {
            GUI.TextArea(new Rect(550, 10, 400, Screen.height / 30), "Press '1' for controls | Left Ctrl: Toggle Cursor | F12: Fullscreen");
        }

        if (Input.GetKeyDown(KeyCode.F12))
        {
            // Switch to the desktop resolution in fullscreen mode.
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
            Screen.fullScreen = !Screen.fullScreen;
        }

        if (ShowGUI)
        {
            ShowControlsDebug();
            ShowPlayerDebug();
            //Screen.showCursor = false;
        }

        if (lockMouse)
            Screen.lockCursor = true;
        else 
            Screen.lockCursor = false;

        if (Input.GetKeyDown(KeyCode.Backspace))
            Application.LoadLevel("Splash Screen");
    }
    void ShowControlsDebug()
    {
        if (showControlsDebug)
        {
            GUI.TextArea(new Rect(10, Screen.height - 300, Screen.width / 4, Screen.height / 3),
                " NEW: Move Camera with Mouse \n" +
            " WASD/Arrows: Movement \n K: Select ['!' Prompts] \n L/Spacebar: Jump \n J,I /Mouse Buttons (1-2): Attack \n Q: Look Ahead [Camera] "
            + "\n Backspace: Restart"
            + "\n \n 2: Show Player Information \n 3: Target Enemy " );

         }
    }
    void ShowPlayerDebug()
    {
        if (showPlayerDebug)
        {
            playerHealth = "Health: " + target.health + "/" + target.maxHealth + "\n";
            playerStamina = "Stamina: " + target.stamina + "/" + target.maxStamina + "\n";

            playerLevel = "Level: " + target.level + "\n";
            //camMode = "Camera Mode: " + camManager.camMode.ToString();
            GUI.TextArea(new Rect(10, 40, 200, 150), playerHealth + playerStamina + playerLevel + playerExperience + camMode);
        }
    }
}

