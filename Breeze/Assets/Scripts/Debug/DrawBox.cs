using UnityEngine;
using System.Collections;

public class DrawBox : MonoBehaviour
{
    public bool drawGUIBox = true;
    public enum BoxColor { Red, Blue, Green, Yellow, White, Black }
    public BoxColor boxColor = BoxColor.Red;
    float selectedOpacity = 0.5f;
    float unselectedOpacity = 0.5f;
    float opacity;

    void ColorCheck()
    {
        switch (boxColor)
        {
            case BoxColor.Red:
                Gizmos.color = new Color(1, 0, 0, opacity);
                break;

            case BoxColor.Blue:
                Gizmos.color = new Color(0, 0, 1, opacity);
                break;

            case BoxColor.Green:
                Gizmos.color = new Color(0, 1, 0, opacity);
                break;

            case BoxColor.Yellow:
                Gizmos.color = Color.yellow;
                break;
            case BoxColor.White:
                Gizmos.color = new Color(1, 1, 1, opacity);
                break;
            case BoxColor.Black:
                Gizmos.color = new Color(0, 0, 0, opacity);
                break;
        }
    }

    void OnDrawGizmosSelected()
    {
        opacity = selectedOpacity;
        ColorCheck();
        if (drawGUIBox)
        Gizmos.DrawCube(transform.position, transform.collider.bounds.size);
    }

    void OnDrawGizmos()
    {
        opacity = unselectedOpacity;
        ColorCheck();
        if (drawGUIBox)
        Gizmos.DrawCube(transform.position, transform.collider.bounds.size);
    }

}