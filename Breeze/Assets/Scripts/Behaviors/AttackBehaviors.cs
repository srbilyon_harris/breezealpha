///*
// * Attack Behaviors
// * Contains severals different behaviors for controlling the attack patterns for AI 
// * 
// */
//using UnityEngine;
//using System.Collections;

//public class AttackBehaviors : MonoBehaviour {

//    public Enemy self;
//    Attacks attack;
    

//    // Use this for initialization
//    void Start () 
//    {
//        self = GetComponent<Enemy>();
//    }
	
//    #region Behaviors
//    public void SeekAndAttack()
//    {
//        var behavior = self.behavior;

//        self.target = self.combat.GetTarget();
//        var target = self.target;

//        if (self.status == Character.Status.flinching)
//        {
//            self.Stop();
//            return;
//        }

//        #region Attack
//        if (behavior == Enemy.Behavior.seek)
//        {
//            //If I'm not too close, follow
//            if (self.GetTargetDistance(target.position) >= self.minDistance)
//            {
//                if (self.trapped)
//                {
//                    self.steer.FaceTarget(target.position);
//                    self.steer.SteeringRays();
//                    self.Stop();
//                }
//                else
//                {
//                    if (self.controller.isGrounded)
//                        self.steer.Seek(target.position);
//                }
//            } //If I'm too close, just look
//            else if (self.GetTargetDistance(target.position) < self.minDistance || self.trans.position.y + 3f < target.position.y)
//            {
//                if (!self.attacking)
//                {
//                    attack = gameObject.AddComponent<Jab>();
//                    StartCoroutine(self.combat.MeleeAttack(attack) );
//                    Destroy(attack); 
//                }
                
//                self.Stop();
//            }
//        #endregion

//        }

//        if (self.health <= 40)
//            self.ChooseCombatParadigm(Enemy.BattleType.FleeAndRange);
//    }

//    /// <summary>
//    /// Sets the Steering Behavior mode to 'flee'
//    /// Makes the AI track the target and attack when close, as well as shoot
//    /// </summary>
//    public void FleeAndRanged()
//    {
//        self.behavior = Enemy.Behavior.flee;
//        self.target = self.combat.GetTarget();
//        //var target = self.target;

//        if (self.behavior == Enemy.Behavior.flee)
//        {
//            //If I'm not too close, follow
//            if (self.GetTargetDistance(self.target.position) < 10 && self.GetTargetDistance(self.target.position) >= 3)//self.safeDistance)
//            {
//                if (self.trapped)
//                {
//                    self.steer.SteeringRays();
//                    self.Stop();

//                    attack = gameObject.AddComponent<Jab>();
//                    StartCoroutine(self.combat.MeleeAttack(attack));
//                    Destroy(attack); 

//                }
//                else
//                {
//                    if (self.controller.isGrounded)
//                        self.steer.Flee(self.target.position);
//                }
//            } ///Throw a projectile when in range
//            else if (self.GetTargetDistance(self.target.position) > 3/*self.minDistance*/  || self.trans.position.y + 3f < self.target.position.y)
//            {

//                self.steer.FaceTarget(self.target.position);

//                if (!self.attacking)
//                {
//                    attack = gameObject.AddComponent<Jab>();
//                    StartCoroutine(self.combat.ProjectileAttack(attack));
//                    Destroy(attack);
//                }

//                self.Stop();
//            } ///If too close to the target, perform a basic melee attack
//            else if (self.GetTargetDistance(self.target.position) < 3)
//            {
//                if (!self.attacking)
//                {
//                    attack = gameObject.AddComponent<Jab>();
//                    StartCoroutine(self.combat.MeleeAttack(attack));
//                    Destroy(attack);
//                }
//            }
//        }
//    }

//    /// <summary>
//    /// Sets the Steering behavior mode to 'none'
//    /// Makes the AI track the target and shoot when close
//    /// </summary>
//    public void StayAndShoot()
//    {
//        self.behavior = Enemy.Behavior.none;
//        self.target = self.combat.GetTarget();

//        if (self.GetTargetDistance(self.target.position) > 3/*self.minDistance*/  || self.trans.position.y + 3f < self.target.position.y)
//        {

//            self.steer.FaceTarget(self.target.position);

//            if (!self.attacking)
//            {
//                attack = gameObject.AddComponent<Jab>();
//                StartCoroutine(self.combat.ProjectileAttack(attack));
//                Destroy(attack);
//            }

//            self.Stop();
//        }
//    }
//    #endregion
//}
