using UnityEngine;
using System.Collections;

public class PickUpItem : MonoBehaviour
{

    #region Variables

    private Transform m_tParent;
    private Transform m_tItem;
    private Animation anim;

    #endregion

    #region Properties

    /// <summary>
    /// Property to avoid running extra methods when there is no need
    /// </summary>
    public bool ItemInRange
    {
        get
        {
            return (m_tItem != null);
        }
    }
    
    /// <summary>
    /// Property to determine if we are holding an item
    /// </summary>
    public bool ItemHeld
    {
        get
        {
            if (this.m_tItem != null)
            {
                return (this.m_tItem.parent == this.m_tParent);
            }
            else
            {
                return false;
            }
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Reference to the parent so that we don't continously use "getcomponent"
    /// </summary>
    void Start()
    {
        anim = this.animation;
        this.m_tParent = this.gameObject.transform;
    }

    /// <summary>
    /// When our player gets in range of a dynamic object, we will select it so that we can parent it to our player
    /// </summary>
    /// <param name="_selectedItem"></param>
    public void InheritItem(Transform _selectedItem)
    {
         this.m_tItem = _selectedItem;
         print("prepared to grab item");
    }

    /// <summary>
    /// This is the actual acquisition of the item. Will parent it to our anchor and animate it upwards.
    /// </summary>
    public void GrabItem()
    {
        if (!anim.IsPlaying("PickUp"))
        {
            anim["PickUp"].speed = 1.0f;
            anim.Play("PickUp");
        }
        else
        {
            return;
        }

        // parenting
        this.m_tItem.parent = this.m_tParent;
        this.m_tItem.localPosition = Vector3.zero;
        Rigidbody rb = this.m_tItem.GetComponent<Rigidbody>();
        if(rb != null)
        {
            rb.isKinematic = true;
        }
        

        // picking up item
        print("srbiggon needs to put an anim here, but for now we'll just position it automatically above.");
    }

    /// <summary>
    /// We remove our reference after we play the "removal" animation
    /// </summary>
    public void DropItem()
    {
        // dropping item 
        print("srbiggon needs to put an anim here, but for now we'll just position it infront and drop.");
        if (!anim.IsPlaying("PickUp"))
        {
            anim.Play("PickUp");
        }
        else
        {
            return;
        }

        //if (this.m_tItem == null)
        //    return;

        Rigidbody rb = this.m_tItem.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = false;
        }

        // This may not be safe....
        try
        {
            m_tItem.parent = this.m_tParent.transform.parent.transform.parent;
        }
        catch
        {
            this.m_tItem.parent = null;
        }

        RemoveItem();
    }

    /// <summary>
    /// removes the selected item
    /// </summary>
    public void RemoveItem()
    {
        this.m_tItem = null;
    }
    #endregion
}
