/* 
 * A class for controlling the steering behaviors of AI characters
 * TODO: Create a variable for manipulating wander direction change variance
 * TODO: Make the wandering of the character change when hitting a dead end
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value `false'

public class SteeringBehaviors : MonoBehaviour
{
    #region Members [Object References]
    public Enemy character;
    public Transform trans;
    #endregion

    #region Members [bools]
    private bool left = false;
    public bool right = false;
    public bool front = false; 
    public bool isSteering;

    public bool canJump = true;
    #endregion

    #region Members [Vector3]
    public Vector3 adjDirection;    
    public List<Vector3> steeringRays;
    public Vector3 steering;
    private Vector3 _holdTheJump;// = dir.y;

    public Vector2 wanderPoint;
    #endregion

    #region Members [Raycast]
    private RaycastHit leftRayHit;
    private RaycastHit rightRayHit;
    private RaycastHit frontRayHit;
    #endregion

    #region Members [floats]
    /// <summary>
    /// Behavior collision check smoothing variables
    /// </summary>
    private float rayLength = 1.5f, percision = 50;
    #endregion

    public Transform[] points;
    public Transform platform;
    public float speed = 2;
    public float waitTime = 2;
    private int amountOfPoints;
    public int currentPoint = 0;

    public void Awake()
    {
        character = GetComponent<Enemy>();
        //trans = character.transform;
        steering = new Vector3(1f, 1f, 1f);

        if (points != null)
        amountOfPoints = points.Length;

    }

    public void Start()
    {
        if (character.charType == Character.CharacterType.ally)
            this.gameObject.tag = "Ally";
        if (character.charType == Character.CharacterType.enemy)
            this.gameObject.tag = "Enemy";
    }

    public void SteeringRays()
    {
        Vector3 adjPos = new Vector3(trans.position.x, trans.position.y, trans.position.z) + adjDirection;
        List<Vector3> steeringRays = new List<Vector3>();

        #region Create and Draw Steering Rays
        steeringRays.Add(transform.TransformDirection(-steering.x, steering.y, steering.z)); //ray pointed slightly left 
        steeringRays.Add(transform.TransformDirection(steering.x, steering.y, steering.z)); //ray pointed slightly right 
        steeringRays.Add(transform.forward); //ray 1 is pointed straight ahead

        Debug.DrawRay(adjPos, steeringRays[0].normalized * rayLength, Color.cyan);
        Debug.DrawRay(adjPos, steeringRays[1].normalized * rayLength, Color.cyan);
        Debug.DrawRay(adjPos, steeringRays[2].normalized * rayLength, Color.cyan);
        #endregion

        RaycastHit hit;

        if (Physics.Raycast(adjPos, steeringRays[2], out hit, rayLength) && !hit.collider.isTrigger)
        {
            if (hit.collider.gameObject.layer != 13 && (!right && !left) && !hit.collider.isTrigger)
            {
                Debug.DrawLine(transform.position, hit.point, Color.red);
                isSteering = true;
                front = true; right = false; left = false;
                frontRayHit = hit;
                character.trapped = false;
            }
            else
                if (hit.collider.gameObject.layer == 13)
                    character.trapped = true;
        }
        else
            if (Physics.Raycast(adjPos, steeringRays[0], out hit, rayLength) && !hit.collider.isTrigger)
            {
                if (hit.collider.gameObject.layer != 13 && (!front && !left))
                {
                    Debug.DrawLine(transform.position, hit.point, Color.red);
                    isSteering = true;
                    front = false; right = false; left = true;
                    rightRayHit = hit;
                    character.trapped = false;
                }
                else
                    if (hit.collider.gameObject.layer == 13)
                        character.trapped = true;
            }
            else
                if (Physics.Raycast(adjPos, steeringRays[1], out hit, rayLength) && !hit.collider.isTrigger)
                {
                    if (hit.collider.gameObject.layer != 13 && (!front && !left)) //Character layer
                    {
                        Debug.DrawLine(transform.position, hit.point, Color.red);
                        front = false; right = true; left = false;
                        isSteering = true;
                        leftRayHit = hit;
                        character.trapped = false;
                    }
                    else
                        if (hit.collider.gameObject.layer == 13)
                            character.trapped = true;
                }
                else
                {
                    isSteering = false;
                    left = false; right = false; front = false;
                    character.trapped = false;

                }
    }

    Vector3 ObstacleAvoidance(Vector3 dir)
    {
        SteeringRays();

        if (front)
        {
            dir.y = _holdTheJump.y;

            if (canJump)
            character.Upward(15);

            character.Rush(trans.forward,5);
            return trans.forward;
        }
        else
            if (left)
            {
                trans.forward += Vector3.Reflect(trans.forward, leftRayHit.normal) * Time.smoothDeltaTime;
                return trans.forward;

            }
            else
                if (right)
                {
                    trans.forward += Vector3.Reflect(trans.forward, rightRayHit.normal) * Time.smoothDeltaTime;
                    return trans.forward;

                }
                else if ((!left && !right && !front))
                {
                    isSteering = false;
                    left = false; right = false; front = false;
                }
        return trans.forward;
    }

    public bool Seek(Vector3 targetPos)
    {
        var tempDir = (targetPos - trans.position);
        var holdY = character.direction.y;

        character.direction = new Vector3(tempDir.x, 0, tempDir.z).normalized;
        character.direction.y = holdY;
        character.speed = character.targetSpeed;

        //if (character.controller.isGrounded)
        //    character.jumping = false;

        //If not to close, but not to far
        if ((tempDir.magnitude > character.minDistance))
        {
            //Check for potential pitfalls. Stop if there is a pitfall ahead
            RaycastHit hit;
            bool onEdge = false;
            Vector3 edgePostion = new Vector3();

            #region Ground Ahead!
            if (Physics.Raycast(trans.position + trans.forward, Vector3.down, out hit, 10) && !hit.collider.isTrigger)
            {
                var adjDir = ObstacleAvoidance(character.direction);

               character.targetSpeed = character.runSpeed;

               #region Obstacle Avoidance
               if (!isSteering)
               {
                   character.direction = new Vector3(character.direction.x * character.targetSpeed, character.direction.y, character.direction.z * character.targetSpeed);
                   trans.forward += new Vector3(character.direction.x, 0, character.direction.z).normalized * Time.smoothDeltaTime * 5f;
               }
               else if (isSteering)
               {
                   character.direction = new Vector3(adjDir.x * character.targetSpeed, character.direction.y, adjDir.z * character.targetSpeed);
                   trans.forward += new Vector3(character.direction.x, 0, character.direction.z).normalized * Time.smoothDeltaTime * 5f;
               }
               #endregion

               if (!onEdge)
                    character.trans.forward += new Vector3(tempDir.x, 0, tempDir.z) * Time.deltaTime;

                if (onEdge && trans.position != edgePostion)
                    onEdge = false;
            }
            #endregion

            #region No Ground
            if (!Physics.Raycast(trans.position + trans.forward, Vector3.down, out hit, 10))
            {
                FaceTarget(targetPos);
                character.Stop();

                onEdge = true;
                
                edgePostion = new Vector3(trans.position.x, 0, trans.position.z);

                if (onEdge)
                    FaceTarget(targetPos);

                if (!character.jumping)
                {
                    //character.Upward(15);
                    //character.Rush(trans.forward, 10);
                    character.jumping = true;
                }
            }
            #endregion

            //return false;
        }
        return true;
    }

    public bool Flee(Vector3 targetPos)
    {
        var tempDir = (trans.position - targetPos);
        var holdY = character.direction.y;

        character.direction = new Vector3(tempDir.x, 0, tempDir.z).normalized;
        character.direction.y = holdY;
        character.speed = character.targetSpeed;

        //if (character.controller.isGrounded)
        //    character.jumping = false;

        //If not to close, but not to far
        if (tempDir.magnitude < character.safeDistance)
        {
            //Check for potential pitfalls. Stop if there is a pitfall ahead
            RaycastHit hit;
            bool onEdge = false;
            Vector3 edgePostion = new Vector3();
			
			//#region Ground Ahead!
            if (Physics.Raycast(trans.position + trans.forward, Vector3.down, out hit, 10))
            {
                character.targetSpeed = character.runSpeed;

                if (!onEdge)
                    character.trans.forward += new Vector3(tempDir.x, 0, tempDir.z) * Time.deltaTime;

                if (onEdge && trans.position != edgePostion)
                    onEdge = false;
                //	            return false;
            }
            if (!Physics.Raycast(trans.position + trans.forward, Vector3.down, out hit, 10))
            {
                //FaceTarget(targetPos);
                character.Stop();
                onEdge = true;
                edgePostion = new Vector3(trans.position.x, 0, trans.position.z);

                if (onEdge)
                    FaceTarget(targetPos);
                //character.trans.forward += new Vector3(-tempDir.x, 0, -tempDir.z) * Time.deltaTime;


                if (!character.jumping)
                {
                    character.jumping = true;
                }
            }
            //      		return false;
        }

        return true;
    }

    public void Idle()
    { }

    Vector2 CreateNewWanderPoint()
    {
        var ranX = Random.Range(-100, 100);
        var ranZ = Random.Range(-100, 100);
        wanderPoint = new Vector2(ranX, ranZ);
        return new Vector2(ranX, ranZ);
    }

    IEnumerator ChooseNewWanderPoint()
    {
        yield return new WaitForSeconds(5f);
        StopCoroutine("ChooseNewWanderPoint");
        CreateNewWanderPoint();
    }

    public void Wander()
    {
        var tempDir = (new Vector3(wanderPoint.x + trans.position.x,
                       trans.position.y, wanderPoint.y + trans.position.z) - trans.position);
        var holdY = character.direction.y;

        character.direction = new Vector3(tempDir.x, 0, tempDir.z).normalized;
        character.direction.y = holdY;
        character.speed = character.targetSpeed/3;

        //if (character.controller.isGrounded)
        //    character.jumping = false;

            //Check for potential pitfalls. Stop if there is a pitfall ahead
            RaycastHit hit;

            #region Ground Ahead!
            if (Physics.Raycast(trans.position + trans.forward, Vector3.down, out hit, 10))
            {
                //var adjDir = ObstacleAvoidance(character.direction, new Vector3(2f, 1, 2f), character.checkObstacles);
                var adjDir = ObstacleAvoidance(character.direction);

                character.targetSpeed = 2;


                if (!isSteering)
                {
                    character.direction = new Vector3(character.direction.x * character.targetSpeed, character.direction.y, character.direction.z * character.targetSpeed);
                    trans.forward += new Vector3(character.direction.x, 0, character.direction.z).normalized * Time.smoothDeltaTime * 5f;
                }
                else if (isSteering)
                {
                    character.direction = new Vector3(adjDir.x * character.targetSpeed, character.direction.y, adjDir.z * character.targetSpeed);
                    trans.forward += new Vector3(character.direction.x, 0, character.direction.z).normalized * Time.smoothDeltaTime * 5f;
                }
            }
            #endregion
            else if (!Physics.Raycast(trans.position + trans.forward, Vector3.down, out hit, 10) )
            #region There is a pit!
            {
                FaceTarget(tempDir);
                //				character.targetSpeed = 0; 
                //				character.direction.x = 0; 	character.direction.z = 0;
                character.Stop();
                character.trapped = true;

                if (!character.jumping)
                {
                    //character.Upward(10);	
                    character.jumping = true;
                }
            }
            #endregion
    }

    public void FaceTarget(Vector3 target)
    {
        Vector3 foe = target;

        var tempDir = new Vector3(foe.x, 0, foe.z);//Camera.mainCamera.transform.forward * enemy.transform.position.z + enemy.transform.position.x * Camera.mainCamera.transform.right;
        character.trans.LookAt(new Vector3(tempDir.x, character.trans.position.y, tempDir.z));
    }

    /* 
     * This area is for Following Path AI
     * 
     */

    public void FollowPath()
    {
        var platformDirection = Vector3.MoveTowards(platform.transform.position, points[currentPoint].position, speed * Time.deltaTime);
        platform.transform.position = platformDirection;

        if (platform.transform.position == points[currentPoint].position)
            StartCoroutine("SwitchDirection");
    }

    IEnumerator SwitchDirection()
    {
        yield return new WaitForSeconds(waitTime);
        StopCoroutine("SwitchDirection");
        currentPoint++;

        if (currentPoint == amountOfPoints)
            currentPoint = 0;
    }
}
