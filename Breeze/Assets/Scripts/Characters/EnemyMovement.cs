using UnityEngine;
using System.Collections;

/** AI controller specifically made for the spider robot.
 *  
 */
public class EnemyMovement : MonoBehaviour
{
    #region Memebers [Floats]
    public float yVelocity;

    /** Minimum velocity for moving */
    public float sleepVelocity = 0.4F;

    /** Speed relative to velocity with which to play animations */
    public float animationSpeed = 0.2F;
    #endregion

    #region Members [Vector]
    Vector3 dir;
    public Vector3 force, velocity;
    public Rigidbody rigid;
    #endregion

    #region Memebers [Bools]
    public bool reachedTarget = false;
    public bool isGrounded = false;
    #endregion

    #region Members [Objects]
    public Animation anim;
    public Enemy character;
    public Transform target;

    public bool canMove, canSearch;

    /** Effect which will be instantiated when end of path is reached.
     * \see OnTargetReached */
    // public GameObject endOfPathEffect;
    #endregion

    public new void Start()
    {
        character = GetComponent<Enemy>();
        rigid = GetComponent<Rigidbody>();
        trans = GetComponent<Transform>();
    }

    /// <summary>
    /// Check for the current movement state of a character
    /// </summary>
    public void StateMachine()
    {
        switch (character.status)
        {
            case Character.Status.none:
                //MovementState();
                break;
            case Character.Status.attacking:
                character.characterState = Character.CharacterState.attacking;
                break;
            case Character.Status.flinching:
                character.characterState = Character.CharacterState.flinching;
                break;
            case Character.Status.dead:
                character.characterState = Character.CharacterState.dead;
                break;
        }

        //Keep this instance from moving
        if (character.status == Character.Status.flinching || character.status == Character.Status.attacking || character.status == Character.Status.dead)
        { canMove = false; }
    }

    /// <summary>
    /// Checks if this instance is either falling (airborne) or grounded
    /// This also affects this instance's animations
    /// </summary>
    public void MovementState()
    {
        if (isGrounded)
            character.characterState = Character.CharacterState.grounded;
        else
            character.characterState = Character.CharacterState.falling;

        switch (character.characterState)
        {
            case Character.CharacterState.falling:
                character.characterState = Character.CharacterState.falling;
                break;

            case Character.CharacterState.grounded:
                if (dir != Vector3.zero)
                    character.moveState = Character.MoveState.Moving;
                else
                    character.moveState = Character.MoveState.Stationary;
                break;
        }
    }

    /// <summary>
    /// Face the Target
    /// </summary>
    /// <param name="target"></param>
    public void RotateTowards(Vector3 target)
    {
        Vector3 foe = target;
        var tempDir = new Vector3(foe.x, 0, foe.z);

        transform.LookAt(new Vector3(tempDir.x, transform.position.y, tempDir.z));
    }

    Vector3 targetDirection;

    /// <summary>
    /// Move this instance towards it's target
    /// </summary>
    public void MoveToTarget()
    {
        if (!character.combat.CanSeeTarget())
        {
            RotateTowards(target.position);
            return;
        }
        else
        {
            //Move this instance towards it's target
            if (canMove)
            {
                RaycastHit hit;

                #region Ground Ahead!
                if (Physics.Raycast(transform.position + transform.forward, Vector3.down, out hit, 10f))
                {
                    RotateTowards(target.position);
                    //transform.position += transform.forward * 5 * Time.deltaTime;
                    rigid.velocity = transform.forward * 5;
                }
                //There was a small grap ahead, but just move me forward anyway
                else if (Physics.Raycast(transform.position + transform.forward * 2, Vector3.down, out hit, 10f))
                {
                    character.rigidbody.velocity = (transform.forward * 200);
                }
                else
                {
                    RotateTowards(target.position);
                }
                #endregion
            }
        }

        #region Old Astar Stuff
        ////If this instance can't see it's target, then don't proceed
        //if (!character.combat.CanSeeTarget())
        //{
        //    //Rotate towards targetDirection (filled in by CalculateVelocity)
        //    if (targetDirection != Vector3.zero)
        //    {
        //        RotateTowards(targetDirection);
        //    }
        //    canSearch = false;
        //    return;
        //}
        //else
        //{
        //    canSearch = true;
        //    //Move this instance towards it's target
        //    if (canMove)
        //    {
        //        ////Calculate desired velocity
        //        //dir = CalculateVelocity(GetFeetPosition());

        //        ////Rotate towards targetDirection (filled in by CalculateVelocity)
        //        //if (targetDirection != Vector3.zero)
        //        //{
        //        //    RotateTowards(targetDirection);
        //        //}

        //        ////Apply the movement
        //        //Vector3 offset = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);

        //        //if (!Physics.Raycast(offset, transform.forward, 2))
        //        //{
        //        //    transform.position += dir * Time.deltaTime;

        //        //    var fwd = transform.TransformDirection(Vector3.forward);
        //        //    Vector3 t; t = transform.position;

        //        //    //Vector3 up = new Vector3(transform.position.x, transform.position.y+3, transform.position.z);
        //        //    Debug.DrawRay(new Vector3(t.x, t.y + 2, t.z), fwd * 2, Color.green);
        //        //}
        //        //else
        //        //{
        //        //    var fwd = transform.TransformDirection(Vector3.forward);
        //        //    Vector3 t; t = transform.position;

        //        //    Debug.DrawRay(new Vector3(t.x, t.y + 2, t.z), fwd * 2, Color.red);
        //        //}
        //    }
        //}
        #endregion
    }

    void Update()
    {
        ///TODO: For Photon Enemy State Machine
        StateMachine();
        //Send a raycast down to see if there is anything underneath
        isGrounded = Physics.Raycast(transform.position, -transform.up, 1);


        BaseMovement();
    }

    public Vector3 direction;
    public Transform trans;
    public float gravity, speed, targetSpeed, maxSpeed, accelerationRate;

    protected virtual void Gravity()
    {
        //If not on the ground, assume normal gravity

        //RaycastHit hitCast;
        Vector3 down = new Vector3(trans.position.x, trans.position.y + 1f, trans.position.z);
        //Debug.DrawLine(trans.position, down, Color.cyan);

        if (!isGrounded)
        {
            //isGrounded = false;
            RaycastHit hit;
            Debug.DrawLine(trans.position, down, Color.cyan);

            if (!Physics.Raycast(trans.position, Vector3.down, out hit, 1f))//&& !jumping
            {

            }

            direction.y -= (direction.y > -gravity) ? gravity * Time.deltaTime : 0;
            
            //if (direction.y < 0 && jumping)
            //{
            //    //falling = true;
            //    //jumping = false;
            //}
        }
        else
        {
            //if (!hitCast.collider.isTrigger)
            //{
                //isGrounded = true;
                if (direction.y != 0 )//&& falling
                {
                    //falling = false;
                    //direction.y = 0;
                }
                //if (!jumping && !falling)
                //{
                //    direction.y = 0;
                //}
            //}

        }

    }

    /// <summary>
    /// Basic movment function
    /// </summary>
    /// <param name="input">Direction (x,z)</param>
    /// <param name="speed">Speed (0 - 1) where 1 is max speed</param>
    /// <returns>true if we are moving false if we are not</returns>
    public virtual bool BaseMovement()
    {
        //build our movement vector
        direction = new Vector3(dir.x, direction.y, dir.z);

        //prevent snapping forward
        if (direction.x == 0 && direction.z == 0)
            direction = new Vector3(trans.forward.x, direction.y, trans.forward.z);

        //are we accelerationg or decelerating?
        targetSpeed = (speed != 0) ? maxSpeed : 0;

        if (speed > 0.9f)
            trans.forward = new Vector3(direction.x, 0, direction.z);


        //Lerping of speed, etc.
        //////////////////////////////////////
        //Catch the current vertical movement (for jumping/falling)
        var _holdTheJump = direction.y;

        //Don't want our character pointing up if we jump
        direction.y = 0;

        //direction only want direction at this point
        direction.Normalize();

        if (Vector3.Dot(direction, trans.forward) < .5f)
            trans.forward = Vector3.Lerp(trans.forward, direction, 1 / speed);
        else
            trans.forward = new Vector3(direction.x, 0, direction.z);

        //This is our "friction"
        //speed = Mathf.Lerp(speed, targetSpeed, accelerationRate);

        var vel = (direction) * speed;

        velocity = Vector3.Lerp(velocity, vel, 5 * accelerationRate * accelerationRate * Time.deltaTime);

        //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
        direction = (speed > .9f) ? new Vector3(direction.x * speed, _holdTheJump, direction.z * speed)
                                              : new Vector3(trans.forward.x * speed, _holdTheJump, trans.forward.z * speed);

        velocity.y = _holdTheJump;
        direction = velocity;
        
        //apply gravity
        //if (character.status != Character.Status.flinching)
        Gravity();

        //Allows for knockback and rush to work
        if (force.magnitude > 1)
        {
            force *= Mathf.Lerp(force.magnitude, 0, .03f) / force.magnitude;
            //rigidbody.velocity = (force);
        }
        else if (force.magnitude > 0)
            force = Vector3.zero;

        //float currentHeight = 0;
        //if (_holdTheJump > 0)
        //    currentHeight = trans.position.y;

        //force.y = _holdTheJump;
        //controller.Move(direction * Time.deltaTime);
        //rigid.velocity = new Vector3(direction.x,-gravity,direction.z);
        rigid.velocity = direction + force;    

        //if (currentHeight == trans.position.y)
        //    direction.y = 0;

        //are we still moving?
        return (speed == 0) ? false : true;
    }

}
