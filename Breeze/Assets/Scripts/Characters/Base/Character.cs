/* Character
 * Created by SrBilyon Harris - 2013
 * 
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure; // Required in C#


#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value `false'

public abstract class Character : MonoBehaviour
{
    /// <summary>
    /// This variable will keep a character from moving or attacking if false
    /// Affects: 
    /// </summary>
    public bool activated = true;

    #region Variables

    #region Object References
    /// <summary>
    /// Get the static Pause Manager
    /// </summary>
    public PauseManager pauseManager;

    /// <summary>
    /// The mesh that will be flashed when hit should be assigned here
    /// </summary>
    public SkinnedMeshRenderer body;

    /// <summary>
    /// The character controller of this instance
    /// </summary>
    public CharacterController controller;
    public Transform trans, target;

    /// <summary>
    /// The animation component of this instance
    /// </summary>
    public Animation anim;

    /// <summary>
    /// The animation manager of the player
    /// </summary>
    public AnimationManager animManager;

    /// <summary>
    /// The hit and attack boxes of this instance
    /// </summary>
    //public Collider hitbox, attackbox;

    /// <summary>
    /// Weapon Slots
    /// </summary>
    //public GameObject shield, weapon;

    /// <summary>
    /// The initial color/tint of the character from start
    /// </summary>
    private Color normalColor;

    public ParticleManager particleManager;
    public SFXManager sfxManager;
    #endregion

    #region Members [Timers]
    /// <summary>
    /// Character Stats - Current Inflicted State
    /// </summary>
    public float flinchTime = 0.5f, maxFlinchTime = 0.5f;

    /// <summary>
    /// This is a threshold that keeps a character from flinching if an attack isn't that powerful
    /// </summary>
    protected int flinchTolerance = 1;

    public float attackRate, attackCooldown, castLength, castCooldown = 2;

    #endregion

    #region Members [Enums]

    /// <summary>
    /// This contains a set of states that can be used for both the enemy and character child classes
    /// </summary>
    public enum Status { none, pushing, staggered, flinching, swimming, dead, climbing, attacking, gliding, locked, walljumped }
    public Status status = Status.none;

    /// <summary>
    /// This contains a set of infliction states that affect the character in certain ways
    /// </summary>
    public enum Infliction { none, poisoned, frozen, shocked, dizzy }
    public Infliction infliction = Infliction.none;

    /// <summary>
    /// This contains a set of states that dicate whether a character is a player, enemy, etc
    /// </summary>
    public enum CharacterType { none, player, ally, enemy }
    public CharacterType charType = CharacterType.none;

    /// <summary>
    /// Need primarly for specific state checking, this helps in assuring the corrected animations are being played
    /// </summary>
    public enum CharacterState
    {
        none, landing, falling, grounded, dead, flinching, casting, animating, gliding, jumping, crouching, crawling,
        climbing, swimming, pushing, ledgehanging, sliding, attacking, wallclimbing
    }

    public CharacterState characterState;

    /// <summary>
    /// Checks to see if this instance is moving or if it's stationary
    /// </summary>
    public enum MoveState { Stationary, Moving }
    public MoveState moveState = MoveState.Stationary;

    /// <summary>
    /// Checks to see which direction this instance is moving towards
    /// </summary>
    public enum MoveDirection { None, Up, Down, Left, Right }
    public MoveDirection mDirection = MoveDirection.None;

    /// <summary>
    /// The attack state of the character. Checks if on ground, or in air.
    /// </summary>
    public enum AttackState { melee, casting, projectile }
    public AttackState attackState = AttackState.melee;

    public enum ClimbState { none, ledge, wall }
    public ClimbState climbState = ClimbState.none;
    #endregion

    #region Members [Ints]
    /// <summary>
    /// The current stats after the adjustments.
    /// </summary>
    public int health = 1, maxHealth = 80, stamina = 1, maxStamina = 1, level = 1, strength = 1, defense = 1, jumpHeight = 15;
    public int priorityLevel = 0, castLevel = 0, cryType = 1;
    /// <summary>
    /// Adjustments made after equipping items
    /// </summary>
    #endregion

    #region Members [Booleans]
    /// <summary>
    /// Related to wallclimbing or ledgehanging
    /// </summary>
    protected bool torsoHit, topHit, wallClimbing, checkObstacles = true;

    /// <summary>
    /// Combat oriented variables. Detects current attacking state. [Might end up an enum, idk]
    /// </summary>
    public bool guarding = false, ledgehanging = false, crouching = false;
    public bool trapped = false, slammed = false, invincible = false, jumping = false;
    public bool pushing = false, canJump = true; public bool attacking;
    public bool hitTerminalVelocity = false, sprinting = false, weak = false, tired = false;
    #endregion

    #region Members [Ledgehanging]
    public float torsoOffset = 0, topOffset = 0;
    #endregion

    #region Members [Sliding]
    private float rayDistance;
    private Vector3 contactPoint;
    private RaycastHit hit;
    #endregion

    #region Members [Vector3]
    public Vector3 direction, force;
    #endregion

    #region Members [Float]
    /// <summary>
    /// Protected members 
    /// </summary>
    public float speed, walkSpeed = 3, runSpeed = 8, targetSpeed, accelerationSpeed = 1.5f, gravity = 20;
    #endregion

    #endregion

    /// <summary>
    /// Grab out inital transform, controller, and animation
    /// </summary>
    public virtual void Start()
    {
        //Reference to this character's transformation
        trans = this.transform;

        //Reference to the character controller
        controller = GetComponent<CharacterController>();

        //Reference to the animation component
        anim = GetComponent<Animation>();

        //Set my initial status to none
        status = Status.none;

        #region Find the Hitbox and Attackbox attached to this player
        
       
        #endregion

        //Find the animation manager
        animManager = GetComponent<AnimationManager>();

        //Reference the particle Manager
        particleManager = GetComponent<ParticleManager>();

        //Reference to the sound effects manager
        sfxManager = GetComponent<SFXManager>();
    }

    #region Character Physics
    /// <summary>
    /// Applies speed to the direction.
    /// Applies Gravity.
    /// Applies Animation Framework.
    /// </summary>
    public virtual void BasicPhysics()
    {
        //Catch the current vertical movement (for jumping/falling)
        var _holdTheJump = direction.y;

        //Don't want our character pointing up if we jump
        direction.y = 0;

        //We only want direction at this point
        direction.Normalize();

        //This is our "friction"
        speed = Mathf.Lerp(speed, targetSpeed, accelerationSpeed);

        //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
        direction = (direction.magnitude > 1) ? new Vector3(direction.x * speed, _holdTheJump, direction.z * speed)
                                              : new Vector3(trans.forward.x * speed, _holdTheJump, trans.forward.z * speed);
        //Deactive gravity if ledgehanging
        if (!ledgehanging)
        { Gravity(); }

        //Allows for knockback and rush to work
        if (force.magnitude > 1)
        {
            force *= Mathf.Lerp(force.magnitude, 0, .03f) / force.magnitude;

            if (controller != null)
                controller.Move(force * Time.deltaTime);
            else
                rigidbody.velocity = (force);
        }
        else if (force.magnitude > 0)
            force = Vector3.zero;

        //This is caused by attacks that have a slamming characteristic
        if (slammed && controller.isGrounded)
        {
            var slamParticle = particleManager.SpawnParticle(particleManager.slam);
            slamParticle.transform.position = new Vector3(trans.position.x, trans.position.y, trans.position.z + 2);
            slammed = false;
        }

    }

    /// <summary>
    /// Makes the character check for collisions above. Player cannot jump if there is something above
    /// </summary>
    public bool CheckAbove()
    {
        // See if surface above me
        Vector3 rayOrigin = trans.position + new Vector3(0, 2, 0); //Offset above head

        Debug.DrawLine(rayOrigin, rayOrigin + Vector3.up * 2, Color.cyan);

        if (Physics.Raycast(rayOrigin, Vector3.up, out hit, 2))
        {
            Debug.DrawLine(rayOrigin, rayOrigin + Vector3.up * 2, Color.red);
            return true;
        }
        else
        {
            return false;
        }
    }

    #region Sliding Members
    bool sliding = false;
    bool slideWhenOverSlopeLimit = true;
    float slideSpeed = 5;
    float slideLimit = 45;
    #endregion

    /// <summary>
    /// Makes the character slide when on slopes
    /// </summary>
    public void Slide()
    {
        sliding = false;

        // See if surface immediately below should be slid down
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, rayDistance))
        {
            //If I detected another character, don't slide
            if (hit.transform.gameObject.layer == 8)
                return;

            //If the angle of the slope is greater that the slide limit, I'm sliding
            if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                sliding = true;
        }
        //In the case that the slope is really steep: If the above raycast didn't catch anything, check with ControllerColliderHit point
        else
        {
            Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit);

            if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
            {
                if (Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit))
                {
                    //if (hit.transform.gameObject.layer == 8)
                    //    return;

                    sliding = true;
                }
            }
        }

        // If sliding (and it's allowed), or if we're on an object tagged "Slide", get a vector pointing down the slope we're on
        if ((sliding && slideWhenOverSlopeLimit))
        {
            if (hit.transform.gameObject.layer == 8)
                return;

            Vector3 hitNormal = hit.normal;
            Vector3 slideDirection = new Vector3(hitNormal.x, -hit.normal.y, hitNormal.z).normalized;

            Vector3.OrthoNormalize(ref hitNormal, ref direction);

            force = slideDirection * slideSpeed;
            direction = slideDirection;

            if (gameObject.tag == "Player")
                if (anim.Play(animManager.slideAnim) != true)
                    anim.Play(animManager.slideAnim);
        }
    }

    /// <summary>
    /// Basic gravity for characters
    /// </summary>
    protected virtual void Gravity()
    {
        var velocity = direction;

        velocity.y = direction.y;
        direction.y = velocity.y;

        direction.y -= (direction.y > -gravity) ? gravity * Time.deltaTime : 0;
    }

    /// <summary>
    /// Default jump which sets direction.y to 10.
    /// </summary>
    public virtual void Upward()
    {
        //If on the ground and nothing is above
        if (characterState == CharacterState.grounded && !CheckAbove())
        {
            //Make the y direction equal to the jump force
            direction.y = jumpHeight;

            if (status != Status.flinching)
                anim.CrossFade(animManager.jumpAnim, .01f);

            characterState = CharacterState.jumping;
        }

        //iF something is above, cancel additional upward force
        if (CheckAbove())
        {
            direction.y = 0;
            direction.y = -direction.y;
        }
    }

    /// <summary>
    /// Overloaded jump that applies upward force [y axis]
    /// </summary>
    /// <param name="_force">this constitutes the uppward force</param>
    public virtual void Upward(float _force)
    {
        if (((controller != null && controller.isGrounded) || rigidbody != null) && !CheckAbove())
        {
            if (status != Status.flinching)
                anim.CrossFade(animManager.jumpAnim, .01f);

            characterState = CharacterState.jumping;
        }



        if (tag == "Enemy")
        {
            //rigidbody.elo(Vector3.up * _force);
            //rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y * force.y * 10, rigidbody.velocity.z);
            GetComponent<EnemyMovement>().direction.y = _force * 2;
            //var em = GetComponent<EnemyMovement>().direction.y;// = _force * 2;
            //em = Mathf.Lerp(em, em + _force * 2, 0.5f * Time.deltaTime);

            print("Hola");
        }
        else
        {
            direction.y = _force;
        }

    }

    /// <summary>
    /// Sends the character in the specified direction
    /// </summary>
    /// <param name="height">Where is the character going? Specify force in the vector</param>
    public virtual void KnockBack(Vector3 dir)
    {
        force = dir;

        if (tag == "Enemy")
        {
            rigidbody.velocity = (dir);
        }
    }

    /// <summary>
    /// Thrusts the object towards it's target [Adds force]
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="amount"></param>
    public virtual void Rush(Vector3 dir, float amount)
    {
        force = dir * amount;
    }

    /// <summary>
    /// Thrusts the object towards it's target [Adds force]
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="amount"></param>
    public IEnumerator Rush(Vector3 dir, float amount, float time)
    {
        force = dir * amount;
        yield return new WaitForSeconds(time);
        Stop();
    }
    #endregion

    /// <summary>
    /// Stop all movement
    /// </summary>
    public void Stop()
    {
        force = Vector3.zero;
        targetSpeed = 0f; speed = 0f;
        direction.z = 0; direction.x = 0;
    }

    #region Battle Related
    ///// <summary>
    ///// Apply damage to the character. This is used for non-combative attacks
    ///// such as damage triggers
    ///// </summary>
    ///// <param name="damage">amount of damage</param>
    ///// 
    //public virtual void ApplyDamage(int damage, Vector3 foe, float push, float lift)
    //{
    //    //Prevents multiple killings :D
    //    if (status == Status.dead || characterState == CharacterState.dead)
    //        return;

    //    if (tag == "Enemy")
    //    {
    //        rigidbody.AddForce((foe * (push * 1000)));
    //    }

    //    if (guarding)
    //    {
    //        sfxManager.PlaySound(sfxManager.block);
    //        return;
    //    }

    //    //Play the hit sound effect
    //    sfxManager.PlaySound(sfxManager.hit1);

    //    StopCoroutine("FlinchReset");
    //    body.material.SetColor("_Ambient", normalColor);

     

    //    StartCoroutine("FlashCharacter");

    //    characterState = CharacterState.flinching;

    //    status = Status.flinching;

    //    anim.Stop();
    //    anim.Play(animManager.hitAnimPrefix);

    //    speed = 0;
    //    KnockBack(new Vector3(foe.x * push, 0, foe.z * push));

    //    if (lift < 0)
    //    {
    //        slammed = true;
    //    }

    //    Upward(0);
    //    Stop();


    //    if (health <= 0)
    //    {
    //        health = 0;
    //        Die();
    //    }
    //    StartCoroutine("FlinchReset");
    //}

    ///// <summary>
    ///// Overloaded to get the direction of the attacker. Used for when getting attacked by other characters
    ///// The following is used:
    ///// Damage: The amount of damage to inflict on the eneny
    ///// Foe (Vector3): This is used for getting the direction of the foe that attacked this instance
    ///// Push: The direction to push the enemy if hit force is involved
    ///// Lift: The amount of force in the y direction to add to this instance when hit
    ///// 
    ///// Flinch Strength: The "strength" of the attack. This will cause this instance to flinch if this
    ///// value is greater than this instance's flinch tolerence
    ///// </summary>
    ///// <param name="damage"></param>     
    //public virtual void ApplyDamage(int damage, Vector3 foe, float push, float lift, int flinchStr)
    //{
    //    //Prevents multiple "killings" :D
    //    if (status == Status.dead || characterState == CharacterState.dead)
    //        return;

    //    //This will cancel getting hit if guarding and will play a sound
    //    if (guarding)
    //    {
    //        ///TODO: For Photon Guard Sound
    //        //photonView.RPC("PlaySound", PhotonTargets.All, "Sounds/Breeze/BreezeBlock01");
            
    //        if(sfxManager.block != "")
    //        sfxManager.PlaySound(sfxManager.block);

    //        return;
    //    }

    //    //if (gameObject.tag == "Player")
    //    //{
    //        ///TODO: For Photon hit sound
    //        //photonView.RPC("PlaySound", PhotonTargets.All, "SFX/Hit4");

    //        if (sfxManager.hit1 != "")
    //        {
    //            sfxManager.PlaySound(sfxManager.hit2);
    //        }

    //        StartCoroutine("Rumble");
    //    //}
    //    //This will put this enemy in a getting hit state
    //    characterState = CharacterState.flinching;

    //    //StopCoroutine("FlinchReset");
    //    body.material.SetColor("_Ambient", normalColor);

    //    #region Damage
    //    //Damage Forumla
    //    if (!invincible)
    //    {
    //        var damageDealt = (damage - (defense / 3));

    //        if (damageDealt <= 1)
    //            damageDealt = 1;

    //        health -= damageDealt;

    //    #endregion

    //        ///TODO: For flashing characters
    //        //StartCoroutine("FlashCharacter");

    //        //Flinch the character
    //        if (flinchTolerance <= flinchStr)
    //        {
    //            flinchTime = maxFlinchTime;
    //            status = Status.flinching;

    //            if (tag == "Player")
    //            {
    //                speed = 0;
    //                KnockBack(new Vector3(foe.x * push, 0, foe.z * push));
    //                Upward(lift);

    //                if (lift < 0)
    //                {
    //                    slammed = true;
    //                }
    //            }
    //        }
    //        //The enemies use rigidbodies, so we will add force
    //        if (tag == "Enemy")
    //        {
    //            rigidbody.AddForce((new Vector3(foe.x * push, lift, foe.z * push) * (1000)));
    //            if (lift < 0)
    //            {
    //                slammed = true;
    //            }
    //        }
    //    }
    //    else
    //    {
    //        GameObject newDamage = (GameObject)Instantiate(Resources.Load("Prefabs/DamageText"));
    //        newDamage.GetComponent<DisplayDamage>().target = transform;
    //        newDamage.GetComponent<GUIText>().text = "Invincible";
    //    }

    //    //Death Condition
    //    if (health <= 0)
    //    {
    //        health = 0;
    //        Die();
           
    //        ///TODO: For Photon Dead
    //       //photonView.RPC("Die", PhotonTargets.AllBuffered, null);
    //    }
    //}

    /// <summary>
    /// Flash this character
    /// </summary>
    /// <returns></returns>
    public IEnumerator FlashCharacter()
    {
        body.material.SetColor("_Ambient", Color.white);
        yield return new WaitForSeconds(.1f);
        body.material.SetColor("_Ambient", normalColor);
    }

    /// <summary>
    /// Face the Target
    /// </summary>
    /// <param name="target"></param>
    public void FaceTarget(Vector3 target)
    {
        Vector3 foe = target;

        if (foe != null)
        {
            var tempDir = new Vector3(foe.x, 0, foe.z);

            trans.LookAt(new Vector3(tempDir.x, trans.position.y, tempDir.z));
            BasicPhysics();

            if (controller != null)
                controller.Move(direction * Time.deltaTime);
            else
                rigidbody.velocity = (direction);
        }
    }


    /// <summary>
    /// Kill this instance on the spot :D
    /// </summary>
    public virtual void Die()
    {
        //Stop all movement on this instance
        Stop();

        characterState = CharacterState.dead;
        status = Status.dead;
        activated = false;

        #region Death Feedback
        if (gameObject.tag == "Enemy")
        {
            //target.GetComponent<ExperienceSystem>().GainExperience(currentExp);
            //GetComponent<Enemy>().enabled = false;
            StartCoroutine("DestroyThisInstance", 1);
        }
        else if (gameObject.tag == "Player")
        {
            //Application.LoadLevel(0);
            print("Player died! Double click on this line to add in your own death condition");
        }
        #endregion
    }

    IEnumerator DestroyThisInstance(float t)
    {
        yield return new WaitForSeconds(t);
        if (particleManager.SpawnParticle(particleManager.death) != null)
        {
            GameObject deathParticle = particleManager.SpawnParticle(particleManager.death);
            deathParticle.transform.position = this.trans.position;
        }
        if (sfxManager.death != "")
        {
            sfxManager.PlaySound(sfxManager.death);
        }

        Destroy(this.gameObject);
    }
    #endregion

    #region Infliction
    /// <summary>
    /// Drain stamina over time
    /// </summary>
    /// <returns></returns>
    IEnumerator StaminaDrain()
    {
        yield return new WaitForSeconds(0.5f);
        StopCoroutine("StaminaDrain");

        if (stamina > 0)
            stamina -= 1;
    }
    /// <summary>
    /// Gain stamina over time
    /// </summary>
    /// <returns></returns>
    IEnumerator StaminaGain()
    {
        yield return new WaitForSeconds(2f);
        StopCoroutine("StaminaGain");

        if (stamina < maxStamina)
            stamina += 5;
    }
    /// <summary>
    /// Drain health over time
    /// </summary>
    /// <returns></returns>
    IEnumerator Poisoned()
    {
        yield return new WaitForSeconds(1.5f);
        StopCoroutine("Poisoned");
        //ApplyDamage(maxHealth / 5, trans.position, 0, 0);
        RemoveInfliction(3);
    }
    /// <summary>
    /// Inflict paralysis for a certain amount of time
    /// </summary>
    /// <returns></returns>
    IEnumerator Shocked()
    {
        yield return new WaitForSeconds(1.5f);
        StopCoroutine("Shocked");
        //ApplyDamage(maxHealth / 5);
        StartCoroutine("FlashCharacter");

    }
    /// <summary>
    /// Inflict paralysis and damage over time
    /// </summary>
    /// <returns></returns>
    IEnumerator Frozen()
    {
        yield return new WaitForSeconds(1.5f);
        StopCoroutine("Frozen");
        //ApplyDamage(maxHealth / 5, trans.position, 0, 0);
    }
    /// <summary>
    /// Alter the player's movement for a certain amount of time
    /// </summary>
    /// <returns></returns>
    IEnumerator Dizzy()
    {
        yield return new WaitForSeconds(1.5f);
        StopCoroutine("Dizzy");
        //ApplyDamage(maxHealth / 5, trans.position, 0, 0);
    }
    /// <summary>
    /// Clear the player of
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    IEnumerator RemoveInfliction(float t)
    {
        yield return new WaitForSeconds(t);
        status = Status.none;
    }
    #endregion

    /// <summary>
    /// Vibrate the controller
    /// </summary>
    /// <returns></returns>
    IEnumerator Rumble()
    {
        #if !UNITY_WEBPLAYER
                GamePad.SetVibration(0, 0, 1);
        #endif
                yield return new WaitForSeconds(0.1f);
        #if !UNITY_WEBPLAYER
                GamePad.SetVibration(0, 0, 0);
        #endif
    }

}

