using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour 
{
    /// <summary>
    /// This contains a set of states that can be used for both the enemy and character child classes
    /// </summary>
    public enum Status { none, flinching, poisoned, frozen, shocked, dizzy, staggered, dead }
    public Status status = Status.none;

    public float moveSpeed = 0;
    public Vector3 direction, force;

    public CharacterController controller;
    public Transform trans, target;
    public Animation anim;

    public Transform[] points;


    #region Members [Sliding]
    private float slideLimit = 0;
    private float rayDistance = 0;
    private Vector3 contactPoint = new Vector3();
    private RaycastHit hit;
    public float slideSpeed = 15f;

    // If the player ends up on a slope which is at least the Slope Limit as set on the character controller, then he will slide down
    public bool slideWhenOverSlopeLimit = false;
    public bool sliding = false;
    #endregion

    /// <summary>
    /// Protected members 
    /// </summary>
    public float speed = 5, maxSpeed = 8, jump = 5, targetSpeed, accelerationSpeed = 1, gravity = 20;
    public bool falling = true, jumping = false;


	// Use this for initialization
	void Start () 
    {
        trans = this.transform;
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animation>();
	}

    #region Character Physics
    /// <summary>
    /// Applies speed to the direction.
    /// Applies Gravity.
    /// Applies Animation Framework.
    /// </summary>
    public virtual void BasicPhysics()
    {
        //AnimationFramework();

        //Catch the current vertical movement (for jumping/falling)
        var _holdTheJump = direction.y;

        //Don't want our character pointing up if we jump
        direction.y = 0;

        //We only want direction at this point
        direction.Normalize();


        //This is our "friction"
        speed = Mathf.Lerp(speed, targetSpeed, accelerationSpeed);


        //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
        direction = (direction.magnitude > 1) ? new Vector3(direction.x * speed, _holdTheJump, direction.z * speed)
                                              : new Vector3(trans.forward.x * speed, _holdTheJump, trans.forward.z * speed);

        Gravity(); 

        //Allows for knockback and rush to work
        if (force.magnitude > 1)
        {
            force *= Mathf.Lerp(force.magnitude, 0, .03f) / force.magnitude;
            controller.Move(force * Time.deltaTime);
        }
        else if (force.magnitude > 0)
            force = Vector3.zero;
    }

    /// <summary>
    /// Makes the character slide when on slopes
    /// </summary>
    public void Slide()
    {
        //if (controller.isGrounded)
        //{
        sliding = false;

        // See if surface immediately below should be slid down. We use this normally rather than a ControllerColliderHit point,
        // because that interferes with step climbing amongst other annoyances
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, rayDistance))
        {
            //if (hit.transform.gameObject.layer == 8)
            //    return;

            if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                sliding = true;
        }
        // However, just raycasting straight down from the center can fail when on steep slopes
        // So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
        else
        {
            Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit);

            if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
            {
                if (Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit))
                {
                    //if (hit.transform.gameObject.layer == 8)
                    //    return;

                    sliding = true;
                }
            }
        }

        // If sliding (and it's allowed), or if we're on an object tagged "Slide", get a vector pointing down the slope we're on
        if ((sliding && slideWhenOverSlopeLimit))
        {
            if (hit.transform.gameObject.layer == 8)
                return;

            Vector3 hitNormal = hit.normal;
            Vector3 slideDirection = new Vector3(hitNormal.x, -hit.normal.y, hitNormal.z).normalized;

            Vector3.OrthoNormalize(ref hitNormal, ref direction);
            //direction = new Vector3(slideDirection.x * slideSpeed, direction.y, slideDirection.z * slideSpeed);
            force = slideDirection * slideSpeed;
            direction = slideDirection;
            //if (!anim.IsPlaying("Sliding_Down"))

            if (gameObject.tag == "Player")
                if (anim.Play("Sliding_Down") != true)
                    anim.Play("Sliding_Down");
            //playerControl = false;
        }
        //}
    }

    /// <summary>
    /// This needs to be updated
    /// </summary>
    protected virtual void Gravity()
    {
        var velocity = direction;

        velocity.y = direction.y;
        direction.y = velocity.y;

        direction.y -= (direction.y > -gravity) ? gravity * Time.deltaTime : 0;
    }

    /// <summary>
    /// Default jump which sets direction.y to 10.
    /// </summary>
    public virtual void Upward()
    {
        if (controller.isGrounded)
        {
            direction.y = jump;
            //if (status != Status.flinching)
                //anim.CrossFade(jumpAnim, .01f);
            jumping = true;
        }
    }

    /// <summary>
    /// Overloaded jump that applies and upward force
    /// </summary>
    /// <param name="_force">this constitutes the uppward force</param>
    public virtual void Upward(float _force)
    {
        direction.y = _force;
    }

    /// <summary>
    /// Sends the character in the specified direction
    /// </summary>
    /// <param name="height">Where is the character going? Specify force in the vector</param>
    public virtual void KnockBack(Vector3 dir)
    {
        force = dir;
    }

    /// <summary>
    /// Thrusts the object towards it's target
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="amount"></param>
    public virtual void Rush(Vector3 dir, float amount)
    {
        force = dir * amount;
    }
    #endregion

}
