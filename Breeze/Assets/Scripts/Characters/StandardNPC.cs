using UnityEngine;
using System.Collections;

public class StandardNPC : NPC 
{

    public enum Behavior { none, wander, seek, flee, circle, pursuit, scoping }
    public Behavior behavior;

    public NPCSteeringBehaviors steer;
    public bool cantContinue;

    /// <summary>
    /// Behavior distance check
    /// </summary>
    public float minDistance = 2, safeDistance = 15, attackDistance = 2, castDistance = 5;

    protected virtual void Awake()
    {
        steer = gameObject.AddComponent<NPCSteeringBehaviors>();
        steer.CreateNewWanderPoint();
    }

    public virtual void CharacterMovement()
    {
        BasicPhysics();
        controller.Move(direction * Time.deltaTime);
    }
	
	// Update is called once per frame
	void Update () 
    { 
        CharacterMovement();
        steer.FollowPath();
        //steer.StartCoroutine("ChooseNewWanderPoint");
        //steer.Wander();
	}

    public void Stop()
    {
        targetSpeed = 0f; speed = 0f;
        direction.z = 0; direction.x = 0;
    }
}
