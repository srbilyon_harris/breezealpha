using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EquipmentRenderer : MonoBehaviour 
{
		//Store all the possible components in here
	//public List<GameObject> shirts, gloves, shorts, bag;
	
	//List the currently equipped one here
	public GameObject body, bookBag;
	
	public bool showbag, showShirt, showGloves, showShorts;
	
	public Texture shirt1,shirt2,shirt3;
	

	/// <summary>
	/// Gets the renderer.
	/// </summary>
	/// <returns>
	/// The renderer.
	/// </returns>
	/// <param name='equip'>
	/// Equip.
	/// </param>
	Renderer GetRenderer(GameObject equip)
	{
		Renderer equipRenderer = equip.GetComponent<Renderer>();
		return equipRenderer;
	}
	
	/// <summary>
	/// Gets the texture.
	/// </summary>
	/// <returns>
	/// The texture.
	/// </returns>
	/// <param name='equip'>
	/// Equip.
	/// </param>
	Texture GetTexture (GameObject equip)
	{
		Texture equipTexture = equip.GetComponent<Renderer>().material.mainTexture;
		return equipTexture;
	}
	
	/// <summary>
	/// Sets the texture.
	/// </summary>
	/// <param name='equip'>
	/// Equip.
	/// </param>
	/// <param name='tex'>
	/// Tex.
	/// </param>
	public void SetTexture (GameObject equip, Texture tex, string type)
	{
		if (type == "Shirt")
		{
			equip.GetComponent<Renderer>().materials[0].SetTexture("_Diffuse", tex);
			Debug.Log(equip.GetComponent<Renderer>().materials[0].GetTexture("_Diffuse").name);
		}
	}
	
	/// <summary>
	/// Shows the equip.
	/// </summary>
	/// <param name='equip'>
	/// Equip.
	/// </param>
	void ShowEquip(GameObject equip)
	{
		Renderer equipRenderer = equip.GetComponent<Renderer>();
		equipRenderer.renderer.enabled = true;
	}
	
	/// <summary>
	/// Hides the equip.
	/// </summary>
	/// <param name='equip'>
	/// Equip.
	/// </param>
	void HideEquip(GameObject equip)
	{
		Renderer equipRenderer = equip.GetComponent<Renderer>();
		equipRenderer.renderer.enabled = false;
	}
}


