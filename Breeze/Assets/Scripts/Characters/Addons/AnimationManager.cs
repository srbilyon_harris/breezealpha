using UnityEngine;
using System.Collections;

/// <summary>
/// AnimationManager.cs
/// Allows for define animations on a character based on their current state
/// </summary>
public class AnimationManager : MonoBehaviour
{
    #region Animation Strings
    public string bufferedAnimation;

    public string walkAnim = "Walk";
    public string runAnim = "Run";
    public string sprintAnim = "Sprint";
    public string idleAnim = "Idle";
    public string weakAnim = "Weak";
    public string fallAnim = "Fall";
    public string glideMoveAnim = "Glide";
    public string glideIdleAnim = "GlideIdle";

    public string jumpAnim = "Jump";
    public string landAnim = "Land";

    public string deathAnim = "Death";

    public string sprintGuardAnim = "SpringGuard";
    public string runGuardAnim = "RunGuard";

    public string idleGuardAnim = "Knockback_Medium";
    public string blockHitAnim = "Knockback_Medium";

    public string attackAnimPrefix = "Attack";
    public string swordAttackAnimPrefix = "Sword";
    public string castingAnimPrefix = "Cast";

    public string hitAnimPrefix = "Knockback_Lite";
    public string cryAnimPrefix = "Cry";

    public string crouchAnim = "Crouch";
    public string crawlAnim = "Crawl";

    public string swimMove = "SwimMove";
    public string swimIdle = "SwimIdle";

    public string pushMove = "PushMove";
    public string pushIdle = "PushIdle";

    public string throwAnim = "Throw";
    public string rollAnim = "Roll";
    
    public string ledgeHangIdle = "ClimbIdle";
    public string ledgeMoveRight = "LedgeMoveRight";
    public string ledgeMoveLeft = "LedgeMoveLeft";

    public string wallIdle = "WallIdle";
    public string wallMoveRight = "WallMoveRight";
    public string wallMoveLeft = "WallMoveLeft";
    public string wallMoveUp = "WallMoveUp";
    public string wallMoveDown = "WallMoveDown";

    public string slideAnim = "Sliding_Down";
    #endregion

    public Character character;
    public Animation anim;

    // Use this for initialization
    void Start()
    {
        character = GetComponent<Character>();
        anim = GetComponent<Animation>();

        #region Clamped Animations
        if (fallAnim != "")
        anim[fallAnim].wrapMode = WrapMode.ClampForever;

        if (deathAnim != "")
        anim[deathAnim].wrapMode = WrapMode.ClampForever;

        if (jumpAnim != "")
        anim[jumpAnim].wrapMode = WrapMode.ClampForever;
        #endregion
    }

    /// <summary>
    /// Basic Movement Animation Framework
    /// </summary>
    public void AnimationFramework()
    {
        switch (character.characterState)
        {
            #region Landing
            case Character.CharacterState.landing:
                if (character.attacking) 
                    return;

                if (landAnim != "")
                {
                    anim[landAnim].speed = 2f;
                    anim.CrossFade(landAnim);
                }
                break;
            #endregion

            #region Falling
            case Character.CharacterState.falling:
                if (fallAnim != "")
                {
                    if (character.status != Character.Status.staggered)
                        anim.CrossFade(fallAnim);
                }
                break;
            #endregion

            #region Grounded
            case Character.CharacterState.grounded:
                if (character.status == Character.Status.dead)
                    return;

                    if (character.attacking)
                        return;

                if (character.status == Character.Status.flinching)
                {
                    if (hitAnimPrefix != "")
                    {
                        anim[hitAnimPrefix].wrapMode = WrapMode.ClampForever;
                        anim.Play(hitAnimPrefix);
                    }
                }
                else
                {
                    if (character.moveState == Character.MoveState.Moving)
                    {
                        //GUARDING
                        if (character.guarding)
                        {
                            if (character.sprinting)
                            {
                                if (sprintAnim != "")
                                    anim.Play(sprintGuardAnim);
                            }
                            else
                            {
                                if (runGuardAnim != "")
                                anim.Play(runGuardAnim);
                            }
                        }
                        else
                        {
                            if (character.sprinting)
                            {
                                if (sprintAnim != "")
                                anim[sprintAnim].speed = 2;

                                if (character.targetSpeed != 0)
                                {
                                    if (sprintAnim != "")
                                        anim.CrossFade(sprintAnim);
                                }
                                else
                                    if (idleAnim != "")
                                    {
                                        anim.Play(idleAnim);
                                    }
                            }
                            else
                            {
                                if (runAnim != "")
                                {
                                    anim[runAnim].speed = 1;
                                    anim.Play(runAnim);
                                }
                            }
                        }
                    }
                    else if (character.moveState == Character.MoveState.Stationary && character.status != Character.Status.staggered)
                    {
                        //GUARDING
                        if (character.guarding)
                        {
                            if (idleGuardAnim != "")
                            anim.Play(idleGuardAnim);
                        }
                        else
                        {
                            if (!character.weak && !character.tired)
                            {
                                if (idleAnim != "")
                                    anim.Play(idleAnim);
                            }
                            else
                            {
                                if (weakAnim != "")
                                anim.Play(weakAnim);
                            }
                        }
                    }
                }
                break;
            #endregion

            #region Dead
            case Character.CharacterState.dead:
                if (deathAnim != "" && !anim.IsPlaying(deathAnim))
                {
                    anim.Stop();
                    anim.Play(deathAnim);
                }
                break;
            #endregion

            #region Gliding
            case Character.CharacterState.gliding:
                switch (character.moveState)
                {
                    case Character.MoveState.Moving:
                        if (glideMoveAnim != "") anim.Play(glideMoveAnim); 
                        break;
                    case Character.MoveState.Stationary:
                        if (glideIdleAnim != "") anim.Play(glideIdleAnim); 
                        break;
                }
                break;
            #endregion

            #region Jumping
            case Character.CharacterState.jumping:

                if (character.attacking)
                    return;

                if (character.status != Character.Status.staggered)
                {
                    if (jumpAnim != "")
                    {
                        anim[jumpAnim].wrapMode = WrapMode.ClampForever;
                        anim.Play(jumpAnim);
                    }
                }
                break;
            #endregion

            #region Attacking
            case Character.CharacterState.attacking:
                //if (!anim.IsPlaying(bufferedAnimation))
                //{
                    
                    //if (bufferedAnimation != "")
                    //{
                        //anim[bufferedAnimation].wrapMode = WrapMode.ClampForever;
                        
                    //}
                //}
                break;
            #endregion

            #region Crouching
            case Character.CharacterState.crouching:
                if (crouchAnim != "")
                anim.Play(crouchAnim);
                break;
            #endregion

            #region Crawling
            case Character.CharacterState.crawling:
                if (crawlAnim != "")
                anim.Play(crawlAnim);
                break;
            #endregion

            #region Swimming
            case Character.CharacterState.swimming:
                switch (character.moveState)
                {
                    case Character.MoveState.Moving:
                        if (swimMove != "") anim.Play(swimMove); break;
                    case Character.MoveState.Stationary:
                        if (swimIdle != "") anim.Play(swimIdle); break;
                }
                break;
            #endregion

            #region Pushing
            case Character.CharacterState.pushing:
                switch (character.moveState)
                {
                    case Character.MoveState.Moving:
                        if (pushMove != "") anim.Play(pushMove); break;
                    case Character.MoveState.Stationary:
                        if (pushIdle != "") anim.Play(pushIdle); break;
                }
                break;
            #endregion

            #region Ledgehending
            case Character.CharacterState.ledgehanging:
                switch (character.moveState)
                {
                    case Character.MoveState.Moving:
                        switch (character.mDirection)
                        {
                            case Character.MoveDirection.Left:
                                if (ledgeMoveLeft != "")
                                {
                                    anim[ledgeMoveLeft].speed = 2;
                                    anim.Play(ledgeMoveLeft);
                                }
                                break;
                            case Character.MoveDirection.Right:
                                if (ledgeMoveRight != "")
                                {
                                    anim[ledgeMoveRight].speed = 2;
                                    anim.Play(ledgeMoveRight);
                                }
                                break;
                        }
                        break;

                    case Character.MoveState.Stationary:
                        if (ledgeHangIdle != "")
                        anim.Play(ledgeHangIdle); break;
                }
                break;
            #endregion

            #region Flinching
            case Character.CharacterState.flinching:
                ///TODO: Flinch animation based on flinchlevel
                //anim.Play(hitAnimPrefix + character.flinchLevel);
                anim[hitAnimPrefix].wrapMode = WrapMode.ClampForever;
                anim.Play(hitAnimPrefix);
                break;
            #endregion

            #region Casting
            case Character.CharacterState.casting:
                if (bufferedAnimation != "")
                {
                    anim[bufferedAnimation].wrapMode = WrapMode.ClampForever;
                    anim.Play(bufferedAnimation + character.castLevel);
                }
                break;
            #endregion

            #region Animating
            case Character.CharacterState.animating:
                if (bufferedAnimation != "")
                {
                    anim[bufferedAnimation].wrapMode = WrapMode.ClampForever;
                    anim.Play(bufferedAnimation);
                }
                break;
            #endregion

            #region Climbing
            case Character.CharacterState.climbing:
                if (character.direction.x == 0 && character.direction.y == 0)
                {
                    if (wallIdle != "")
                        anim.Play(wallIdle);
                }
                else
                {
                    if (character.direction.x < 0f && character.direction.x != 0)
                    {
                        if (wallMoveLeft != "")
                        {
                            anim[wallMoveLeft].speed = 2;
                            anim.Play(wallMoveLeft);
                        }
                        character.direction.y = 0;
                    }
                    else

                        if (character.direction.x > 0f && character.direction.x != 0)
                        {
                            if (wallMoveRight != "")
                            {
                                anim[wallMoveRight].speed = 2;
                                anim.Play(wallMoveRight);
                            }

                            character.direction.y = 0;

                        }
                        else
                            if (character.direction.y < 0f && character.direction.y != 0)
                            {
                                if (wallMoveDown != "")
                                {
                                    anim[wallMoveDown].speed = 2;
                                    anim.Play(wallMoveDown);
                                }

                                character.direction.x = 0;
                                character.direction.z = 0;
                            }
                            else
                                if (character.direction.y > 0f && character.direction.y != 0)
                                {
                                    if (wallMoveUp != "")
                                    {
                                        anim[wallMoveUp].speed = 2;
                                        anim.Play(wallMoveUp);
                                    }

                                    character.direction.x = 0;
                                    character.direction.z = 0;

                                }
                }
                break;
            #endregion

        }
    }
}
