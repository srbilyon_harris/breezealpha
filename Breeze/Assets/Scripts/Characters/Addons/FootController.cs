 /*
 * Foot Controller
 * These can be attached to the feet of character so that 
 * the "feet" can detect the surfaces they land on and can: 
 * 
 * - Adjust the friction the player navigates on
 * - Play a sound when hitting certain surface
 * - Emit particles when landing on certain surfaces
 * 
 * Make sure that this attached to a trigger
 */
using UnityEngine;
using System.Collections;

/// <summary>
///  * These can be attached to the feet of character so that 
///  the "feet" can detect the surfaces they land on and can: 
/// 
/// - Adjust the friction the player navigates on
/// - Play a sound when hitting certain surface
/// - Emit particles when landing on certain surfaces
/// </summary>
/// 

[RequireComponent(typeof(Rigidbody))]
public class FootController : MonoBehaviour
{
    /// <summary>
    /// Makes the sound effect not seem so repetitive
    /// </summary>
    float pitchVarience = 0.1f;
    /// <summary>
    /// These variables modify the friction the player walks on
    /// </summary>
    float defaultAccel, iceAccel = 0.3f, thickAccel = 2f;

    /// <summary>
    /// This modifies the max speed the player can go when walking on different surfaces 
    /// </summary>
    public float defaultMaxSpeed, iceMaxSpeed=16, thickMaxSpeed=4;

    public string defaultStep, iceStep, thickStep, defaultSound, iceSound, thickSound;

    /// <summary>
    /// Reference myself
    /// </summary>
    public Character self;

    /// <summary>
    /// Sets all the references
    /// </summary>
    void Start()
    {
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;

        self = gameObject.transform.root.GetComponent<Character>();
        defaultAccel = self.accelerationSpeed;
        defaultMaxSpeed = self.runSpeed;
    }

    /// <summary>
    /// When the foot triggers hit the ground, evaluate it
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other) 
	{
        //Ice Detected
        if (other.collider.tag == "Ice")
        {
            self.accelerationSpeed = iceAccel;
            self.runSpeed = iceMaxSpeed;
            Debug.Log("Ice");
            PlayFootStep("Ice Steps/BreezeFootstepIce01");
        }
        //Thick Surface detected (like mud)
        else if (other.collider.tag == "Thick")
        {
            self.accelerationSpeed = thickAccel;
            self.targetSpeed = 0;
            self.runSpeed = thickMaxSpeed;
            PlayFootStep("Breeze/Ice");

            Debug.Log("Thick");
        }
        //Standard Surface (resets to default friciton)
        else if (other.collider.tag == "Default")
        {
            self.accelerationSpeed = defaultAccel;
            self.runSpeed = defaultMaxSpeed;
            //print(other.name);
            PlayFootStep(defaultSound);

            if (Resources.Load("Particles/" + defaultStep) != null)
            {                
                var ripple = (GameObject)Instantiate(Resources.Load("Particles/"+defaultStep));
                ripple.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            }
        } 
	}

    /// <summary>
    /// Plays a footstep sound
    /// </summary>
    /// <param name="sound"></param>
    void PlayFootStep(string sound)
    {
        if (audio != null)
        {
            audio.clip = (AudioClip)Resources.Load("Sounds/" + sound);
            audio.pitch = Random.Range(1.0f - pitchVarience, 1.0f + pitchVarience);
            audio.Play();
        }
    }

    /// <summary>
    /// If grounded, just make sure that there is an adjustment in friction made
    /// </summary>
    void Update()
    {
        ///TODO: Raycast here
        //if (!self.controller.isGrounded)
        //{
        //    self.accelerationSpeed = defaultAccel;
        //}
    }
}




