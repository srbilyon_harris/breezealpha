using UnityEngine;
using System.Collections;

public class CollisionSoundEffect : MonoBehaviour {
	public AudioClip audioClip;
	public float volumeModifier = 1.0f;
}
