using UnityEngine;
using System.Collections;
using XInputDotNetPure; // Required in C#


public class HeadBop : GenericTrigger
{

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.collider.transform.tag == "Player")
        {
            if (player.transform.position.y > transform.position.y-1)
            {
                //transform.root.GetComponent<Character>().ApplyDamage(2, Vector3.zero, 0, 0, 3);
                player.GetComponent<Character>().Upward(10);
                StartCoroutine("Rumble");
            }
        }
    }

    /// <summary>
    /// Vibrate the controller
    /// </summary>
    /// <returns></returns>
    IEnumerator Rumble()
    {
#if !UNITY_WEBPLAYER
        GamePad.SetVibration(0, 0, 1);
#endif
        yield return new WaitForSeconds(0.1f);
#if !UNITY_WEBPLAYER
        GamePad.SetVibration(0, 0, 0);
#endif
    }
}
