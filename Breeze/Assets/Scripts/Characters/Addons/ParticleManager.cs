/*
 * Particle Manager
 * 
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// ParticleManager.cs
/// This manager handles the caching and instantiation of particle effects
/// to be used by a character.
/// </summary>
public class ParticleManager : MonoBehaviour
{

    #region Particle Strings
    public string block;
    public string hit1, hit2, hit3;
    public string land, slam;
    public string footStep, death = "Death";
   

    public string attack1, attack2, attack3, attack4, attack5, attack6;
    public string special1, special2, special3, special4, special5, special6;    
    #endregion

    /// <summary>
    /// Spawn a particle
    /// </summary>
    /// <param name="location"></param>
    /// <param name="particle"></param>
    public GameObject SpawnParticle(string particle)
    {
        //Particle Load
        if (Resources.Load("Particles/" + particle) != null)
        {
            return (GameObject)Instantiate(Resources.Load("Particles/" + particle));
        }
        else return null;
    }

    /// <summary>
    /// Spawn a hit particle at x location
    /// </summary>
    /// <param name="location"></param>
    /// <param name="particle"></param>
    public void SpawnParticle(Vector3 location, string particle)
    {
        //Particle Load
        if (Resources.Load("Particles/" + particle) != null)
        {
            GameObject newParticle = (GameObject)Instantiate(Resources.Load("Particles/" + particle));
            newParticle.transform.position = location;//new Vector3(location.x, location.y + 1, transform.position.z + 3 + transform.forward.z);
            newParticle.transform.rotation = transform.rotation;
        }
    }
}
