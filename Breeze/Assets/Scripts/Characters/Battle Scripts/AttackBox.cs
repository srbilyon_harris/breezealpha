using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

public enum AttackType { SingleHit, Multihit }
public enum AttackDuration { Single, Multihit }

public class AttackBox : MonoBehaviour
{
    #region Members
    public bool isActive;
    public string attackToAssign = "Jab";
    int damage, hitCount;
    float push, lift;

    List<Vector2> hitFrames;
    GameObject particleEmitter, soundEmitter;
    string animation;
    public Attacks attack;

    public Collider attackbox;
    public Character owner;
    List<CombatHandler> possibleTargets;
    #endregion

    AttackType attackType;
    AttackDuration attackDuration;

    void Start()
    {
        GetAttack(attackToAssign);
        attackbox = this.collider;
        owner = transform.parent.GetComponent<Character>();

        //if (attackbox = transform.Find("AttackBox").collider)
        //    attackbox = transform.Find("AttackBox").collider;
        //else
        //    Debug.LogError("There isn't an attack box attached to this object. Make sure it's a child object and properly spelled 'AttackBox' ");
    }

    /// <summary>
    /// Find an attack from the Attacks Library using a string
    /// </summary>
    /// <param name="attackName"></param>
    void GetAttack(string attackName)
    {
        var p = System.Activator.CreateInstance(Type.GetType(attackName));
        attack = (Attacks)p;
    }

    void Update()
    {
        if (isActive)
        { RunCombat(); }
    }

    /// <summary>
    /// Turn this hitbox on or off
    /// </summary>
    /// <param name="b"></param>
    public void Activate(bool b)
    {
        isActive = b;
    }

    /// <summary>
    /// Have this attackbox check for collisions with a hitbox
    /// </summary>
    bool CheckForCollision(CombatHandler pTarget)
    {
        if (AttackBoxCheck(pTarget.hitbox.collider))
        {
            return true;
        }
        return false;
    }

    void RunCombat()
    {
        switch (attackDuration)
        {
            case AttackDuration.Single:
                switch (attackType)
                {
                    case AttackType.SingleHit:
                        StartCoroutine(PerformAttack());
                        break;
                    case AttackType.Multihit:
                        break;
                }
                break;

            case AttackDuration.Multihit:
                switch (attackType)
                {
                    case AttackType.SingleHit:
                        StartCoroutine(PerformAttack());
                        break;
                    case AttackType.Multihit:
                        break;
                }
                break;
        }
    }

    IEnumerator PerformAttack()
    {
        StopCoroutine("PerformAttack");

        owner.animManager.bufferedAnimation = attack.animName;
        owner.animManager.anim[owner.animManager.bufferedAnimation].speed = owner.GetComponent<CombatHandler>().attackSpeedScale;


        owner.anim.Play(owner.animManager.bufferedAnimation);

        owner.Rush(transform.forward, attack.strikeRush);

        yield return new WaitForSeconds(FrameToTime(attack.hitFrame));
        
        if (attack.lift != 0)
        owner.Upward(attack.lift);

        possibleTargets = GetTargetCombatHandlers();

        if (!owner.GetComponent<CombatHandler>().attackFinished)
        {
            foreach (CombatHandler pTarget in possibleTargets)
            {
                if (CheckForCollision(pTarget))
                {
                    //if (!IsFoeAttacking(pTarget) && !IsFoeGuarding(pTarget))
                    pTarget.ApplyDamage(attack.damage, attack.push, attack.lift, transform.forward);
                }
            }
        }
        yield return new WaitForSeconds(FrameToTime(attack.endFrame) - FrameToTime(attack.hitFrame));
        
        Activate(false);
        StopCoroutine("PerformAttack");

        //yield return new WaitForSeconds(attack.coolDownFrame);
        //print(FrameToTime(attack.endFrame));

        owner.GetComponent<CombatHandler>().StartAttackCoolDown(attack.coolDownFrame / owner.GetComponent<CombatHandler>().attackSpeedScale);
        //if (owner.attacking == false)
        //{
        //owner.characterState = Character.CharacterState.none;
        //owner.status = Character.Status.none;
        //owner.attacking = false;
        //}
    }

    void MultiHitCalculation()
    { }

    bool IsFoeGuarding(CombatHandler pTarget)
    {
        if (pTarget.guarding)
            return true;
        else
            return false;
    }

    bool CanFoeTakeHit()
    {
        return true;
    }
    
    /// <summary>
    /// Create the text that will display the amount of damage dealt
    /// </summary>
    public void SpawnDamageText()
    {
        if (Resources.Load("Prefabs/DamageText") != null)
        {
            GameObject newDamage = (GameObject)Instantiate(Resources.Load("Prefabs/DamageText"));
            newDamage.GetComponent<DisplayDamage>().target = transform;
            newDamage.GetComponent<GUIText>().text = "Invincible";
        }
    }
    
    float FrameToTime(float frame)
    {
        //print((attack.hitFrame / 60) / owner.GetComponent<CombatHandler>().attackSpeedScale);
        return (attack.hitFrame / 60) / owner.GetComponent<CombatHandler>().attackSpeedScale;
    }

    /// <summary>
    /// Check if target is inside this character's attackbox
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public bool AttackBoxCheck(Collider t)
    {
        if (attackbox.collider.bounds.Intersects(t.bounds))
        {
            return true;
        }
        return false;
    }


    void EmitEffects()
    { }

    List<CombatHandler> GetTargetCombatHandlers()
    {
        List<CombatHandler> pTargets = new List<CombatHandler>();
        var t = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject g in t)
        {
            pTargets.Add(g.GetComponent<CombatHandler>());
        }
        return pTargets;
    }

}
