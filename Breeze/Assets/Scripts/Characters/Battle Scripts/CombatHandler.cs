using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure; // Required in C#


public class CombatHandler : MonoBehaviour
{

    #region Members
    public List<AttackBox> attackBoxes;
    public Animation anim;

    public float attackSpeedScale = 2;

    //ParticleManager
    //SFXManager

    public int currentComboSize, comboLimit;
    float comboResetCooldown, chargeAttackCooldown;
    float chargeUpTime, coolDownTime, attackCoolDownTime;

    public float comboTime = 2f, chargeUpTimer = 2f;
    public float flinchTime = 2f, flinchTimer = 2f;

    float maxComboTime = 0.75f, maxChargeUpTime = 3f;


    public Attacks atk;
    public Collider hitbox;
    public Character character;

    public GameObject lockedTarget;

    public bool attackFinished = false, guarding = false, chargedAttack, attackCompleted;
    #endregion

    public void Start()
    {
        if (hitbox = transform.Find("HitBox").collider)
            //Find the character's hit box
            hitbox = transform.Find("HitBox").collider;
        else
            Debug.LogError("There isn't a hit box attached to this object. Make sure it's a child object and properly spelled 'HitBox' ");

        //character = GetComponent<Character>();
    }

    void Update()
    {
        if (!character.attacking)
            GetInput();

        if (currentComboSize > 0)
            ComboTimer();

        if (flinchTime > 0)
        {
            character.status = Character.Status.flinching;
            FlinchTimer();
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button5))
        {
            if (tag == "Player")
            {
                lockedTarget = FindClosestTarget().gameObject;
                Camera.mainCamera.GetComponent<Orbit>().target = lockedTarget.transform;
            }
        }

        if (attackFinished)
            AttackCoolDown();

        ///TODO: Move this to a more appropriate section, or make a seperate function
        #region Guarding
        if (InputHandler.bR2Held || Input.GetKey(KeyCode.JoystickButton4))
        {
            character.guarding = true;
        }
        else if (!InputHandler.bR2Held || Input.GetKeyUp(KeyCode.JoystickButton4))
        {
            character.guarding = false;
        }
        #endregion

        #region Keeps the player from rushing off if he attacks while on a ledge
        RaycastHit hit2;
        if (!Physics.Raycast(transform.position + transform.forward * 1.5f, Vector3.down, out hit2, 10f))
        {
            character.force = Vector3.zero; //Stop();
        }
        #endregion

    }

    void GetInput()
    {
        if (Input.GetMouseButtonDown(0) || InputHandler.b2 || InputHandler.jb2)
        {
            if (currentComboSize == 0)
            {
                GetAttack("Jab");
                foreach (AttackBox ab in attackBoxes)
                {
                    if (ab.attack.name == "Jab")
                    {
                        HandleAttack(ab);
                    }
                }
            }
            else
                if (currentComboSize == 1)
                {
                    GetAttack("JabLeft");
                    foreach (AttackBox ab in attackBoxes)
                    {
                        if (ab.attack.name == "JabLeft")
                        {
                            HandleAttack(ab);
                        }
                    }
                }
                else
                    if (currentComboSize == 2)
                    {
                        GetAttack("JabLeft");
                        foreach (AttackBox ab in attackBoxes)
                        {
                            if (ab.attack.name == "JabLeft")
                            {
                                HandleAttack(ab);
                            }
                        }
                    }
                    else
                        if (currentComboSize == 3)
                        {
                            GetAttack("Jab");
                            foreach (AttackBox ab in attackBoxes)
                            {
                                if (ab.attack.name == "Jab")
                                {
                                    HandleAttack(ab);
                                }
                            }
                        }
                        else
                            if (currentComboSize == 4)
                            {
                                GetAttack("Uppercut");
                                foreach (AttackBox ab in attackBoxes)
                                {
                                    if (ab.attack.name == "Uppercut")
                                    {
                                        HandleAttack(ab);
                                    }
                                }
                            }
                            else
                        if (currentComboSize == comboLimit)
                        {
                            ComboReset();
                        }
        }
    }

    void GetAttackInfo()
    { }

    /// <summary>
    /// Find an attack from the Attacks Library using a string
    /// </summary>
    /// <param name="attackName"></param>
    void GetAttack(string attackName)
    {
        var p = System.Activator.CreateInstance(Type.GetType(attackName));
        atk = (Attacks)p;
        //print(p.GetType());
    }

    /// <summary>
    /// The actual attack begins here. This bumps up the current combo, changes the character's status to attacking,
    /// activates the attack box, and faces the target
    /// </summary>
    /// <param name="ab"></param>
    void HandleAttack(AttackBox ab)
    {
        comboTime = maxComboTime;

        if (!lockedTarget)
            FaceTarget();
        else
            FaceTarget(lockedTarget.transform.position);
        
        ab.owner = GetComponent<Character>();
        ab.Activate(true);
        ab.owner.characterState = Character.CharacterState.attacking;
        ab.owner.status = Character.Status.attacking;
        ab.owner.attacking = true;
        currentComboSize++;
    }

    /// <summary>
    /// A timer that allows the character to exit his flinching state
    /// </summary>
    void FlinchTimer()
    {
        flinchTime -= Time.deltaTime;

        if (flinchTime < 0)
        {
            ComboReset();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void FlinchReset()
    {
        //    if (character.isGrounded)
        //        characterState = CharacterState.grounded;
        //    else
        //        characterState = CharacterState.falling;

        if (character.status != Character.Status.dead)
            character.status = Character.Status.none;
    }

    void FaceTarget()
    {
        if (FindClosestTarget() != null)
        character.FaceTarget(FindClosestTarget().transform.position);
    }

    void FaceTarget(Vector3 target)
    {
        character.FaceTarget(target);
    }

    /// <summary>
    /// Finds the closest enemy and sets him as the target
    /// </summary>
    /// <returns></returns>
    Character FindClosestTarget()
    {
        // Find all game objects with certain tags
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        //Container for the new target
        Character newTarget = null;

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        if (character.charType == Character.CharacterType.ally ||
            character.charType == Character.CharacterType.player)
        {
            // Iterate through them and find the closest one
            foreach (GameObject go in enemies)
            {
                var diff = (go.transform.position - position);
                var curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    newTarget = go.GetComponent<Character>();
                    distance = curDistance;
                }
            }
            return newTarget;
        }

        return null;
        //else
        //    if (character.charType == Character.CharacterType.enemy)
        //        // Iterate through them and find the closest one
        //        foreach (GameObject go in players)
        //        {
        //            var diff = (go.transform.position - position);
        //            var curDistance = diff.sqrMagnitude;

        //            if (curDistance < distance)
        //            {
        //                newTarget = go.GetComponent<Character>();
        //                distance = curDistance;
        //            }
        //        }
        //return newTarget;
    }

    /// <summary>
    /// Begins the delay until the character can attack again
    /// </summary>
    /// <param name="length"></param>
    public void StartAttackCoolDown(float length)
    {
        attackCoolDownTime = length;
        attackFinished = true;
    }

    /// <summary>
    /// Establishes a delay between attacks
    /// </summary>
    void AttackCoolDown()
    {
        attackCoolDownTime -= Time.deltaTime;
        if (attackCoolDownTime < 0)
        {
            AttackReset();
            attackFinished = false;
        }
    }

    /// <summary>
    /// Keeps track of consecutive attacks and resets the combo string if a certain time 
    /// passes between attacks
    /// </summary>
    void ComboTimer()
    {
        comboTime -= Time.deltaTime;

        if (comboTime < 0)
        {
            ComboReset();
        }
    }

    /// <summary>
    /// A timer that counts down to reset the camera
    /// </summary>
    void CooldownTimer()
    {
        coolDownTime -= Time.deltaTime;

        if (coolDownTime < 0)
        {
            AttackReset();
        }
    }

    /// <summary>
    /// Keeps track of how long an attack is held
    /// </summary>
    void ChargeAttackCooldown()
    {
        chargeUpTime += Time.deltaTime;

        if (chargeUpTimer >= maxChargeUpTime)
        {
            chargedAttack = false;
        }
    }

    /// <summary>
    /// Reset the combo string
    /// </summary>
    /// <returns></returns>
    void ComboReset()
    {
        InputHandler.button3Timer = 0; InputHandler.b3Held = false; InputHandler.jb3Held = false;
        InputHandler.button2Timer = 0; InputHandler.b2Held = false; InputHandler.jb2Held = false;
        currentComboSize = 0;
        comboTime = maxComboTime;
    }

    /// <summary>
    /// Resets the attack state
    /// </summary>
    void AttackReset()
    {
        attackCompleted = false;
        character.attacking = false;
        character.status = Character.Status.none;
        
        #if !UNITY_WEBPLAYER && !UNITY_ANDROID
        GamePad.SetVibration(0, 0, 0);
        #endif
    }
    /// <summary>
    /// Apply damage to the character. This is used for non-combative attacks
    /// such as damage triggers
    /// </summary>
    /// <param name="damage">amount of damage</param>
    /// 
    public void ApplyDamage(int damage, float knockBack, float launch, Vector3 knockBackDirection)
    {
        // Check if invincible
        // Check if critical
        // Check if dead
        // Check if flinching

        //Prevents multiple killings :D
        if (character.status == Character.Status.dead || character.characterState == Character.CharacterState.dead)
            return;

        if (tag == "Enemy")
        {
            #if !UNITY_WEBPLAYER && !UNITY_ANDROID
            GamePad.SetVibration(0, 1, 0);
            #endif          
   
            character.sfxManager.PlaySound(character.sfxManager.hit1);
            character.particleManager.SpawnParticle(transform.position,character.particleManager.hit1);

            var em = GetComponent<EnemyMovement>();

            //Make the instance stagger
            character.status = Character.Status.flinching;
            character.characterState = Character.CharacterState.flinching;

            

            //Apply knockback force to this instance
            em.force = knockBackDirection * knockBack * 5;

            em.character.Upward(launch);

            if (CriticalHit(16))
            {
                //Add in critical damage
                character.health -= (damage * 3);
            }
            else
            { character.health -= (damage); }
        }

        print("Ouch! " + damage + " damage dealt");
    }

    /// <summary>
    /// Multiply the amount of damage dealt
    /// </summary>
    /// <param name="ratio"></param>
    /// <returns></returns>
    public bool CriticalHit(int ratio)
    {
        var critChance = UnityEngine.Random.Range(0, ratio);

        if (critChance == 0)
        {
            return true;
        }
        return false;
    }


    #region Hit and Attack box checking

    /// <summary>
    /// Check if target is inside this character's hitbox
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public bool HitBoxCheck(Collider t)
    {
        if (hitbox.collider.bounds.Intersects(t.bounds))
        {
            return true;
        }
        return false;
    }

    #endregion




}
