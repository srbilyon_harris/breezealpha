using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GUIText))]
public class DisplayStatus : MonoBehaviour
{
    public enum DisplayType { Critical, Poison, Shocked, Frozen, LevelUp }
    public DisplayType displayType;

    public Transform target;  // Object that this label should follow
    public Vector3 offset = Vector3.up;    // Units in world space to offset; 1 unit above object by default

    public bool clampToScreen = false;  // If true, label will be visible even if object is off screen
    public float clampBorderSize = 0.05f;  // How much viewport space to leave at the borders when a label is being clamped

    public bool useMainCamera = true;   // Use the camera tagged MainCamera
    public Camera cameraToUse;   // Only use this if useMainCamera is false

    Camera cam;
    Transform thisTransform;
    Transform camTransform;

    void Start()
    {
        thisTransform = transform;
        if (useMainCamera)
            cam = Camera.main;
        else
            cam = cameraToUse;

        switch (displayType)
        {
            case DisplayType.Critical:
                this.gameObject.GetComponent<GUIText>().text = "Critical!";

                //Play Sound
                AudioClip critSound = (AudioClip)Resources.Load("Sounds/Breeze/BreezeCriticalHit01");
                var critAudio = gameObject.AddComponent<AudioSource>();
                critAudio.clip = critSound;
                critAudio.Play();
                //Debug.Log("Play");
                break;
            case DisplayType.Frozen:
                this.gameObject.GetComponent<GUIText>().text = "Frozen";
                break;
            case DisplayType.LevelUp:
                this.gameObject.GetComponent<GUIText>().text = "Level Up!";
                //Play Sound

                AudioClip audioClip = (AudioClip)Resources.Load("Sounds/Breeze/BreezeLevelUp01");
                var lvlUpAudio = gameObject.AddComponent<AudioSource>();
                lvlUpAudio.clip = audioClip;
                lvlUpAudio.audio.Play();
                //Debug.Log("Play");
                break;
            case DisplayType.Poison:
                this.gameObject.GetComponent<GUIText>().text = "Poisoned";
                break;
            case DisplayType.Shocked:
                this.gameObject.GetComponent<GUIText>().text = "Shocked";
                break;
        }
        camTransform = cam.transform;
    }

    void Update()
    {

        if (clampToScreen)
        {
            Vector3 relativePosition = camTransform.InverseTransformPoint(target.position);
            relativePosition.z = Mathf.Max(relativePosition.z, 1.0f);
            thisTransform.position = cam.WorldToViewportPoint(camTransform.TransformPoint(relativePosition + offset));
            thisTransform.position = new Vector3(Mathf.Clamp(thisTransform.position.x, clampBorderSize, 1.0f - clampBorderSize),
                                             Mathf.Clamp(thisTransform.position.y, clampBorderSize, 1.0f - clampBorderSize),
                                             thisTransform.position.z);

        }
        else
        {
            thisTransform.position = cam.WorldToViewportPoint(target.position + offset);
        }

        offset.y += 0.05f;

        this.gameObject.GetComponent<GUIText>().fontSize += 1;

        if (this.gameObject.GetComponent<GUIText>().fontSize == 50)
            Destroy(this.gameObject);
    }

}