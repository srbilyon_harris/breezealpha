using UnityEngine;
using System.Collections;

[System.Serializable]
public class Attacks 
{
    public AttackType attackType = AttackType.SingleHit;
    public AttackDuration attackDuration = AttackDuration.Single;
    public string name;
    public int damage = 1;
    public int priority = 1;
    public float hitFrame = 1, endFrame = 1, coolDownFrame = 0.15f;
    public int hitCount = 1;


    public string animName;

    public float push;
    public float lift;
    public float rise;
    public float radius;

    public float initialRush;
    public float strikeRush;
    public float rushTime;

    public float castDistance;

    public string particle = "PunchRed";
    public int staminaUse = 1;
    public int priorityLevel = 1;
}

/// <summary>
/// A normal punch that send the foe backwards
/// </summary>
public class Jab : Attacks
{
    public Jab()
    {
        name = "Jab";
        damage = 3;
        priority = 3;

        hitFrame = 20;
        endFrame = 22;
        hitCount = 1;

        push = 1;
        lift = 0;
        rise = 0;
        radius = 1;

        initialRush = 0;
        strikeRush = 8;
        rushTime = 0.3f;

        animName = "Heavy_Punch_Right";
        staminaUse = 0;
        //Debug.Log("Jab");
        particle = "PunchBlue";
        priorityLevel = 1;
    }
}

/// <summary>
/// A normal punch that send the foe backwards
/// </summary>
public class JabLeft : Attacks
{
    public JabLeft()
    {
        name = "JabLeft";
        damage = 3;
        priority = 3;

        hitFrame = 20;
        endFrame = 22;

        hitCount = 1;

        push = 1;
        lift = 0;
        rise = 0;

        initialRush = 0;
        strikeRush = 12;
        rushTime = 0.3f;

        radius = 1;
        staminaUse = 0;
        animName = "Heavy_Punch_Left";
        particle = "PunchRed";
        //Debug.Log("Jab");
        priorityLevel = 1;
    }
}
/// <summary>
/// Cross.
/// </summary>
public class Cross : Attacks
{
    public Cross()
    {
        name = "Cross";
        damage = 5;
        priority = 5;
        hitFrame = 30;
        endFrame = 35;

        hitCount = 1;

        push = 3;
        lift = 0;
        rise = 0;

        initialRush = 0;
        strikeRush = 20;
        rushTime = 0.3f;

        radius = 1;
        staminaUse = 1;
        animName = "Heavy_Punch_Left";
        particle = "PunchRed";
        //Debug.Log("Cross");
        priorityLevel = 2;
    }
}
/// <summary>
/// Uppercut.
/// </summary>
public class Uppercut : Attacks
{
    public Uppercut()
    {
        name = "Uppercut";
        damage = 5;
        priority = 8;
        hitFrame = 30;
        endFrame = 35;

        hitCount = 3;

        push = 1;
        lift = 11;
        rise = 12;

        initialRush = 15;
        strikeRush = 3;
        rushTime = 0.3f;

        radius = 2;
        staminaUse = 2;
        animName = "Uppercut_Right";
        particle = "PunchBlue";

        //Debug.Log("Uppercut");
        priorityLevel = 3;
    }
}
/// <summary>
/// Slam Attack
/// </summary>
public class Slam : Attacks
{
    public Slam()
    {
        damage = 8;
        priority = 8;
        hitFrame = 30;
        endFrame = 35;

        hitCount = 1;

        push = 3;
        lift = -50;
        rise = -12;

        initialRush = 0;
        strikeRush = 20;
        rushTime = 0.3f;

        radius = 2;
        staminaUse = 1;
        animName = "Sword_Verticle";
        particle = "PunchRed";

        //Debug.Log("Sword_Verticle");
        priorityLevel = 2;
    }
}
/// <summary>
/// Strong punch that has player swirl its arm
/// </summary>
public class WindUp : Attacks
{
    public WindUp()
    {
        damage = 8;
        priority = 8;
        hitFrame = 54;
        endFrame = 58;


        hitCount = 5;
        push = 8;
        lift = 0; ;
        rise = 0;

        initialRush = 0;
        strikeRush = 20;
        rushTime = 0.3f;

        radius = 2;
        staminaUse = 10;
        animName = "Wind_Up_Punch_Right";
        particle = "PunchBlue";

        //Debug.Log("Wind_Up_Punch_Right");
        priorityLevel = 3;
    }
}
/// <summary>
/// Multipunch attack
/// </summary>
public class Drill : Attacks
{
    public Drill()
    {
        damage = 8;
        priority = 8;
        hitFrame = 30;
        endFrame = 35;

        hitCount = 4;


        push = 3;
        lift = 0; ;
        rise = 0;
        radius = 2;
        staminaUse = 2;
        animName = "Drill_Punch";
        particle = "PunchBlue";

        //Debug.Log("Drill_Punch");
        priorityLevel = 3;
    }
}

/* 
 * Sword Attacks
 * 
 */

/// <summary>
/// A normal punch that send the foe backwards
/// </summary>
public class Slash : Attacks
{
    public Slash()
    {
        damage = 9;
        priority = 3;

        hitFrame = 30;
        endFrame = 35;

        hitCount = 1;

        push = 5;
        lift = 0;
        rise = 0;
        radius = 1;
        animName = "Heavy_Punch_Right";
        staminaUse = 0;
        //Debug.Log("Jab");
        particle = "PunchBlue";
        priorityLevel = 1;
    }
}

/// <summary>
/// A normal punch that send the foe backwards
/// </summary>
public class Slice : Attacks
{
    public Slice()
    {
        damage = 13;
        priority = 3;

        hitFrame = 30;
        endFrame = 35;

        hitCount = 1;

        push = 5;
        lift = 0;
        rise = 0;
        radius = 1;
        animName = "Heavy_Punch_Right";
        staminaUse = 0;
        //Debug.Log("Jab");
        particle = "PunchBlue";
        priorityLevel = 1;
    }
}

/// <summary>
/// A normal punch that send the foe backwards
/// </summary>
public class Spiral : Attacks
{
    public Spiral()
    {
        damage = 10;
        priority = 3;

        hitFrame = 30;
        endFrame = 35;

        hitCount = 1;

        push = 5;
        lift = 0;
        rise = 0;
        radius = 1;
        animName = "Heavy_Punch_Right";
        staminaUse = 0;
        //Debug.Log("Jab");
        particle = "PunchBlue";
        priorityLevel = 1;
    }
}