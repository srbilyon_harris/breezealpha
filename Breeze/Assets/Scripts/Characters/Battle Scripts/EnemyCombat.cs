/*
 * Enemy Combat Component
 * This gives an enemy basic capabilites for seeking out targets
 * 
 */

using UnityEngine;
using System.Collections;

/// <summary>
///   Enemy Combat
///   This gives an enemy basic capabilites for seeking out targets
/// </summary>
public class EnemyCombat : MonoBehaviour
{

    #region Attack Related Variables
    public Attacks attack;
    public int damage = 1;
    public int priority = 1;

    public float coolDown = 0;
    public float hitTime = 0;

    public string animName;

    public float push;
    public float lift;
    public float rise;

    public float radius;

    public float comboCool = 1.0f;
    public string particle;
    public int staminaUse;
    public int flinchStr;
    #endregion

    Enemy character;
    GameObject target;

    public void Awake()
    {
        character = this.gameObject.GetComponent<Enemy>();
    }

    /// <summary>
    /// Prepare the variables that control the parameters for the next attack
    /// </summary>
    public void PrepValues()
    {
        damage = attack.damage;

        priority = attack.priority;

        //Timer Variables
        hitTime = attack.hitFrame;

        //Knock variables
        push = attack.push;
        lift = attack.lift;
        rise = attack.rise;
        radius = attack.radius;
        animName = attack.animName;
        particle = attack.particle;
        staminaUse = attack.staminaUse;
        flinchStr = attack.priorityLevel;
    }

    /// <summary>
    /// Switches the battle state to idle and makes this object back off
    /// for a while.
    /// </summary>
    public virtual void AttackCoolDown()
    {
        character.attacking = false;
        character.status = Character.Status.none;
    }

    /// <summary>
    /// Checks to see if the target is in the FOV of this instance
    /// </summary>
    /// <returns></returns>
    public bool CanSeeTarget()
    {
        RaycastHit hit;
        var rayDirection = new Vector3();

        if (target)
            rayDirection = target.transform.position - transform.position;

        //Get the player's origin
        Vector3 offset = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        //Draw a ray in the direction the target
        Debug.DrawRay(offset, rayDirection, Color.red);
        Debug.DrawRay(offset, transform.forward * 10, Color.magenta);

        //If there is a target inbetween the FOV of this instance
        if ((Vector3.Angle(rayDirection, transform.forward)) < character.fieldOfViewRange)
        {
            // Detect if player is within the field of view
            if (Physics.Raycast(offset, rayDirection, out hit))
            {
                if (hit.transform.root.tag == "Player")
                {
                    //Debug.Log("Can see player");
                    Debug.DrawRay(offset, rayDirection, Color.green);
                    return true;
                }
                else
                {
                    //Debug.Log(hit.collider.gameObject.name);
                    //Debug.Log("Can not see player");
                    return false;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Get the next instance of a target you want to attack
    /// </summary>
    /// <returns></returns>
    public Transform GetTarget()
    {
        // Find all game objects with certain tags
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        //Container for the new target
        Character newTarget = null;

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        if (character.charType == Character.CharacterType.enemy)
        {
            // Iterate through them and find the closest one
            foreach (GameObject go in players)
            {
                var diff = (go.transform.position - position);
                var curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    newTarget = go.GetComponent<Character>();
                    distance = curDistance;
                }

            }
            if (newTarget != null)
                return newTarget.transform;
            else
                return null;
        }
        return newTarget.transform;
    }

    /// <summary>
    /// Creates a list of foes and returns it (choose "enemy" or "player")
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public GameObject[] GetFoes(string type)
    {
        if (type == "enemy")
        {
            // Find all game objects with certain tags
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            return enemies;
        }
        if (type == "player")
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            return players;
        }
        return null;
    }

    /// <summary>
    /// Performs a close ranged attack
    /// </summary>
    public virtual IEnumerator MeleeAttack(Attacks loadedAttack)
    {
        if (character.status == Character.Status.dead || character.status == Character.Status.flinching)
            yield break;

        //Debug.Log("Attacking");
        character.status = Character.Status.attacking;
        character.attacking = true;

        //Make a new attack
        character.combat.attack = loadedAttack;
        character.combat.PrepValues();

        character.anim.Stop(character.anim.animation.name);
        character.anim.Stop(character.animManager.idleAnim);
        character.animManager.bufferedAnimation = character.animManager.attackAnimPrefix;

        //Yield until the hit of the animation occurs
        yield return new WaitForSeconds(0.6f);
        if (character.status == Character.Status.dead || character.status == Character.Status.flinching)
            yield break;

        foreach (GameObject go in character.combat.GetFoes("player"))
        {
            if (go == null)
                yield return null;

            #region  Damage Player(s)
            //Apply damage to "players" only
            //if (character.AttackBoxCheck(go.GetComponent<Character>().hitbox) && go.GetComponent<Character>().status != Character.Status.dead)
            //{
             //   GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), 5);
              //  target.GetComponent<Character>().FaceTarget(character.trans.position);

                //var networkEnemy = go.GetComponent<PlayerInterpolatedNetworkViewer>().controllerScript;

                ///TODO: Convert into NON photon
                //networkEnemy.photonView.RPC("ApplyDamage", networkEnemy.photonView.owner, character.GetComponent<Character>().strength + damage, transform.forward, push, lift, flinchStr);
                //target.GetComponent<Character>().ApplyDamage(character.strength + damage, character.trans.forward, push, lift, flinchStr);

                //Get the target's transform, and make me face it 
                //var enemyTransform = go.transform;
                //FaceTarget(enemyTransform.transform);

                //Spawn a hit particle in all clients
                //SpawnHitParticle(enemyTransform.position, particle);
            //}
            #endregion
        }
        yield return new WaitForSeconds(character.attackRate);

        AttackCoolDown();
    }

    /// <summary>
    /// Seek for a new target and prepare to attack
    /// </summary>
    void PrepareToAttack()
    {
        target = character.enemyMovement.target.gameObject;

        //If not flinching or attacking
        if (character.status != Character.Status.flinching || character.status != Character.Status.dead)
        {
            if (character.GetTargetDistance(target.transform.position) >= character.minDistance)
            {
                character.enemyMovement.canMove = true;
            }
            else
            {
                character.enemyMovement.canMove = false;

                //If not attacking, check to see if the target is in view,
                //Then attack
                if (!character.attacking)
                {
                    if (CanSeeTarget())
                    {
                        //var attack = gameObject.AddComponent<Jab>();
                        //StartCoroutine(MeleeAttack(attack));
                        //Destroy(attack);
                    }
                }

            }
        }
    }

    /// <summary>
    /// Performs a close ranged attack
    /// </summary>
    public virtual IEnumerator ProjectileAttack(Attacks loadedAttack)
    {
        if (character.status == Character.Status.dead)
            yield break;

        //Debug.Log("Attacking");
        character.attacking = true;
        character.status = Character.Status.attacking;


        //Make a new attack
        character.combat.attack = loadedAttack;
        character.combat.PrepValues();

        if (!character.anim.IsPlaying(character.animManager.attackAnimPrefix))
        {
            character.anim.Play(character.animManager.attackAnimPrefix);
        }

        //Yield until the hit of the animation occurs
        yield return new WaitForSeconds(0.6f);

        if (character.status == Character.Status.dead)
            yield break;
        #region  Damage Player(s)

        if (tag == "Enemy" && character.status != Character.Status.flinching) //Check to see if I'm an enemy
        {
            if (Vector3.Distance(character.target.position, character.trans.position) < character.attackDistance)
            {
                GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), 5);
                character.target.GetComponent<Character>().FaceTarget(character.trans.position);
            }

            //GameObject projectile = (GameObject)Instantiate(Resources.Load("Prefabs/Projectile"));
            //gameObject.GetComponent<Character>().PlaySound("Sounds/Breeze/BreezeThrow01");
            //projectile.transform.position = transform.position;


            //Vector3 direction = character.target.position - projectile.transform.position;
            //direction.Normalize();

            //projectile.rigidbody.AddRelativeForce(direction.normalized * 800, ForceMode.Force);
        }
        #endregion

        yield return new WaitForSeconds(character.attackRate);
        character.attacking = false;
        character.status = Character.Status.none;

        AttackCoolDown();
    }

    public void Update()
    {
        //if (PhotonNetwork.isMasterClient)
        //{
            PrepareToAttack();
        //}
    }
}


#region Old
//using UnityEngine;
//using System.Collections;

//public class EnemyCombat : MonoBehaviour
//{

//    #region Attack Related Variables
//    public Attacks attack;
//    public int damage = 1;
//    public int priority = 1;

//    public float coolDown = 0;
//    public float hitTime = 0;

//    public string animName;

//    public float push;
//    public float lift;
//    public float rise;

//    public float radius;

//    public float comboCool = 1.0f;
//    public string particle;
//    public int staminaUse;
//    public int flinchStr;
//    #endregion

//    Enemy character;

//    public void PrepValues()
//    {
//        damage = attack.damage;

//        priority = attack.priority;

//        //Timer Variables
//        hitTime = attack.hitTime;
//        coolDown = attack.coolDown;

//        //Knock variables
//        push = attack.push;
//        lift = attack.lift;
//        rise = attack.rise;
//        radius = attack.radius;
//        animName = attack.animName;
//        particle = attack.particle;
//        staminaUse = attack.staminaUse;
//        flinchStr = attack.flinchStr;
//    }

//    public void Awake()
//    {
//        character = this.gameObject.GetComponent<Enemy>();
//    }

//    /// <summary>
//    /// Switches the battle state to idle and makes this object back off
//    /// for a while.
//    /// </summary>
//    public virtual void AttackCooldown()
//    {
//        character.status = Character.Status.none;
//    }

//    public Transform GetTarget()
//    {
//        // Find all game objects with certain tags
//        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
//        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

//        //Container for the new target
//        Character newTarget = null;

//        float distance = Mathf.Infinity;
//        Vector3 position = transform.position;

//        if (character.charType == Character.CharacterType.ally ||
//            character.charType == Character.CharacterType.player)
//        {
//            // Iterate through them and find the closest one
//            foreach (GameObject go in enemies)
//            {
//                var diff = (go.transform.position - position);
//                var curDistance = diff.sqrMagnitude;

//                if (curDistance < distance)
//                {
//                    newTarget = go.GetComponent<Character>();
//                    distance = curDistance;
//                }
//            }
//            return newTarget.transform;
//        }
//        else
//            if (character.charType == Character.CharacterType.enemy)
//            {
//                // Iterate through them and find the closest one
//                foreach (GameObject go in players)
//                {
//                    var diff = (go.transform.position - position);
//                    var curDistance = diff.sqrMagnitude;

//                    if (curDistance < distance)
//                    {
//                        newTarget = go.GetComponent<Character>();
//                        distance = curDistance;
//                    }

//                }
//                if (newTarget != null)
//                    return newTarget.transform;
//                else
//                    return null;
//            }
//        return newTarget.transform;
//    }

//    /// <summary>
//    /// Creates a list of foes and returns it (choose "enemy" or "player")
//    /// </summary>
//    /// <param name="type"></param>
//    /// <returns></returns>
//    public GameObject[] GetFoes(string type)
//    {
//        if (type == "enemy")
//        {
//            // Find all game objects with certain tags
//            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
//            return enemies;
//        }
//        if (type == "player")
//        {
//            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
//            return players;
//        }
//        return null;
//    }

//    /// <summary>
//    /// Performs a close ranged attack
//    /// </summary>
//    public virtual IEnumerator MeleeAttack(Attacks loadedAttack)
//    {
//        if (character.status == Character.Status.dead)
//            yield break;

//        //Debug.Log("Attacking");
//        character.attacking = true;

//        //Make a new attack
//        character.combat.attack = loadedAttack;
//        character.combat.PrepValues();

//        if (!character.anim.IsPlaying(character.animManager.attackAnimPrefix))
//        {
//            character.anim.Play(character.animManager.attackAnimPrefix);
//        }

//        //Yield until the hit of the animation occurs
//        yield return new WaitForSeconds(0.6f);

//        if (character.status == Character.Status.dead)
//            yield break;


//        foreach (GameObject go in character.combat.GetFoes("player"))
//        {
//            if (go == null)
//                yield return null;

//            #region  Damage Player(s)
//            //Apply damage to "players" only
//            if (character.AttackBoxCheck(go.GetComponent<Character>().hitbox))
//            {
//                if (Vector3.Distance(character.target.position, character.trans.position) < character.attackDistance)
//                {
//                    GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), 5);
//                    character.target.GetComponent<Character>().FaceTarget(character.trans.position);
                
//                    //Display Damage
//                    //var networkEnemy = go.GetComponent<PlayerNetworkViewer>().controllerScript;
//                    //networkEnemy.photonView.RPC("ApplyDamage", networkEnemy.photonView.owner, character.GetComponent<Character>().strength + damage, transform.forward, push, lift, flinchStr);

//                    //Get the target's transform, and make me face it 
//                    //var enemyTransform = go.transform;
//                    //FaceTarget(enemyTransform.transform);

//                    //Spawn a hit particle in all clients
//                   // SpawnHitParticle(enemyTransform.position, particle);

//                    character.target.GetComponent<Character>().ApplyDamage(character.strength + damage, character.trans.forward, push, lift, flinchStr);
//                }
//            }
//            #endregion
//        }


//        yield return new WaitForSeconds(character.attackRate);
//        character.attacking = false;
//        AttackCooldown();
//    }


//        /// <summary>
//    /// Performs a close ranged attack
//    /// </summary>
//    public virtual IEnumerator ProjectileAttack(Attacks loadedAttack)
//    {
//        if (character.status == Character.Status.dead)
//            yield break;

//        //Debug.Log("Attacking");
//        character.attacking = true;

//        //Make a new attack
//        character.combat.attack = loadedAttack;
//        character.combat.PrepValues();

//        if (!character.anim.IsPlaying(character.animManager.attackAnimPrefix))
//        {
//            character.anim.Play(character.animManager.attackAnimPrefix);
//        }

//        //Yield until the hit of the animation occurs
//        yield return new WaitForSeconds(0.6f);

//        if (character.status == Character.Status.dead)
//            yield break;
//        #region  Damage Player(s)

//           if (tag == "Enemy" && character.status != Character.Status.flinching) //Check to see if I'm an enemy
//           {
//                if (Vector3.Distance(character.target.position, character.trans.position) < character.attackDistance)
//                {
//                    GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), 5);
//                    character.target.GetComponent<Character>().FaceTarget(character.trans.position);
//                }

//                //GameObject projectile = (GameObject)Instantiate(Resources.Load("Prefabs/Projectile"));
//                //gameObject.GetComponent<Character>().PlaySound("Sounds/Breeze/BreezeThrow01");
//                //projectile.transform.position = transform.position;


//                //Vector3 direction = character.target.position - projectile.transform.position;
//                //direction.Normalize();

//                //projectile.rigidbody.AddRelativeForce(direction.normalized * 800, ForceMode.Force);
//        }
//        #endregion

//        yield return new WaitForSeconds(character.attackRate);
//        character.attacking = false;
//        AttackCooldown();
//    }


//}
#endregion

