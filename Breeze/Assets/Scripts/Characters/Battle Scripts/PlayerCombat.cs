/*
 * PlayerCombat
  * This adds action combat functionality to this instance
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure; // Required in C#


#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value `false'

/// <summary>
/// This adds action combat functionality to this instance
/// </summary>
public class PlayerCombat : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The two attack styles
    /// - Action is dynamic combat that utilized an attackbox
    /// - HeadBop is a traditional type that involves landing ontop 
    ///   of an adversary's head
    /// </summary>
    public enum AttackStyle { Action, HeadBop }
    public AttackStyle attackStyle = AttackStyle.HeadBop;

    /// <summary>
    /// Use of this is optional. This can be used if the player can switch 
    /// between two attacks
    /// </summary>
    public enum AttackMode { Fist, Weapon }
    public AttackMode attackMode = AttackMode.Fist;

    /// <summary>
    /// Two modes to check and see if the player is in the air 
    /// when attacking or not
    /// </summary>
    public enum AttackType { Grounded, Aerial }
    public AttackType attackType;

    #region Attack Related Variables

    Attacks attack;
    public int damage = 1;
    public int priority = 1;

    public float coolDown = 0;
    public float hitTime = 0;

    public string animName;

    public float push;
    public float lift;
    public float rise;

    public float initialRush;
    public float strikeRush;
    public float rushTime;

    public float radius;
    public float castDistance;
    public Character target;

    public float comboCool = 1.0f;
    public string particle;
    public int staminaUse;
    public int flinchStr;

    /// <summary>
    /// The amount of times an attack inflicts damage
    /// </summary>
    int hitCount = 5;

    #endregion

    #region Combo Related Values
    public int comboCount = 0;
    public int comboMax = 3;
    public float chargeCoolDown = 3, timer = 0;
    public bool attacking, chargedAttack = false;
    #endregion

    /// <summary>
    /// Reference to animation
    /// </summary>
    public Animation anim;
    public GameObject self;
    public Character character;
    public ParticleManager particleManager;
    public SFXManager sfxManager;
    #endregion

    /// <summary>
    /// Start this instance.
    /// </summary>
    public void Start()
    {
        anim = GetComponent<Animation>();
        self = this.gameObject;
        particleManager = GetComponent<ParticleManager>();
        sfxManager = GetComponent<SFXManager>();
        character = GetComponent<Character>();
    }


    /// <summary>
    /// Checks to see if the player has triggered the attack key
    /// </summary>
    public void GetInput()
    {
        //Get the character component of this player
        character = GetComponent<Character>();

        //Being in these states forbid me from attacking
        if (character.status == Character.Status.attacking || character.ledgehanging || character.status == Character.Status.flinching || character.status == Character.Status.swimming)
            return;

        #region Check for the actual attack button(s) here

        if (Input.GetMouseButtonDown(0) || InputHandler.b2 || InputHandler.jb2)
        {
            MeleeAttack1(comboCount);
        }

        if (InputHandler.b2Held && !chargedAttack)
        {
            ChargeAttack(comboCount);
            timer = 0;
            chargedAttack = true;
        }

        if (Input.GetMouseButtonDown(1) || InputHandler.b3 || InputHandler.jb3)
        { HeavyFist(comboCount); }
        #endregion
    }

    void Update()
    {
        if (character == null)
            return;

        //If we started a combo, make sure to reset it after a while
        if (comboCount > 0)
            ComboTimer();

        //Make the combo string end if at combo max
        if (comboCount == comboMax)
            ComboReset();

        //If no longer attacking, cool down everything
        if (!attacking && attackCompleted)
        {
            CoolDownTimer();
        }

        if (chargedAttack)
        {
            ChargedAttackCoolDown();
        }


        #region Keeps the player from rushing off if he attacks while on a ledge
        RaycastHit hit2;
        if (!Physics.Raycast(transform.position + transform.forward * 1.5f, Vector3.down, out hit2, 10f))
        {
            self.GetComponent<Character>().force = Vector3.zero; //Stop();
        }
        #endregion

    }

    #region Standard Fist Attack Cases
    void MeleeAttack1(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Jab>(); PrepValues(); Destroy(attack);
                break;

            case 1:
                //attack = gameObject.AddComponent<Cross>(); PrepValues(); Destroy(attack);
                break;

            case 2:
                //attack = gameObject.AddComponent<JabLeft>(); PrepValues(); Destroy(attack);
                break;
        }

    }
    void MediumFist(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Jab>(); PrepValues(); Destroy(attack);
                break;
            case 1:
                //attack = gameObject.AddComponent<Cross>(); PrepValues(); Destroy(attack);
                break;
            case 2:
                //attack = gameObject.AddComponent<Uppercut>(); PrepValues(); Destroy(attack);
                break;
        }
    }

    void ChargeAttack(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Uppercut>(); PrepValues(); Destroy(attack);
                break;
            case 1:
                //attack = gameObject.AddComponent<Uppercut>(); PrepValues(); Destroy(attack);
                break;
            case 2:
                //attack = gameObject.AddComponent<Uppercut>(); PrepValues(); Destroy(attack);
                break;
        }
    }


    void HeavyFist(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Slam>();
                //PrepValues(); Destroy(attack);
                break;
            case 1:
                //attack = gameObject.AddComponent<WindUp>();
                //PrepValues(); Destroy(attack);
                break;
            case 2:
                //attack = gameObject.AddComponent<Cross>();
                //PrepValues(); Destroy(attack);
                break;
        }
    }
    #endregion

    #region Standard Sword Attack Cases
    void LightWeapon(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Slash>(); PrepValues(); Destroy(attack);
                break;

            case 1:
                //attack = gameObject.AddComponent<Slash>(); PrepValues(); Destroy(attack);
                break;

            case 2:
                //attack = gameObject.AddComponent<Slash>(); PrepValues(); Destroy(attack);
                break;
        }

    }
    void MediumWeapon(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Slice>(); PrepValues(); Destroy(attack);
                break;
            case 1:
                //attack = gameObject.AddComponent<Slice>(); PrepValues(); Destroy(attack);
                break;
            case 2:
                //attack = gameObject.AddComponent<Slice>(); PrepValues(); Destroy(attack);
                break;
        }
    }
    void HeavyWeapon(int level)
    {
        switch (level)
        {
            case 0:
                //attack = gameObject.AddComponent<Spiral>(); PrepValues(); Destroy(attack);
                break;
            case 1:
                //attack = gameObject.AddComponent<Spiral>(); PrepValues(); Destroy(attack);
                break;
            case 2:
                //attack = gameObject.AddComponent<Spiral>(); PrepValues(); Destroy(attack);
                break;
        }
    }
    #endregion

    /// <summary>
    /// Reads in the values
    /// </summary>
    void PrepMagic()
    {
        StartCoroutine("ProjectileAttack");
    }

    /// <summary>
    /// Reads in the values
    /// </summary>
    void PrepValues()
    {
        var character = GetComponent<Character>();
        damage = attack.damage;

        priority = attack.priority;

        //Timer Variables
        hitTime = attack.hitFrame;
        hitCount = attack.hitCount;

        initialRush = attack.initialRush;
        strikeRush = attack.strikeRush;
        rushTime = attack.rushTime;

        //Knock variables
        push = attack.push;
        lift = attack.lift;
        rise = attack.rise;
        radius = attack.radius;
        animName = attack.animName;
        comboCount++;
        particle = attack.particle;
        staminaUse = attack.staminaUse;
        flinchStr = attack.priorityLevel;

        //Start the attack sequence
        if (staminaUse <= character.stamina)
        {
            character.stamina -= staminaUse;
            StartCoroutine("Attack");
        }
        else
        {
            character.attacking = false;
            AttackReset();
        }
    }

    ///// <summary>
    ///// Check if target is inside this character's attackbox
    ///// </summary>
    ///// <param name="t"></param>
    ///// <returns></returns>
    //public bool AttackBoxCheck(Collider t)
    //{
    //    //if (GetComponent<Character>().attackbox.collider.bounds.Intersects(t.bounds))
    //    //{
    //    //    print(t.name + " is inside attackbox");
    //    //    return true;
    //    //}
    //    //return false;
    //}
    /// <summary>
    /// Check if prop target is inside this character's attackbox
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    //public bool PropHitBoxCheck(Collider t)
    //{
    //    if (character.attackbox.collider.bounds.Intersects(t.bounds))
    //    {
    //        print(t.name + " is inside attackbox");
    //        return true;
    //    }
    //    return false;
    //}

    /// <summary>
    /// [Modified for Multiplayer]
    /// Applies damage and knockback to a foe
    /// </summary>
    /// <returns></returns>
    //IEnumerator Attack()
    //{
    //    //Use stamina if the player has any and needs it
    //    if (staminaUse <= character.stamina)
    //    {
    //        character.stamina -= staminaUse;
    //        character.attacking = true;
    //    }
    //    else //Cancel the attack if the player doens't have enough stamina for this attack
    //        if (staminaUse > character.stamina)
    //        {
    //            character.attacking = false;
    //            yield break;
    //        }

    //    //Put the player into an attack state
    //    character.status = Character.Status.attacking;
    //    attackCompleted = false;
    //    coolDownTime = coolDown;

    //    //*********************************
    //    //**Perform Attacking Logic here**
    //    //*********************************
    //    #region Attack Logic
    //    comboTime = maxComboTime;

    //    anim.Stop(anim.animation.name);
    //    anim.Stop(character.animManager.idleAnim);
    //    character.animManager.bufferedAnimation = animName;

    //    anim[animName].speed = 2;
    //    hitTime = hitTime / 2;
    //    coolDown = coolDown / 2;

    //    #region Store all types of targets
    //    GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
    //    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
    //    GameObject[] props = GameObject.FindGameObjectsWithTag("Props");
    //    #endregion

    //    //Stop any current movement or force being applied to the player
    //    self.GetComponent<Character>().Stop();

    //    #region Find the closest foe
    //    target = FindClosestTarget();

    //    //Initial Rush
    //    //self.GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), 4);

    //    if (target != null)
    //    {
    //        //Face the closest foe
    //        if (Vector3.Distance(target.transform.position, transform.position) < 5)
    //        {
    //            self.GetComponent<Character>().FaceTarget(target.trans.position);
    //        }
    //    }
    //    #endregion

    //    //Apply Initial Rush
    //    self.GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), initialRush);

    //    //Continue when hit frame happens
    //    yield return new WaitForSeconds(hitTime);

    //    #region If this attack makes me go airborne, apply it
    //    if (self.GetComponent<CharacterController>().isGrounded && rise > 0)
    //        self.GetComponent<Character>().Upward(rise);
    //    else
    //        if (!self.GetComponent<CharacterController>().isGrounded && rise < 0)
    //            self.GetComponent<Character>().Upward(rise);

    //    RaycastHit hit;

    //    //Rush towards foe
    //    if (Physics.Raycast(transform.position + transform.forward * 1.5f, Vector3.down, out hit, 10f))
    //    {
    //        StartCoroutine(self.GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), strikeRush, rushTime));
    //    }
    //    //If close to the target, cancel out the rush force
    //    if (target != null && Vector3.Distance(target.transform.position, transform.position) < 2)
    //    {
    //        self.GetComponent<Character>().Stop();
    //    }

    //    //If flinching, break out of the attack
    //    if (character.status == Character.Status.flinching)
    //    {
    //        AttackReset();
    //        character.attacking = false;
    //        attackCompleted = true;
    //        yield break;
    //    }
    //    #endregion

    //    #region The attack
    //    if (!attackCompleted)
    //    {
    //        for (var i = 0; i < hitCount; i++)
    //        {
    //            GameObject hitEnemy = null;

    //            #region Attack all enemies in the attack radius
    //            foreach (GameObject go in enemies)
    //            {
    //                //If enemy is far away, rush forward
    //                //if (Vector3.Distance(go.transform.position, transform.position) < radius * 5)
    //                if (go == null)
    //                    yield return null;

                    ////print(go.name);
                    //if (go != null && AttackBoxCheck(go.GetComponent<Character>().hitbox))
                    //{
                    //    hitEnemy = go;

                    //    //Display Damage
                    //    var networkEnemy = go.GetComponent<Character>();//.controllerScript;
                    //    //NETWORKING
                    //    //networkEnemy.photonView.RPC("ApplyDamage", PhotonTargets.MasterClient, character.GetComponent<Character>().strength + damage, transform.forward, push, lift, flinchStr);
                    //    networkEnemy.ApplyDamage(character.GetComponent<Character>().strength + damage, transform.forward, push, lift, flinchStr);

                    //    #if !UNITY_WEBPLAYER
                    //    GamePad.SetVibration(0, 1, 0);
                    //    #endif

                    //    //if (Camera.mainCamera.GetComponent<ThirdPersonCamera>())
                    //    //   Camera.mainCamera.GetComponent<ThirdPersonCamera>().SnapBehindTarget();

                    //    //Get the target's transform, and make me face it 
                    //    var enemyTransform = go.transform;

                    //    //NETWORKING
                    //    //photonView.RPC("FaceTarget", PhotonTargets.All, enemyTransform.transform.position);
                    //    character.FaceTarget(enemyTransform.position);

                    //    //Spawn a hit particle in all clients
                    //    //NETWORKING
                    //    //photonView.RPC("SpawnHitParticle", PhotonTargets.All, go.GetComponent<Enemy>().hitbox.transform.position, particle);
                    //    particleManager.SpawnParticle(go.GetComponent<Enemy>().hitbox.transform.position, particle);

                    //    sfxManager.PlaySound(sfxManager.hit1);
                    //}
               // }
                //yield return new WaitForSeconds(0.05f);
           // }
            //    #endregion

            //#region Attack all props in the attack radius
            //foreach (GameObject go in props)
            //{
            //    //If enemy is far away, rush forward
            //    if (go == null)
            //        yield return null;

//                if (PropHitBoxCheck(go.GetComponent<DamageReciever>().hitbox))
//                {
//                    for (var i = 0; i < hitCount; i++)
//                    {
//                        if (Resources.Load("Particles/" + particle))
//                        {
//                            GameObject newParticle = (GameObject)Instantiate(Resources.Load("Particles/" + particle));
//                            newParticle.transform.position = go.transform.position;
//                        }

//                        go.GetComponent<DamageReciever>().ApplyDamage(1, transform.position);

//#if !UNITY_WEBPLAYER
//                        GamePad.SetVibration(0, 1, 0);
//#endif

//                        sfxManager.PlaySound(sfxManager.hit2);
//                        yield return new WaitForSeconds(0.1f);
//                    }
//                }
//            }
//            #endregion

//            //If I'm at the end of an attack or combo, reset the combo counter
//            if (comboCount == comboMax)
//            {
//                ComboReset();
//            }

//            attackCompleted = true;
//            yield return new WaitForSeconds(0.1f);

//#if !UNITY_WEBPLAYER
//            GamePad.SetVibration(0, 0, 0);
//#endif

//        #endregion
//            //*********************************
//        }
//        #endregion
//    }

    /// <summary>
    /// Performs a close ranged attack
    /// </summary>
    public virtual IEnumerator ProjectileAttack()
    {
        if (character.status != Character.Status.attacking)
        {
            character.status = Character.Status.attacking;
            character.animManager.bufferedAnimation = character.animManager.castingAnimPrefix;
            character.characterState = Character.CharacterState.casting;

            #region Find the closest foe
            target = FindClosestTarget();

            if (target != null)
            {
                //Face the closest foe
                if (Vector3.Distance(target.transform.position, transform.position) < 20f)
                {
                    self.GetComponent<Character>().FaceTarget(target.trans.position);
                }
            }
            #endregion
            //Yield until the hit of the animation occurs
            yield return new WaitForSeconds(0.3f);

            // if (character.status == Character.Status.dead)
            //    yield break;
            #region  Damage Player(s)

            if (tag == "Player" && character.status != Character.Status.flinching) //Check to see if I'm an enemy
            {

                if (target != null)
                {
                    if (Vector3.Distance(target.transform.position, character.trans.position) < 50f)
                    {
                        //    GetComponent<Character>().Rush(new Vector3(transform.forward.x, 0, transform.forward.z), 5);
                        target.GetComponent<Character>().FaceTarget(character.trans.position);
                    }
                }

                //NETWORKING
                //GameObject projectile = PhotonNetwork.Instantiate("Fireball", transform.position, Quaternion.identity, 0, null);//(GameObject) PhotonView.Instantiate(Resources.Load("Fireball"));
                GameObject projectile = (GameObject)Instantiate(Resources.Load("Fireball"), transform.position, Quaternion.identity);//(GameObject) PhotonView.Instantiate(Resources.Load("Fireball"));
                //gameObject.GetComponent<Character>().PlaySound("Sounds/Breeze/BreezeThrow01");
                sfxManager.PlaySound(sfxManager.toss);

                projectile.transform.position = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);


                Vector3 direction = character.transform.forward;
                direction.Normalize();

                projectile.rigidbody.AddRelativeForce(direction.normalized * 800, ForceMode.Force);
            }
            #endregion

            yield return new WaitForSeconds(1f);
            AttackReset();
        }
    }

    float comboTime = 2f;
    float maxComboTime = 2f;

    /// <summary>
    /// A timer that counts down to reset the camera
    /// </summary>
    void ComboTimer()
    {
        comboTime -= Time.deltaTime;

        if (comboTime < 0)
        {
            ComboReset();
        }
    }

    /// <summary>
    /// A timer that counts down to reset the camera
    /// </summary>
    void ChargedAttackCoolDown()
    {
        timer += Time.deltaTime;

        if (timer >= chargeCoolDown)
        {
            chargedAttack = false;
        }
    }


    float coolDownTime = 1f;
    bool attackCompleted = false;

    /// <summary>
    /// A timer that counts down to reset the camera
    /// </summary>
    void CoolDownTimer()
    {
        coolDownTime -= Time.deltaTime;

        if (coolDownTime < 0)
        {
            AttackReset();
        }
    }

    /// <summary>
    /// Finds the closest enemy and sets him as the target
    /// </summary>
    /// <returns></returns>
    Character FindClosestTarget()
    {
        // Find all game objects with certain tags
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        //Container for the new target
        Character newTarget = null;

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        if (self.GetComponent<Character>().charType == Character.CharacterType.ally ||
            self.GetComponent<Character>().charType == Character.CharacterType.player)
        {
            // Iterate through them and find the closest one
            foreach (GameObject go in enemies)
            {
                var diff = (go.transform.position - position);
                var curDistance = diff.sqrMagnitude;

                if (curDistance < distance)
                {
                    newTarget = go.GetComponent<Character>();
                    distance = curDistance;
                }
            }
            return newTarget;
        }
        else
            if (self.GetComponent<Character>().charType == Character.CharacterType.enemy)
                // Iterate through them and find the closest one
                foreach (GameObject go in players)
                {
                    var diff = (go.transform.position - position);
                    var curDistance = diff.sqrMagnitude;

                    if (curDistance < distance)
                    {
                        newTarget = go.GetComponent<Character>();
                        distance = curDistance;
                    }
                }


        return newTarget;
    }

    /// <summary>
    /// Reset the combo string
    /// </summary>
    /// <returns></returns>
    void ComboReset()
    {
        InputHandler.button3Timer = 0; InputHandler.b3Held = false; InputHandler.jb3Held = false;
        InputHandler.button2Timer = 0; InputHandler.b2Held = false; InputHandler.jb2Held = false;
        comboCount = 0;
        comboTime = maxComboTime;
        //chargedAttack = false;
    }

    public void AttackReset()
    {
        attackCompleted = false;
        character.attacking = false;
        character.status = Character.Status.none;
    }
}


