using UnityEngine;
using System.Collections;

public class Floating : MonoBehaviour
{

    #region Variables

    /// <summary>
    /// Reference to the transfrom object
    /// </summary>
    Transform trans;

    /// <summary>
    /// Target position
    /// </summary>
    public Transform target;

    /// <summary>
    /// Average value, offset from zero. 
    /// This is the point at which the object will float around
    /// </summary>
    public float z = 0;

    /// <summary>
    /// Amplitude, this is how high and low the curve will go
    /// </summary>
    public float a = 1;

    /// <summary>
    /// Angular Frequency, this is how fast it will traverse the curve
    /// </summary>
    public float b = 3f;

    /// <summary>
    /// Phase Angle, check back for more details
    /// </summary>
    public float c = .5f;

    /// <summary>
    /// this is how far the curve has been traversed
    /// </summary>
    public float x = .01f;

    public float speed =.01f;

    #endregion


    // Use this for initialization
	void Start () {
        trans = this.transform;
	}
	
	// Update is called once per frame
	void LateUpdate () 
    {

        MoveToPoint();
        //y = z+ a * sin(bx + c)
        trans.localPosition = new Vector3(trans.localPosition.x, 
                                    (z + a * Mathf.Sin(b*x + c)), 
                                    trans.localPosition.z);
        x += Time.smoothDeltaTime;
    }

    public void MoveToPoint ()
    {
        if (target == null)
            return;
        transform.parent.position = Vector3.Slerp(transform.parent.position, target.position+Vector3.up*2, speed);

        trans.LookAt(target);
    }

    public void SetTarget()
    {

    }
}
