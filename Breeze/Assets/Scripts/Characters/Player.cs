/*
 * Player.cs
 * Inherits from character. This script can be attached to the player object and 
 * takes care of movement and all other player features.
 * 
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable 0168 // variable declared but not used. <- LOL
#pragma warning disable 0219 // variable assigned but not used. <- RFLOCOPTER
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value `false'

/// <summary>
/// These required components are attached automatically
/// </summary>
[RequireComponent(typeof(ParticleManager))]
[RequireComponent(typeof(AnimationManager))]
[RequireComponent(typeof(SFXManager))]


/// <summary>
///  Inherits from character. This script can be attached to the player object and 
///  takes care of movement and all other player features.
/// </summary>
public class Player : Character
{
    #region Variables

    int playerID;
    float controllerSize;
    float crouchControllerSize = 1;
    float controllerBase;
    //public float fallTime = 2f;
    private float terminalFallTime = 2f;

    #region Variables [ints]
    public int money;
    #endregion

    #region Variables [RayCasts]
    private RaycastHit leftRayHit;
    private RaycastHit rightRayHit;
    private RaycastHit upRayHit;
    private RaycastHit bottomRayHit;
    #endregion

    #region Variables[Bool]
    bool justLanded = false;
    bool isControllable = false;
    bool isRemotePlayer = false;
    public bool canLedgehang = true;
    public bool canWallClimb = true;
    #endregion

    #region Variables[Vector3]
    Vector3 velocity;
    Vector3 tempDir;

    #endregion

    #region Variables[Objects]
    /// <summary>
    /// The combat componet of the player
    /// </summary>
    public PlayerCombat combat;

    /// <summary>
    /// The Character component of the player
    /// </summary>
    public Character character;
    PickUpItem pickUpItem;
    #endregion

    #endregion


    #region Methods
    /// <summary>
    /// Assign the character type to player, grabs the combat component, assigns the camera target
    /// </summary>
    protected void Awake()
    {

        //Make sure that this character is set as a player
        charType = Player.CharacterType.player;

        #region Get all the required components
        character = gameObject.GetComponent<Character>();
        pickUpItem = gameObject.GetComponentInChildren<PickUpItem>();
        combat = gameObject.GetComponent<PlayerCombat>();
        #endregion

        //Attempt to assign me as the camera target
        if (Camera.mainCamera.GetComponent<ThirdPersonCamera>() != null)
        {
            if (Camera.mainCamera.GetComponent<ThirdPersonCamera>().cameraTarget == null)
            {
                Camera.mainCamera.GetComponent<ThirdPersonCamera>().cameraTarget = this.gameObject;
            }
        }
        else
        {
            Debug.LogWarning("Your main camera doesn't have a ThirdPersonCamera attached to it!");
        }
    }

    /// <summary>
    /// Assign this object as the main camera's target
    /// </summary>
    /// <param name="level"></param>
    void OnLevelWasLoaded(int level)
    {
        if (Camera.mainCamera.GetComponent<ThirdPersonCamera>() != null)
        {
            if (Camera.mainCamera.GetComponent<ThirdPersonCamera>().cameraTarget == null)
            {
                Camera.mainCamera.GetComponent<ThirdPersonCamera>().cameraTarget = this.gameObject;
            }
        }
        else
        {
            Debug.LogWarning("Your main camera doesn't have a ThirdPersonCamera attached to it!");
        }
    }

    /// <summary>
    /// Calls start from the character base, and grabs the sizes of the character controller.
    /// We will be manipulating the size in the crouching funciton
    /// </summary>
    public override void Start()
    {
        base.Start();
        controllerSize = controller.height;
        controllerBase = controller.center.y;
    }

    void Update()
    {
        ///Run the animation framework for this instance
        animManager.AnimationFramework();

        #region Keeps stats from exceeding thier cap
        if (health > maxHealth)
            health = maxHealth;

        if (stamina > maxStamina)
            stamina = maxStamina;
        #endregion

        if (hitTerminalVelocity)
        {
            print("Entering Terminal Velocity");
        }

        ///Run the State Machine
        StateMachine();

        if (status != Status.climbing)
            CheckIfGrounded();

        MoveDirectionCheck();
    }

    /// <summary>
    /// The state machine of the player
    /// </summary>
    void StateMachine()
    {
        //TODO: Revise this area so that it uses a characterState switch
        switch (status)
        {
            ///Normal Status
            case Status.none:
                Movement();
                //combat.GetInput();

                Cry();
                CheckForLedges();
                break;

            case Status.pushing:
                PushMovement();
                break;

            case Status.staggered:
                StopMovement();
                break;

            case Status.flinching:
                character.characterState = CharacterState.flinching;
                StopMovement();
                //combat.AttackReset();
                break;

            case Status.walljumped:
                Movement();
                break;

            case Status.swimming:
                characterState = CharacterState.swimming;
                SwimMovement();
                SwimmingPhysics();
                Cry();
                break;

            case Status.dead:
                //StopMovement();
                characterState = CharacterState.dead;
                break;

            case Status.climbing:
                //TODO: Figure out what to put in status.climbing...
                WallClimb();
                break;

            case Status.attacking:
                Movement();
                //StopMovement();
                //TODO: Put in special cases depending on the current attack state...
                break;

            case Status.gliding:
                Glide();
                break;

            default:
                //StopMovement();
                break;
        }

        switch (infliction)
        {
            case Infliction.frozen:
                status = Status.staggered;
                StartCoroutine("RemoveInfliction", 3f);
                break;

            case Infliction.shocked:
                status = Status.staggered;
                StartCoroutine("Shocked");
                StartCoroutine("RemoveInfliction", 10f);
                break;

            case Infliction.poisoned:
                StartCoroutine("Poisoned");
                StartCoroutine("RemoveInfliction", 10f);
                break;

            case Infliction.dizzy:
                StartCoroutine("Dizzy");
                break;
        }
    }

    /// <summary>
    /// NOT WORKING YET: Allows for an instance to hop off a wall
    /// </summary>
    void WallJumpCheck()
    {
        if (speed == runSpeed) //characterState == CharacterState.jumping && 
        {
            Vector3 fwd = transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position, fwd, 1)) //Input.GetKeyUp(KeyCode.Space) &&
            {
                //characterState = CharacterState.jumping;
                status = Status.walljumped;
                //trans.forward = -fwd;
                //Rush(-fwd, 20);
                Upward(10);
                StartCoroutine("WallJumped");
            }
        }
    }

    /// <summary>
    /// Disables movement
    /// </summary>
    void StopMovement()
    {
        #region In Case we are Blocking or "Cant Move"

        var _holdTheJump = direction.y;


        //Don't want our character pointing up if we jump ;; true story bro
        direction.y = 0;

        //We only want direction at this point
        direction.Normalize();

        speed = Mathf.Lerp(speed, 0, accelerationSpeed);

        var vel = (direction) * speed;
        velocity = Vector3.Lerp(velocity, vel, 5 * accelerationSpeed * accelerationSpeed * Time.deltaTime);

        //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
        direction = (direction.magnitude > 1) ? new Vector3(direction.x * speed, _holdTheJump, direction.z * speed)
                                              : new Vector3(trans.forward.x * speed, _holdTheJump, trans.forward.z * speed);

        velocity.y = _holdTheJump;
        direction = velocity;

        Gravity();
        controller.Move(direction * Time.deltaTime);
        #endregion
    }

    public bool staggerOnLand = false;

    /// <summary>
    /// Gravity for Player
    /// Allows for jumping when grounded or when in water
    /// </summary>
    protected override void Gravity()
    {
        if (charType == CharacterType.player)
        {
            RaycastHit hit;
            Vector3 down = new Vector3(trans.position.x, trans.position.y - 2f, trans.position.z);
            Debug.DrawLine(trans.position, down, Color.cyan);

            if (characterState != CharacterState.swimming)
            {
                //If not on the ground, assume normal gravity
                if (!controller.isGrounded)
                {
                    #region Falling
                    if (characterState != CharacterState.gliding)
                    {
                        direction.y -= (direction.y > -gravity) ? gravity * Time.deltaTime : 0;

                        if (direction.y < -5)
                        {
                            if (characterState == CharacterState.crouching || characterState == CharacterState.crawling)
                            { UnCrouch(); }

                            characterState = CharacterState.falling;
                        }
                    }
                    #endregion

                    #region Gliding
                    //Disables gliding from happening if the player is in a cutscene or incapcitated in any way
                    if (status != Status.dead && status != Status.staggered && characterState != CharacterState.swimming)
                    {
                        ////if ((InputHandler.buttonUHeld) && (characterState == CharacterState.jumping || characterState == CharacterState.falling))
                        ////{
                        ////    characterState = CharacterState.gliding;
                        ////    Glide();
                        ////}

                        if (characterState == CharacterState.gliding)
                        {
                            moveState = MoveState.Stationary;
                            characterState = CharacterState.falling;
                        }
                    }
                    #endregion
                }
                else
                    #region Free Falling
                    if (!Physics.Raycast(trans.position, Vector3.down, out hit, 2f) && characterState != CharacterState.jumping)
                    {
                        var velocity = direction;

                        velocity.y = direction.y;
                        direction.y = velocity.y;

                        direction.y = .01f;
                        direction.y -= (direction.y > -gravity) ? gravity * Time.deltaTime : 0;

                        if (direction.y < -1f)
                        {
                            characterState = CharacterState.falling;
                        }
                    }

                if (staggerOnLand && Physics.Raycast(trans.position, Vector3.down, out hit, 1.5f) && character.characterState != CharacterState.jumping)
                {
                    //print("Just Landed");
                    staggerOnLand = false;
                    characterState = CharacterState.landing;

                    StartCoroutine("Landed");
                }
                else
                    if (character.characterState == CharacterState.falling && !Physics.Raycast(trans.position, Vector3.down, out hit, 1.5f))
                    {
                        //print("In the air");
                        staggerOnLand = true;

                    }
                    #endregion

                //Needed to make the corresponding animations work
                if (controller.isGrounded || climbState == ClimbState.wall || climbState == ClimbState.ledge)
                {
                    anim.Stop(animManager.jumpAnim);
                    anim.Stop(animManager.fallAnim);
                    anim.Stop(animManager.glideMoveAnim);
                }

                canWallClimb = true;
                canLedgehang = true;

                //Jumping
                //Enable Jumping. The player cannont jump if there is something directly above him (or her :D)
                #region Jumping on land
                if (status != Status.dead && status != Status.staggered && !CheckAbove())
                {
                    if (Input.GetKeyDown(KeyCode.Space) || InputHandler.jb4  || InputHandler.b4)
                    {
                        //Debug.Log("Hi");
                        if (characterState == CharacterState.crouching || characterState == CharacterState.crawling)
                        {
                            UnCrouch();
                            Upward(8);
                        }
                        else
                        {
                            Upward();
                            print("I should be jumping");
                        }
                    }
                }
                #endregion
            }
            else
            {
                direction.y -= (direction.y > -(gravity / 4)) ? (gravity / 4) * Time.deltaTime : 0;
            }

        }
    }

    public bool wallJumping = false;

    /// <summary>
    /// Allows the player to swim, Mario Style
    /// </summary>
    public void SwimmingPhysics()
    {
        #region Jumping out of water
        //Enable Jumping. The player cannont jump if there is something directly above him (or her :D)
        if (characterState == CharacterState.swimming && !CheckAbove())
        {
            if (Input.GetKeyDown(KeyCode.Space) || InputHandler.b4 || Input.GetKeyDown(KeyCode.JoystickButton1))
            {
                status = Status.none;
                characterState = CharacterState.jumping;
                Upward(8);
            }
        }
        #endregion
    }

    /// <summary>
    /// Movement Function for the player.
    /// </summary>
    public virtual void Movement()
    {
        if (climbState != ClimbState.none)
        { return; }

        //Get the forward direction of the camera
        if (Camera.mainCamera != null)
            tempDir = Camera.mainCamera.transform.forward * InputMovement().y + InputMovement().x * Camera.mainCamera.transform.right;

        if (status == Status.walljumped)
            direction = new Vector3(direction.x, direction.y, direction.z); //Keeps movement from happening
        else
        {
                direction = new Vector3(tempDir.x, direction.y, tempDir.z); //Input movement is used
        }

            //Correctly set the target speed
            targetSpeed = (tempDir.magnitude != 0) ? walkSpeed : 0;

            //Snap speed to 0 if going SUPER slow
            if (speed < .01f) speed = 0;

            //NOTE: Without this status check, the player will attempt to snap in the direction that the joystick is pointing
            //However, with the autolocking, the combat is also trying to direct the character's direction, so this hack fixes that
            if (status != Status.attacking)
            {
                //Snap movement 
                if (new Vector3(tempDir.x, 0, tempDir.z).normalized.magnitude > 0.9f)
                    trans.forward = new Vector3(direction.x, 0, direction.z);
            }

            //WE need this here in order to store the downward/upward force of the player
            var _holdTheJump = direction.y;
            
            //Don't want our character pointing up if we jump
            direction.y = 0;

            //We only want direction at this point
            direction.Normalize();

            //Keeps player from moving if he is attacking
            if (status != Status.attacking)
            {
                #region Sprinting
                if ((Input.GetKey(KeyCode.LeftShift) || InputHandler.bL2Held || InputHandler.jbL2Held || Input.GetAxis("LeftTrigger") > 0) && (characterState != CharacterState.gliding))
                {
                    sprinting = true;
                    speed = Mathf.Lerp(speed, runSpeed, accelerationSpeed);
                }
                else
                {
                    sprinting = false;
                    speed = Mathf.Lerp(speed, targetSpeed, accelerationSpeed);
                }
                #endregion
            }
            else
                speed = 0;

            var vel = (direction) * speed;

            velocity = Vector3.Lerp(velocity, vel, 5 * accelerationSpeed * accelerationSpeed * Time.deltaTime);

            //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
            direction = (direction.magnitude > 1) ? new Vector3(direction.x * speed, _holdTheJump, direction.z * speed)
                                                  : new Vector3(trans.forward.x * speed, _holdTheJump, trans.forward.z * speed);

            velocity.y = _holdTheJump;
            direction = velocity;

            Gravity();

            float currentHeight = 0;

            if (_holdTheJump > 0)
                currentHeight = trans.position.y;

        //////////////////////////////////////
        //Allows for knockback, rush and sliding to work
        if (force.magnitude > 1)
        {
            force *= Mathf.Lerp(force.magnitude, 0, .03f) / force.magnitude;
            controller.Move(force * Time.smoothDeltaTime);
        }
        else if (force.magnitude > 0)
            force = Vector3.zero;
        ///////////////////////////////////////

        //Must happen last
        //Slide();

        //Reapply the vertical force and speed to the movement and override basic gravity
        controller.Move(direction * Time.deltaTime);

        if (currentHeight == trans.position.y)
            direction.y = 0;
    }

    #region Optional Movement Variations
    /// <summary>
    /// Push Movement Function for the player.
    /// </summary>
    public virtual void PushMovement()
    {
        if (Camera.mainCamera != null)
            tempDir = transform.forward * InputMovement().y;//Camera.mainCamera.transform.forward * InputMovement().y + InputMovement().x * Camera.mainCamera.transform.right;

        direction = new Vector3(tempDir.x, direction.y, tempDir.z); //Input movement is used

        targetSpeed = (tempDir.magnitude != 0) ? runSpeed : 0;

        //Snap speed to 0 if going SUPER slow
        if (speed < .01f) speed = 0;

        var _holdTheJump = direction.y;

        //Don't want our character pointing up if we jump
        direction.y = 0;

        //We only want direction at this point
        direction.Normalize();

        #region Sprinting
        if ((Input.GetKey(KeyCode.LeftShift)) && (characterState != CharacterState.gliding))
            speed = Mathf.Lerp(speed, targetSpeed * 2, accelerationSpeed);
        else if (Input.GetKeyUp(KeyCode.LeftShift) || characterState == CharacterState.gliding)
            speed = Mathf.Lerp(speed, targetSpeed, accelerationSpeed);
        #endregion

        var vel = (direction) * speed;

        velocity = Vector3.Lerp(velocity, vel, 5 * accelerationSpeed * accelerationSpeed * Time.deltaTime);

        //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
        direction = (direction.magnitude > 1) ? new Vector3(trans.position.x, _holdTheJump, direction.z * speed)
                                              : new Vector3(trans.position.x, _holdTheJump, trans.forward.z * speed);
        velocity.y = 0;
        direction = velocity;

        Gravity();

        //////////////////////////////////////
        //Allows for knockback, rush and sliding to work
        if (force.magnitude > 1)
        {
            force *= Mathf.Lerp(force.magnitude, 0, .03f) / force.magnitude;
            controller.Move(force * Time.smoothDeltaTime);
        }
        else if (force.magnitude > 0)
            force = Vector3.zero;
        ///////////////////////////////////////

        //Reapply the vertical force and speed to the movement and override basic gravity
        controller.Move(direction * Time.deltaTime);
    }

    /// <summary>
    /// Swim Movement Function for the player.
    /// </summary>
    public virtual void SwimMovement()
    {
        if (climbState != ClimbState.none || status == Status.attacking)
        { return; }
        //Get the forward direction of the camera
        if (Camera.mainCamera != null)
            tempDir = Camera.mainCamera.transform.forward * InputMovement().y + InputMovement().x * Camera.mainCamera.transform.right;

        if (!wallJumping)
        {

            // if (!wallJumping)
            //{
            //     direction = new Vector3(direction.x, direction.y, direction.z); //Keeps movement from happening
            if (status == Status.walljumped)
                direction = new Vector3(direction.x, direction.y, direction.z); //Keeps movement from happening
            else
                direction = new Vector3(tempDir.x, direction.y, tempDir.z); //Input movement is used
            //}

            //Correctly set the target speed
            targetSpeed = (tempDir.magnitude != 0) ? walkSpeed : 0;

            //Snap speed to 0 if going SUPER slow
            if (speed < .01f) speed = 0;

            //Snap movement 
            if (new Vector3(tempDir.x, 0, tempDir.z).normalized.magnitude > 0.9f)
                trans.forward = new Vector3(direction.x, 0, direction.z);

            var _holdTheJump = direction.y;

            //Don't want our character pointing up if we jump
            direction.y = 0;

            //We only want direction at this point
            direction.Normalize();

            ///TODO: Add in the sprint button for swimming
            #region Sprinting
            if ((Input.GetKey(KeyCode.LeftShift) || InputHandler.jbL2Held) && (characterState != CharacterState.gliding))
            {
                sprinting = true;
                speed = Mathf.Lerp(speed, runSpeed, accelerationSpeed);
            }
            else if ((Input.GetKeyUp(KeyCode.LeftShift) || Input.GetAxis("LeftTrigger") == 0) || characterState == CharacterState.gliding)
            {
                sprinting = false;
                speed = Mathf.Lerp(speed, targetSpeed, accelerationSpeed);
            }
            #endregion

            var vel = (direction) * speed;

            velocity = Vector3.Lerp(velocity, vel, 5 * accelerationSpeed * accelerationSpeed * Time.deltaTime);

            //If we've got a signification magnitude, continue moving forward ;; if were are recieving movement, apply it 
            direction = (direction.magnitude > 1) ? new Vector3(direction.x * speed, _holdTheJump, direction.z * speed)
                                                  : new Vector3(trans.forward.x * speed, _holdTheJump, trans.forward.z * speed);

            velocity.y = _holdTheJump;
            direction = velocity;

        }

        Gravity();

        //////////////////////////////////////
        //Allows for knockback, rush and sliding to work
        if (force.magnitude > 1)
        {
            force *= Mathf.Lerp(force.magnitude, 0, .03f) / force.magnitude;
            controller.Move(force * Time.smoothDeltaTime);
        }
        else if (force.magnitude > 0)
            force = Vector3.zero;
        ///////////////////////////////////////

        //Must happen last
        //Slide();

        //Reapply the vertical force and speed to the movement and override basic gravity
        controller.Move(direction * Time.deltaTime);
    }
    #endregion

    #region Player Features
    /* This includes all the possible features your player has */

    /// <summary>
    /// Crouching behavior for the characters
    /// </summary>
    void Crouch()
    {
        print("Crouching");
        if (InputMovement() == Vector2.zero)
            characterState = CharacterState.crouching;
        else
            characterState = CharacterState.crawling;

        controller.height = crouchControllerSize;
        controller.center = new Vector3(0, 0.63f, 0);
    }

    /// <summary>
    /// Uncrouch the player
    /// </summary>
    void UnCrouch()
    {
        characterState = CharacterState.grounded;
        controller.height = controllerSize;
        controller.center = new Vector3(0, controllerBase, 0);
    }

    /// <summary>
    /// Checks for ledges.
    /// </summary>
    public void CheckForLedges()
    {
        if (status != Status.none) //sliding || 
        {
            return;
        }
        //Create the two rays need to calculate if the player is hanging on the ledge
        RaycastHit torsoHitCast, topHitCast;

        var fwd = transform.TransformDirection(Vector3.forward);
        Vector3 t; t = transform.position;

        //If the top ray isn't hitting anything, but the torso cast is, and we aren't on the ground
        if (!Physics.Raycast(new Vector3(t.x, t.y + topOffset, t.z), fwd, out topHitCast, 1)
            && Physics.Raycast(new Vector3(t.x, t.y + torsoOffset, t.z), fwd, out torsoHitCast, 1)
            && direction.y < 2 && !controller.isGrounded)
        {
            //If the ray hit something with a mesh collider or box collider, and it isn't a prop or is in layer 8...
            if ((torsoHitCast.collider.GetType() == typeof(MeshCollider) || torsoHitCast.collider.GetType() == typeof(BoxCollider))
                && (torsoHitCast.collider.tag != "Prop" && torsoHitCast.collider.tag != "Prop" &&
                    torsoHitCast.collider.gameObject.layer != 8 && torsoHitCast.collider.gameObject.layer != 8))
            {
                //Jump off of the ledge
                if (Input.GetKeyDown(KeyCode.Space) || InputHandler.jb4 || InputHandler.b4)
                {
                    Upward(10);
                    //Not sure why I did this... :P
                    if (characterState == CharacterState.crouching || characterState == CharacterState.crawling)
                    { UnCrouch(); }

                    //Change the character state to jumping
                    characterState = CharacterState.jumping;

                    //No longer on a ledge
                    climbState = ClimbState.none;
                }
                else
                {
                    //Make sure that I'm not descending
                    direction.y = 0;

                    //Rotate to face the ledge
                    trans.rotation = Quaternion.LookRotation(new Vector3(-torsoHitCast.normal.x, 0, -torsoHitCast.normal.z));

                    //I'm on the ledge
                    climbState = ClimbState.ledge;
                    characterState = CharacterState.ledgehanging;

                    //Make the camera snap behind the player
                    Camera.mainCamera.GetComponent<ThirdPersonCamera>().SnapBehindTarget();

                    LedgeShuffling();

                }
            }
        }
        else climbState = ClimbState.none;

        //Draw rays for debugging
        Debug.DrawRay(new Vector3(t.x, t.y + topOffset, t.z), fwd, Color.green);
        Debug.DrawRay(new Vector3(t.x, t.y + torsoOffset, t.z), fwd, Color.green);
    }

    /// <summary>
    /// Allow for climbing on walls
    /// </summary>
    void WallClimb()
    {
        climbState = ClimbState.wall;

        if (Camera.mainCamera.GetComponent<ThirdPersonCamera>())
        {
            Camera.mainCamera.GetComponent<ThirdPersonCamera>().SnapBehindTarget();

            RaycastHit torsoHitCast;

            var fwd = transform.TransformDirection(Vector3.forward);
            Vector3 t; t = transform.position;
            
            if (Physics.Raycast(new Vector3(t.x, t.y + torsoOffset, t.z), fwd, out torsoHitCast, 1) && direction.y < 2 && canWallClimb)
            {
                trans.rotation = Quaternion.LookRotation(new Vector3(-torsoHitCast.normal.x, 0, -torsoHitCast.normal.z));
            }
        }

        var tempDir = trans.up * InputMovement().y + InputMovement().x * trans.right;
        direction = new Vector3(tempDir.x, tempDir.y, tempDir.z); //Input movement is used

        controller.Move(direction * 5 * Time.deltaTime);
    }

    /// <summary>
    /// Allow for climbing on walls
    /// </summary>
    void LedgeShuffling()
    {
        //climbState = ClimbState.ledge;
        Camera.mainCamera.GetComponent<ThirdPersonCamera>().SnapBehindTarget();

        var tempDir = trans.up * InputMovement().y + InputMovement().x * trans.right;
        direction = new Vector3(tempDir.x, 0, tempDir.z); //Input movement is used

        if (direction == Vector3.zero)
            moveState = MoveState.Stationary;
        else
            moveState = MoveState.Moving;

        controller.Move(direction * 5 * Time.deltaTime);
    }

    /// <summary>
    /// Gliding reduces the gravity and speed
    /// </summary>
    /// <returns>Boolean for are we gliding or not.</returns>
    private void Glide()
    {
        //Make sure I'm not on the ground or sliding down a slope.
        if (!controller.isGrounded) // && !sliding
        {
            direction.y -= (gravity / Time.deltaTime);

            //Slow down descent
            direction.y = -.5f;
            targetSpeed = runSpeed;
        }
    }
    #endregion

    /// <summary>
    /// Check if this instance is grounded. Also checks to see if I'm crouching and if something is above me
    /// </summary>
    public void CheckIfGrounded()
    {
        CheckAbove();

        if (controller.isGrounded)
        {
            //fallTime = terminalFallTime;

            //TODO: Cancel Terminal velocity check if fallen into water or just got off of a ledge
            if (hitTerminalVelocity)
            {
                //ApplyDamage(maxHealth / 4, transform.forward, 0f, 0f, 3);
                hitTerminalVelocity = false;
            }

            if (speed == 0)
            {
                if (characterState != CharacterState.pushing)
                    characterState = CharacterState.grounded;

                moveState = MoveState.Stationary;
            }
            else
                if (speed > 0)
                {
                    if (characterState != CharacterState.pushing)
                        characterState = CharacterState.grounded;

                    moveState = MoveState.Moving;
                }

            #region Crouching
            if ((Input.GetKeyDown(KeyCode.R)) && !crouching)
            {
                print("Should Crouch");
                crouching = true;
            }

            if (((Input.GetKeyUp(KeyCode.R)) && crouching) || (!crouching))
            {
                if (!CheckAbove())
                {
                    //print("Should UnCrouch");

                    crouching = false;
                    UnCrouch();
                }
            }

            if (crouching)
            {
                Crouch();
            }
            #endregion
        }
        else if (!controller.isGrounded)
        {
            //if (characterState == CharacterState.gliding)
            //    fallTime = terminalFallTime;

            ////StartCoroutine("FallTimer");
            //if (climbState == ClimbState.none && characterState == CharacterState.falling)
            //    FallingTimer();

        }

        if (status == Status.attacking)
            characterState = CharacterState.attacking;

        if (characterState == CharacterState.gliding)
            StartCoroutine("StaminaDrain");

        if (climbState != ClimbState.none)
            StartCoroutine("StaminaDrain");

        if (characterState == CharacterState.grounded)
            StartCoroutine("StaminaGain");
    }

    /// <summary>
    /// Called when this instance has landed on the ground
    /// </summary>
    /// <returns></returns>
    IEnumerator Landed()
    {
        //justLanded = true;
        status = Status.staggered;
        yield return new WaitForSeconds(0.2f);
        status = Status.none;
        //characterState = CharacterState.grounded;
    }

    /// <summary>
    /// Called when this instance has landed on the ground
    /// </summary>
    /// <returns></returns>
    IEnumerator WallJumped()
    {
        //justLanded = true;
        yield return new WaitForSeconds(2f);

        wallJumping = false;
        status = Status.none;
    }

    /// <summary>
    /// A timer that counts how long this instance has been in the air
    /// </summary>
    void FallingTimer()
    {
        //fallTime -= Time.deltaTime;

        //if (fallTime < 0)
        //{
        //    if (characterState == CharacterState.falling)
        //    {
        //        hitTerminalVelocity = true;
        //    }
        //}
    }

    /// <summary>
    /// Make this instance make a noise
    /// </summary>
    public void Cry()
    {
        if (Input.GetKeyDown(KeyCode.RightControl) || Input.GetKeyDown(KeyCode.LeftControl))
        {
            //character.PlaySound("Sounds/Breeze/BreezeBarkDefault01");
            //NETWORKING
            //photonView.RPC("PlaySound", PhotonTargets.AllBuffered, "Sounds/Breeze/BreezeBarkDefault01");
            //PlaySound(null);
        }
    }

    /// <summary>
    /// Gets horizontal and Vertical Axis
    /// </summary>
    /// <returns>Returns Horizontal and Vertical Axis as Vector2</returns>
    public Vector2 InputMovement()
    {
        return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    /// <summary>
    /// Checks the direction that this instance is moving in
    /// </summary>
    public void MoveDirectionCheck()
    {
        if (InputMovement().x < 0)
            mDirection = MoveDirection.Left;
        else if (InputMovement().x > 0)
            mDirection = MoveDirection.Right;
        else if (InputMovement().y < 0)
            mDirection = MoveDirection.Down;
        else if (InputMovement().y > 0)
            mDirection = MoveDirection.Up;
        else if (InputMovement().x == 0 && direction.y == 0)
            mDirection = MoveDirection.None;
    }

    /// <summary>
    /// Will check for the "K" input key and will grab an item if possible.
    /// If not possible, will do nothing
    /// </summary>
    /// <returns></returns>
    void AttemptToGrabItem()
    {
        if (Input.GetKeyUp(KeyCode.K))
        {
            if (!pickUpItem.ItemInRange && !pickUpItem.ItemHeld)
            {
                print("no item in range");
                return;
            }
            print("itemheld - " + pickUpItem.ItemHeld);
            if (!pickUpItem.ItemHeld)
            {
                pickUpItem.GrabItem();
            }
            else
            {
                pickUpItem.DropItem();
            }
        }
    }

    #endregion
}

