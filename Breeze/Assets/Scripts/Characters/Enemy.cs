using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value `false'

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ParticleManager))]
[RequireComponent(typeof(AnimationManager))]
[RequireComponent(typeof(SFXManager))]


public class Enemy : Character
{
    #region Members [Objects]
    //public AttackBehaviors attackMode;
    public LevelManager levelManager;
    public EnemyCombat combat;
    public EnemyMovement enemyMovement;
    public Rigidbody rigid;
    #endregion

    #region Members [Enums]
    //This determines the movement behavior of this instance
    public enum MovementBehavior { none, wander, seek, flee, circle, stalk }
    public MovementBehavior mBehavior;

    /// <summary>
    /// This contains a set of states that can be used for both the enemy and character child classes
    /// This will allow for dictating the current battle state of an enemy or AI controller party member
    /// </summary>
    public enum Action { none, Wait, ChaseTarget, RunAway, BackUp, Attack, Dodge, Heal, HealNeighbor }
    public Action action = Action.none;

    public enum AttackType { melee, casting, healing }
    public AttackType attackType = AttackType.melee;
    #endregion

    #region Members [Floats]
    public float maxSpeed = 8;
    public float minDistance = 2, safeDistance = 15, attackDistance = 2, castDistance = 5;
    public float fieldOfViewRange = 30, targetDistance = 30;
    #endregion

    #region Members [Bool]
    public bool boss = false;
    public bool canBackUp = false;
    public bool canCircleTarget = false;
    public bool canJumpBack = false;
    #endregion

    protected virtual void Awake()
    {
        #region Set Up Components
        rigid = GetComponent<Rigidbody>();
        anim = GetComponent<Animation>();
        combat = gameObject.AddComponent<EnemyCombat>();
        //combat.attack = gameObject.AddComponent<Attacks>();
        charType = Enemy.CharacterType.enemy;
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        enemyMovement = GetComponent<EnemyMovement>();
        #endregion

        //Get an initial target
        if (enemyMovement.target == null)
        {
            enemyMovement.target = combat.GetTarget();
        }
    }

    /// <summary>
    /// Allows for movement and physics
    /// </summary>
    public virtual void CharacterMovement()
    {
        BasicPhysics();
    }

    /// <summary>
    /// Check to see how far away the target is from this instance
    /// </summary>
    /// <param name="tarPos"></param>
    /// <returns></returns>
    public float GetTargetDistance(Vector3 tarPos)
    {
        Vector3 dist;
        dist = new Vector3(tarPos.x, 0, tarPos.z) - new Vector3(trans.position.x, 0, trans.position.z);
        return dist.magnitude;
    }

    public float standbyTime = 0, standbyLength = 2f;

    /// <summary>
    /// This runs the basic Battle AI behavior in a character
    /// Doesn't run when game is paused
    /// </summary>
    public void Update()
    {
        ///Run the animation framework
        animManager.AnimationFramework();

        //If Dying
        if (characterState == CharacterState.dead || status == Status.dead)
        {
            enemyMovement.canMove = false;
            enemyMovement.canSearch = false;
            return;
        }

        if (combat.GetTarget())
            enemyMovement.target = combat.GetTarget();

        if (GetTargetDistance(enemyMovement.target.transform.position) < targetDistance)
        {
            action = Action.ChaseTarget;
        }
        else
        {
            action = Action.Wait;
        }

        #region Activated
        //Activated indicates that a foe (player) is spotted
        if (activated)
        {
            //The State Machine of this instance
            UpdateState();

            //Check to see if flinching
            //FlinchCheck();

            //Check to see if this instance is attacking
            if (status == Status.attacking)
                characterState = CharacterState.attacking;
        }
        #endregion

        #region Not activated
        else if (!activated)
        {
            StandBy();
        }
        #endregion
    }

    public void UpdateState()
    {
        switch (action)
        {
            case Action.Wait:
                StandBy();
                StandbyTimer();
                enemyMovement.canSearch = false;
                break;
            case Action.ChaseTarget:

                if (enemyMovement.canMove)
                {
                    enemyMovement.MoveToTarget();
                }
                break;
            case Action.Attack:
                break;
            case Action.RunAway:
                break;
            case Action.BackUp:
                break;
            case Action.Heal:
                break;
            case Action.HealNeighbor:
                break;
        }
    }

    void FlinchCheck()
    {
        //Flinch Timer
        if (status == Status.flinching)
        {
            attacking = false;
            characterState = CharacterState.flinching;
            enemyMovement.canMove = false;
            rigidbody.AddForce(force);

            FlinchTimer();
        }
    }

    public void FlinchTimer()
    {
        flinchTime -= Time.deltaTime;

        if (flinchTime < 0)
        {
            ResetFlinch();
        }
    }

    public void ResetFlinch()
    {
        if (status != Character.Status.dead && characterState != CharacterState.dead)
        {
            status = Status.none;
            enemyMovement.canMove = true;
            attacking = false;
        }

    }

    public void StandbyTimer()
    {
        standbyTime -= Time.deltaTime;

        if (standbyTime < 0)
        {
            ResetFlinch();
        }
    }

    /// <summary>
    /// Check to see if the level manager's game state should be changed into a battle mode
    /// or boss mode
    /// </summary>
    void BattleModeCheck()
    {
        //If the target goes out of range, give up...
        if (target != null && GetTargetDistance(target.position) > (targetDistance + 5) && (status != Status.dead))   // && battleType != Action.Stationary)
        {
            //levelManager.photonView.RPC("ChangeGameState", PhotonTargets.AllBuffered, "Normal");

            //Not In Range, ill wander around        
            activated = false;
        }
        else
        {
            ///TODO: FIX THIS
            // if (GameObject.Find("LevelManager").GetComponent<LevelManager>().gameState != LevelManager.GameState.battle)
            //if (!boss)
            //levelManager.photonView.RPC("ChangeGameState", PhotonTargets.AllBuffered, "Battle");
            //else
            // levelManager.photonView.RPC("ChangeGameState", PhotonTargets.AllBuffered, "Boss");
        }
    }

    /// <summary>
    /// An Idle function for this type of instance
    /// </summary>
    void StandBy()
    {
        target = combat.GetTarget();

        if (GetTargetDistance(target.position) > 10 && status != Status.dead)
        {
            //Not In Range, ill wander around
            //steer.StartCoroutine("ChooseNewWanderPoint");
            //steer.Wander();
        }
        else if (status == Status.dead)
        {
            Stop();
        }
    }


}
