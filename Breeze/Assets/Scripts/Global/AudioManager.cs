using UnityEngine;
using System.Collections;

/// <summary>
/// AudioManager.cs
/// Only one instance of this can exist at a time
/// Controls the background music and ambient tones that are played
/// Can play or pause the music currently loaded as well
/// </summary>
public class AudioManager : MonoBehaviour 
{
    public AudioClip BGM, ambientSound;
    public  AudioSource mainBGM;

    public LevelManager levelManager;
    public float pitch =1, speed=1, volume=1;
    public bool muted = false;

    /// <summary>
    /// Find the level manager and assigns the bgm to whatever is assigned in the level manager
    /// </summary>
    void Awake()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        BGM = levelManager.BGM;
        mainBGM = gameObject.AddComponent<AudioSource>();

        DontDestroyOnLoad(transform.gameObject);
    }

	// Use this for initialization
	void Start () 
    {     
        mainBGM.clip = BGM;
        mainBGM.Play();

        mainBGM.pitch = pitch;
        mainBGM.volume = volume;
        mainBGM.mute = muted;
        mainBGM.loop = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
        mainBGM.pitch = pitch;
        mainBGM.volume = volume;
        mainBGM.mute = muted;
	}

    void OnLevelWasLoaded()
    {
        //print("YOLO");
        //mainBGM.clip = levelManager.BGM;
        //mainBGM.Play();

    }
}
