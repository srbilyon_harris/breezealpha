using UnityEngine;
using System.Collections;

/// <summary>
/// Level Manager
/// Allows for adjusting the setting of a particular level
/// - Define the spawn locations
/// - Define the music to play in the level
/// 
/// NOTE: Spawn points must be named "SpawnPoint" + Location#
/// Example: The first spawn point of a level should be named "SpawnPoint0"
/// </summary>
public class LevelManager : MonoBehaviour 
{

    public Main main;
    public GameObject spawnLocation;
    public GameObject player;
    public AudioManager audioManager;

    public bool initLoad = false;
    public int intLocationNumber = 1;
    /// <summary>
    /// When you move between scenes, the information on the place to place 
    /// the player is stored here
    /// </summary>
    public string strLocationNumber;

    public AudioClip BGM;
	
    // Use this for initialization
	void Start () 
    {
        //Locate the game manager
        main = GameObject.Find("GameManager").GetComponent<Main>();

        //Get Location Number
        intLocationNumber = main.locationNumber;
        
        //Get Starting location
        strLocationNumber = "SpawnPoint"+intLocationNumber;
        
        //Store Spawn Location
        spawnLocation = GameObject.Find(strLocationNumber);

        //Get the audio manager
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

        //Replace the currently playing BGM music with the one for this level
        if (audioManager.mainBGM.clip != BGM)
        {
            audioManager.mainBGM.clip = BGM;
            audioManager.mainBGM.Play();
        }
        
        //Make sure that the GameLoader is finished loading
        PlayerInit();
	}

    /// <summary>
    /// Place the player in the right location
    /// </summary>
    void PlayerInit()
    {
        //Make sure Player exists, then load his player information
        if (GameObject.Find("Player"))
        {
            player = GameObject.Find("Player");

            //Get location name
            //player.GetComponent<GaleCharacterBase>().area = Application.loadedLevelName;

            //Make sure Player is found, wait a sec, and set his data
            //main.saver.Load("Save/Save.sav");
            Input.ResetInputAxes();
            player.transform.position = spawnLocation.transform.position;
            player.transform.rotation = spawnLocation.transform.rotation;

            Debug.Log("Player Info Loaded");
        }
        else
            Debug.LogError("Can't find the Player");
    }

}
