/*
 * Pause Manager
 * Controls the Pause State of the game by manipulating time.
 * 
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Controls the Pause State of the game by manipulating time.
/// </summary>
public class PauseManager : MonoBehaviour 
{
    /// <summary>
    /// The current pause state.
    /// </summary>
    public enum GameState { paused, unpaused }
    public GameState gameState = GameState.unpaused;

    /// <summary>
    /// If you have a pause menu object you want displayed, attach it here
    /// </summary>
    public GameObject pauseMenu;
  
    /// <summary>
    /// The main camera is assigned here
    /// </summary>
    public Camera mainCam;

    /// <summary>
    /// This element holds the default time scale of the game
    /// </summary>
    private float time;

    /// <summary>
    /// The Start function will grab the main camera and the time scale, as well as make the game unpaused
    /// </summary>
    void Start()
    {
        Time.timeScale = 1;
        mainCam = Camera.mainCamera;
        time = Time.timeScale;
        Play();
    }

	/// <summary>
	/// The update function will allow you to pause and unpause the game via the 'P' button
	/// </summary>
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.P) && gameState == GameState.unpaused)
            Pause();

        else if (Input.GetKeyDown(KeyCode.P) && gameState == GameState.paused)
            Play();
	}

    /// <summary>
    /// The Pause function will set the timeScale to 0.
    /// It will also change the game state to pause and pause the currently played BGM if wanted
    /// You can also disable the default camera here
    /// </summary>
    public void Pause()
    {
        Time.timeScale = 0;
        //pauseMenu.gameObject.active = true;
        gameState = GameState.paused;

        var camEar = mainCam.GetComponent<AudioListener>();
        camEar.audio.Pause();
    }

    /// <summary>
    /// Resume the game
    /// </summary>
    public void Play()
    {
        //Set the time back to default
        Time.timeScale = time;

        //If there is a menu, disable it here
        if (pauseMenu != null)
        {
            pauseMenu.gameObject.SetActive(false);
        }

        //Unpause the game
        gameState = GameState.unpaused;
        
        //Enable the camera
        mainCam.enabled = true;

        //Get the audio listen of the camera
        var camEar = mainCam.GetComponent<AudioListener>();
        //If there is an audio listener...
        if (camEar != null)
        {
            //and there is audio attached to it...
            if (camEar.audio != null)
            {
                //Play it
                camEar.audio.Play();
            }
        }
    }
}
