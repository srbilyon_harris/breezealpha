/* 
 * Controls the saving and loading of data in the game
 * 
 * There are seperate serializing classes for serializing objects for saving
 * Anything that is going to be save should be serialized, then saved using saver.Save()
 * 
 * Serializable objects include
 * - Player
 * - Levels
 */

using UnityEngine;
using System.Collections;

using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Runtime.Serialization;
using System.Reflection;



[Serializable()]
public class SaveData : ISerializable
{
    public int playerID;

    //For storing players 1-4's info
    public MainPlayer p1d;
    public string playerName = "Breeze";

    // The default constructor. Included for when we call it during Save() and Load()
    public SaveData() 
    {}

    // This constructor is called automatically by the parent class, ISerializable
    // We get to custom-implement the serialization process here
    public SaveData(SerializationInfo info, StreamingContext ctxt)
    {
        // Get the values from info and assign them to the appropriate properties. Make sure to cast each variable.
        // Do this for each var defined in the Values section above
        p1d = (MainPlayer)info.GetValue("Player1", typeof(MainPlayer));
    }

    // Required by the ISerializable class to be properly serialized. This is called automatically
    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        // Repeat this for each var defined in the Values section
        info.AddValue("Player1", (p1d));
    }
}


// === This is the class that will be accessed from scripts ===
public class SaveLoad
{
    public static string currentFilePath = "Assets/Save/Save.sav";    // Edit this for different save files

    // Call this to write data
    public void Save()
    {
        Save(currentFilePath);
    }
    
     public void Save(string filePath)
    {
         //Create new save data holder
        SaveData data = new SaveData();

        #region Items to Save

        //Load Player Data
        PlayerSerializer playerSerializer = new PlayerSerializer();
        playerSerializer.RetrievePlayer1Info(data);

        #endregion

        #region Steam writter
        Stream stream = File.Open(filePath, FileMode.Create);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        bformatter.Serialize(stream, data);
        stream.Close();
        #endregion

        Debug.Log("Game Saved");
    }

    // Call this to load from a file into "data"
    public void Load()
    {
        Load(currentFilePath);
    } 

    public void Load(string filePath)
    {
        //Create save data holder
        SaveData data = new SaveData();

        #region Stream Reader
        Stream stream = File.Open(filePath, FileMode.Open);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        data = (SaveData)bformatter.Deserialize(stream);
        stream.Close();
        #endregion

        #region Items to Load
        MainPlayer mPlayerSerializer = new MainPlayer();
        mPlayerSerializer.LoadMainPlayer(data);
        #endregion

        Debug.Log(data);
    }
}

public sealed class VersionDeserializationBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
        {
            Type typeToDeserialize = null;

            assemblyName = Assembly.GetExecutingAssembly().FullName;

            // The following line of code returns the type. 
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return typeToDeserialize;
        }

        return null;
    }
}

