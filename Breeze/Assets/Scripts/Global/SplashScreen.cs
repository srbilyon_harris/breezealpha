using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour 
{
    public string levelToLoad = "Cloud 1";
	// Use this for initialization
	void Start () 
    {
        Screen.SetResolution(1280, 720, true, 60);
       StartCoroutine("StartGame");
	}
	
	// Update is called once per frame
	IEnumerator StartGame () 
    {
         yield return new WaitForSeconds(5f);
        Application.LoadLevel(levelToLoad);
	}
}
