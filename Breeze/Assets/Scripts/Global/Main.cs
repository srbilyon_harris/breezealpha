/* 
 * Creates an instance of a SaveLoad component
 * 
 */ 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Creates an instance of a SaveLoad component
/// </summary>
public class Main : MonoBehaviour 
{
    public enum GameType { Action, Platformer }
    public GameType gameType;

    public enum HealthType { HP, Lives }
    public HealthType healthType;

    [SerializeField]
    public SaveLoad saver;
    public bool showSL = false;
    public int locationNumber = 1;

    //Needs to be awake for OnGUI stuff to work
    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
        saver = new SaveLoad();
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
            showSL = !showSL;
    }
    public void OnGUI()
    {
        if (showSL)
        {
            if (GUI.Button(new Rect(20, 80, 80, 20), "Save"))
                saver.Save();

            if (GUI.Button(new Rect(20, 100, 80, 20), "Load"))
                saver.Load();

        }
    }
}
