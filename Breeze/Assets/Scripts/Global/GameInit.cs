using UnityEngine;
using System.Collections;

/// <summary>
/// GameInit.cs
/// Initialize the game
/// </summary>
public class GameInit : MonoBehaviour 
{
    public enum GameType { Action, Platformer }
    public static GameType gameType = GameType.Platformer;

    public enum HealthType { HP, Lives }
    public static HealthType healthType;

    /// <summary>
    /// The Game Manager prefab is assigned here
    /// </summary>
    public GameObject gM; 
    /// <summary>
    /// The Audio Manager prefab is assigned here
    /// </summary>
    public GameObject aM;

    /// <summary>
    /// A temporary instance of a game manager
    /// </summary>
    GameObject instanceOfGM;
    /// <summary>
    /// A temporary instance of a audio manager
    /// </summary>
    GameObject instanceOfAM;

    /// <summary>
    /// Checks to see if an instance of a GameManager or AudioManager exists
    /// If not, create one
    /// </summary>
    void Awake()
    {
        if (GameObject.Find("GameManager") == null)
        {
            instanceOfGM = Instantiate(gM, Vector3.zero, transform.rotation) as GameObject;
            instanceOfGM.name = "GameManager";

            var instanceGameType =instanceOfGM.GetComponent<Main>().gameType;
            var instanceHealthType = instanceOfGM.GetComponent<Main>().healthType;
            
            //Get the game type and let the game manager know
            switch (gameType)
            {
                case GameType.Action:
                    instanceGameType = Main.GameType.Action;
                    break;
                case GameType.Platformer:
                    instanceGameType = Main.GameType.Platformer;
                    break;
            }

            //Get the health type and let the game manager know
            switch (healthType)
            {
                case HealthType.HP:
                    instanceHealthType = Main.HealthType.HP;
                    break;
                case HealthType.Lives:
                    instanceHealthType = Main.HealthType.Lives;
                    break;
            }
        }

        if (GameObject.Find("AudioManager") == null)
        {
            instanceOfAM = Instantiate(aM, Vector3.zero, transform.rotation) as GameObject;
            instanceOfAM.name = "AudioManager";
        }
    }

    /// <summary>
    /// Destroy this once things are set up
    /// </summary>
    void Update()
    {
        Destroy(this.gameObject, 1);
    }


}
