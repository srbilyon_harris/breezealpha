using UnityEngine;
using System.Collections;

/// <summary>
/// This manager handles the caching of sound effects for characters
/// </summary>
public class SFXManager : MonoBehaviour {

    #region Particle Strings
    public string block = "Sounds/SFX/Avoid";
    public string hit1 = "Sounds/SFX/Hit4", hit2 = "Sounds/Breeze/punch2", hit3;
    public string land, slam;
    public string footStep, death = "Sounds/SFX/EnemyHit2";
    public string toss = "Sounds/Breeze/BreezeThrow01";

    public string attack1, attack2, attack3, attack4, attack5, attack6;
    public string special1, special2, special3, special4, special5, special6;


    #endregion

    /// <summary>
    /// Instantiate a sound object and play the sound, then destroy it
    /// </summary>
    /// <param name="sound"></param>
    public void PlaySound(string sound)
    {
        //Play Sound
        GameObject soundObj = (GameObject)Instantiate(Resources.Load("Spawners/SoundEmitter"));
        AudioClip audioClip = (AudioClip)Resources.Load(sound);
        soundObj.transform.position = transform.position;
        soundObj.audio.clip = audioClip;
        soundObj.audio.Play();
        //Destroy(soundObj.gameObject, audioClip.length);
    }
}
