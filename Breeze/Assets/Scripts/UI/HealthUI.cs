using UnityEngine;
using System.Collections;

public class HealthUI : BaseMenu ///TODO: Reimplement
{
    public Camera worldCamera;
    public Camera guiCamera;

    public Vector3 test;

	public event PercentageHandler OnHealthChangePercentage;
	public event ValueHandler OnHealthChangeValue;

    private UILabel HealthText;
    private UIFilledSprite HealthOrb, StaminaOrb;

    #region Variables

    /// <summary>
    /// Reference to the transfrom object
    /// </summary>
    Transform trans;

    /// <summary>
    /// Target position
    /// </summary>
    public GameObject target;

    /// <summary>
    /// Average value, offset from zero. 
    /// This is the point at which the object will float around (on the y - axis)
    /// </summary>
    public float z = 0;

    /// <summary>
    /// Amplitude, this is how high and low the curve will go
    /// </summary>
    public float a = 1f;

    /// <summary>
    /// Angular Frequency, this is how fast it will traverse the curve
    /// </summary>
    public float b = 3f;

    /// <summary>
    /// Phase Angle, check back for more details
    /// </summary>
    public float c = .5f;

    /// <summary>
    /// this is how far the curve has been traversed
    /// </summary>
    public float x = .01f;

    public float speed = .01f;
    public float stuff = 0;

    #endregion

    protected override void Awake()
    {
        // MenuManager.Register(this, true);
    }

    private void Start()
    {
        trans = transform;
        foreach(UIFilledSprite orb in trans.GetComponentsInChildren<UIFilledSprite>())
        {
            if(orb.name.Contains("Blue"))
            {
                HealthOrb = orb;
            }
            else if (orb.name.Contains("Gray"))
            {
                StaminaOrb = orb;
            }
        }
        HealthText = trans.GetComponentInChildren<UILabel>();

        worldCamera = NGUITools.FindCameraForLayer(target.layer);
        guiCamera = NGUITools.FindCameraForLayer(gameObject.layer);
    }

    public void LateUpdate()
    {
        MoveToTarget();
    }

    private void MoveToTarget()
    {
        if (!target)
            return;        

        Vector3 pos = worldCamera.WorldToViewportPoint(target.transform.position);

        pos = guiCamera.ViewportToWorldPoint(pos);

        pos.z = 0f;

        pos = Vector3.Slerp(transform.position, (pos + test), speed);
        transform.position = pos;
        transform.localPosition += new Vector3(0, stuff = (z + a * Mathf.Sin(b * x + c)), 0);
        x += Time.smoothDeltaTime;
    }

    /// <summary>
    /// for text XXX/XXX
    /// </summary>
    /// <param name="health"></param>
    /// <param name="maxHealth"></param>
    public void UpdateHealthValue(int health, int maxHealth)
    {
        UpdateHealthPercentage((float)health / (float)maxHealth);
        HealthText.text = health.ToString() + "/" + maxHealth;
    }

    /// <summary>
    /// 0~100
    /// </summary>
    /// <param name="percent"></param>
    public void UpdateHealthPercentage(int percent)
    {
        HealthOrb.fillAmount = Mathf.Clamp(((float)percent / 100), 0, 1);
        HealthText.text = percent + "%";
    }

    /// <summary>
    /// 0~1
    /// </summary>
    /// <param name="percent"></param>
    public void UpdateHealthPercentage(float percent)
    {
        HealthOrb.fillAmount = Mathf.Clamp(percent, 0, 1);
        HealthText.text = (percent*100).ToString("f0") + "%";
    }    

    /// <summary>
    /// 0~100
    /// </summary>
    /// <param name="percent"></param>
    public void UpdateStaminaPercentage(int percent)
    {
        StaminaOrb.fillAmount = Mathf.Clamp(((float)percent / 100), 0, 1);
    }

    /// <summary>
    /// 0~1
    /// </summary>
    /// <param name="percent"></param>
    public void UpdateStaminaPercentage(float percent)
    {
        StaminaOrb.fillAmount = Mathf.Clamp(percent, 0, 1);
    }

    

}
