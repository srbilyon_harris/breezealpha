Shader "Blend 2 Textures, Simply Lit" { 
	
Properties {
	_Color ("Color", Color) = (1,1,1)
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5

	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Texture2 ("Texture 2", 2D) = ""

}

Category {
	Material {

		Ambient[_Color]
		Diffuse[_Color]
	}

	SubShader 
	{
		//Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
			Tags { "RenderType"="Opaque" }
	
		Pass {

				Lighting On
				//Alphatest Greater [_Cutoff]

				SetTexture[_MainTex]

				SetTexture[_Texture2] 
				{ 
					combine texture lerp (texture) previous	
				}
			}
		}
	
	}

}