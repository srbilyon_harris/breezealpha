using UnityEngine;
using System.Collections;

public abstract class BaseMenu : MonoBehaviour
{
    public delegate void ButtonClickHandler();
	public delegate void PercentageHandler(float percentageOfChange);
	public delegate void ValueHandler(int amountOfChange);
    protected MenuManager mm;

	// Use this for initialization
	protected virtual void Awake () 
    {
        MenuManager.Register(this, false);        
	}
}
